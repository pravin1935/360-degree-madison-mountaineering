<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['video_id','comments','name','email','status'];

    public function video(){
    	return $this->belongsTo('App\Comment','indexer');
    }

      public function children()
	{
	    return $this->hasMany('App\Comment', 'parent_id', 'id');
	}

	public function parent()
	{
	    return $this->belongsTo('App\Comment', 'parent_id');
	}
}
