<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Channel;
use SubChannel;
use DB;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $channel = new Channel;
        $channel_list = $channel->orderBy('channel_id' , 'ASC')->paginate(7);
        $videocount = DB::table('videos')->count();
        $imagecount = DB::table('images')->where('isthumb' , 'no')->count();
        $usercount = DB::table('users')->count();
        $imageview = DB::table('images')->sum('number_of_views');
        $videoview = DB::table('videos')->sum('number_of_views');

         $data = array(
            'address'    => "https://s3-us-west-1.amazonaws.com/madisontv/uploads/" ,
            'images_url' => "https://s3-us-west-1.amazonaws.com/madisontv/uploads/images/" ,
            'videos_url' => "https://s3-us-west-1.amazonaws.com/madisontv/uploads/videos/",
            'channel_list' => $channel_list,
            'videocount' =>$videocount,
            'imagecount' =>$imagecount,
            'usercount' =>$usercount,
            'imageview'=>$imageview,
            'videoview'=>$videoview
         );


        View::share('shareadd' , $data);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
