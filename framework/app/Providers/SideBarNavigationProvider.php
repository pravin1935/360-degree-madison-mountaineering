<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Video;
class SideBarNavigationProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('home.sidebar', function ($view) {
            $view->with(['channels'=>$this->getItems(),'featured'=>$this->getFeature()]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
    }
    public function getItems(){

         return $channels =\DB::select("select channel_id, channel_name, (SELECT count(*)  from videos where videos.channel_id = channels.channel_id) as total from channels where type_id=1");
         
    }
    public function getFeature() {

     //return Video::where('featured','=','yes')->orderBy('date_uploaded','ASC')->paginate(16);
	  return Video::orderBy('date_uploaded','DESC')->paginate(8);

    }
}
