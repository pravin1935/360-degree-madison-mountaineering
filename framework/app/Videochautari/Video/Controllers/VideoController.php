<?php
namespace Videochautari\Video\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Video;
use App\Channel;
use App\SubChannel;
use Videochautari\Channel\Models\Type;
use Videochautari\Video\Services\VideoService;


class VideoController extends Controller {

    protected $video;

    protected $service;

    public function __construct(Video $video,VideoService $service){
        $this->video = $video;
        $this->service = $service;
    }

    public function index(){
		
        $videos = $this->video->orderBy('date_uploaded','DESC')->paginate(30);
        return view('admin.videos.index',compact('videos'));
    }


    public function create(){
	
        $channels = ['Select'=>'--Select Channel Type--']+Channel::lists('channel_name','channel_id')->toArray();
        $sub_channels = ['Select'=>'--Select sub Channel Name--']+SubChannel::lists('sub_channel_name','sub_channel_id')->toArray();
        return view('admin.videos.new',compact('channels','sub_channels'));
    }


    public function store(Request $request){
        // dd($request->all());
		 // $this->validate($request, [
   //      'title' => 'required',
   //      'description' => 'required',
   //  ]);
		$input = [
            'channel_id'=>$request->input('channel_id'),
            'sub_channel_id'=>$request->input('sub_channel_id'),
            'title'=>$request->input('title'),
            'title_seo'=>$request->input('title_seo'),
            'description'=>$request->input('description'),
            'tags'=>$request->input('tags'),
            'featured'=>$request->input('featured'),
            'promoted'=>$request->input('promoted'),
		'approved'=>'yes'
        ];
		
		 // if($request->hasFile('video')){
			
		
			$new = generateRand();
			
            $extension = $request->file('video')->getClientOriginalExtension();
			$extension2 = $request->file('image')->getClientOriginalExtension();
            $fileName = $new.".".$extension;
            $fileName2 = $new.".".$extension2;

            $request->file('video')->move('uploads',$fileName);
            $request->file('image')->move('uploads',$fileName2);

            $input['type'] = $extension;
            $input['thumb'] = $extension2;
            $input = array_add($input,'video_id',$new);
            // $input = array_add($input,'video_id',$new);

        // }
		
         //$this->service->save($request);
		 $this->video->create($input);
        return redirect('dashboard/videos');
    }


    public function edit($channel_id){
        $video = $this->video->find($channel_id);
        $types = ['Select'=>'--Select Channel Type--']+Type::lists('name','id')->toArray();
        $channels = ['Select'=>'--Select Channel Type--']+Channel::lists('channel_name','channel_id')->toArray();
        $sub_channels = ['Select'=>'--Select sub Channel Name--']+SubChannel::lists('sub_channel_name','sub_channel_id')->toArray();
 
        return view('admin.videos.edit',compact('types','video','channels','sub_channels'));
    }

    public function delete($vid){
        $this->video->find($vid)->delete();
        return redirect('dashboard/videos');
    }



    public function update($channel_id,Request $request){
        $video = $this->video->find($channel_id);
        $video->update($request->all());
        return redirect('dashboard/videos');
    }

    public function addVideo(Request $request ){
        $id = $this->service->saveVideo($request);
        return $id->indexer;
    }
}