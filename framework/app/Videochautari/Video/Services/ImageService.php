<?php
namespace Videochautari\Video\Services;

class ImageService{

    public $percent = 0.2;

    public function copyImage($image,$name){
        $remote_img = $image;
        list($width, $height) = \getimagesize($remote_img);

        $newwidth = $width * $this->percent;
        $newheight = $height * $this->percent;



        $thumb = \imagecreatetruecolor($newwidth, $newheight);
        $source = \imagecreatefromjpeg($remote_img);

        \imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);


        $path = 'uploads/videos/mp4/thumbs/'.$name.'.jpg';
        imagejpeg($thumb, $path);


}


}

