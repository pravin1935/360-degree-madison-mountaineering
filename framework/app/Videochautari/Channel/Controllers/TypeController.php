<?php
namespace Videochautari\Channel\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Channel;
use Videochautari\Channel\Models\Type;

class TypeController extends Controller {

    protected $channel;

    public function __construct(Channel $channel){
        $this->channel = $channel;
    }

    public function index(){
        $channels = $this->channel->all();
        return view('admin.channels.index',compact('channels'));
    }

    public function create(){
        return view('admin.channels.new');
    }


}