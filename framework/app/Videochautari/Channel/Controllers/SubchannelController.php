<?php
namespace Videochautari\Channel\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Channel;
use Videochautari\Channel\Models\Type;


class SubChannelController extends Controller {

    protected $channel;

    public function __construct(Channel $channel){
        $this->channel = $channel;
    }

    public function index(){
        $channels = $this->channel->all();
        return view('admin.channels.index',compact('channels'));
    }


    public function create(){
        $types = ['Select'=>'--Select Channel Type--']+Type::lists('name','id')->toArray();
        return view('admin.channels.new',compact('types'));
    }


    public function store(Request $request){
        $input = ['channel_name'=>$request->input('channel_name'),
            'type_id'=>$request->input('type_id'),
            'channel_name_seo'=>$request->input('channel_name_seo'),
            'channel_description'=>$request->input('channel_description')
        ];
        if($request->has('channel_picture')){
            $input['channel_picture'] = $request->file('channel_picture')->getClientOriginalName();
            $request->file('photo')->move('uploads/', 'uploads/'.$input['channel_picture']);
            dd($input);
        }

        $this->channel->create($input);
        return redirect('admin/channels');
    }


    public function edit($channel_id){
        $channel = $this->channel->find($channel_id);
        $types = ['Select'=>'--Select Channel Type--']+Type::lists('name','id')->toArray();
        return view('admin.channels.edit',compact('types','channel'));
    }


    public function update($channel_id,Request $request){
        $channel = $this->channel->find($channel_id);
        $channel->update($request->all());
        return redirect('admin/channels');
    }

}