<?php
namespace Videochautari\Channel\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Channel;
use App\SubChannel;
use Videochautari\Channel\Models\Type;
use Videochautari\Channel\Services\ChannelService;

class ChannelController extends Controller {

    protected $channel;
    protected $subChannel;
    protected $service;

    public function __construct(Channel $channel,SubChannel $subChannel,ChannelService $service){
        $this->channel = $channel;
        $this->subChannel = $subChannel;
        $this->service = $service;
    }

    public function index(){
        $channels = $this->service->getList();
        return view('admin.channels.index',compact('channels'));
    }


    public function create(){
        $types = $this->service->typeselect();
        return view('admin.channels.new',compact('types'));
    }


    public function store(Request $request){
       $this->service->save($request);
        return redirect('admin/channels');
    }


    public function edit($channel_id){
        $channel = $this->channel->find($channel_id);
        $types = $this->service->typeselect();
        return view('admin.channels.edit',compact('types','channel'));
    }


    public function update($channel_id,Request $request){
        $this->service->update($channel_id,$request);
        return redirect('admin/channels');
    }

    public function addSubChannel($channel_id){
        $types = ['Select'=>'--Select Channel Type--']+$this->channel->lists('channel_name','channel_id')->toArray();
        return view('admin.channels.subchannel.new',compact('types','channel_id'));
    }

    public function subchannelStore($channel_id,Request $request){
        $input = ['sub_channel_name'=>$request->input('sub_channel_name'),
            'sub_channel_name_seo'=>$request->input('sub_channel_name_seo'),
            'sub_channel_description'=>$request->input('sub_channel_description'),
            'parent_channel_id'=>$channel_id
        ];
        if($request->hasFile('sub_channel_picture')){
            $input['sub_channel_picture'] = $request->file('sub_channel_picture')->getClientOriginalName();
            $request->file('sub_channel_picture')->move('uploads/', 'uploads/'.$input['sub_channel_picture']);

        }

        $this->subChannel->create($input);
        return redirect('admin/channels');
    }

    public function viewSubChannel($channel_id){
        $channels = $this->subChannel->where(['parent_channel_id'=>$channel_id])->get();
        return view('admin.channels.subchannel.index',compact('channels'));
    }

    public function subChannel($channel_id){
        $types = $this->subChannel->where(['parent_channel_id'=>$channel_id])->get(['sub_channel_name','sub_channel_id']);
        return $types;
    }


}