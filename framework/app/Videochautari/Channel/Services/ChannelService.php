<?php
namespace Videochautari\Channel\Services;

use App\Channel;
use App\SubChannel;
use Videochautari\Channel\Models\Type;
use app\Repository\Service;

class ChannelService {
    protected $channel;
    public function __construct(Channel $channel){
        $this->channel = $channel;
    }

    public function typeselect(){
       return ['Select'=>'--Select Channel Type--']+Type::lists('name','id')->toArray();
    }

    public function getList(){
        return $this->channel->all();

    }
    public function save($request){
        $input = ['channel_name'=>$request->input('channel_name'),
            'type_id'=>$request->input('type_id'),
            'channel_name_seo'=>$request->input('channel_name_seo'),
            'channel_description'=>$request->input('channel_description')
        ];
        if($request->hasFile('channel_picture')){
            $input['channel_picture'] = $request->file('channel_picture')->getClientOriginalName();
            $request->file('photo')->move('uploads/', 'uploads/'.$input['channel_picture']);

        }
        return $this->channel->create($input);
    }
    public function update($channel_id,$request){
            $this->channel = $this->channel->find($channel_id);
            $input = ['channel_name'=>$request->input('channel_name'),
                'type_id'=>$request->input('type_id'),
                'channel_name_seo'=>$request->input('channel_name_seo'),
                'channel_description'=>$request->input('channel_description')
            ];
            if($request->hasFile('channel_picture')){
                $input['channel_picture'] = $request->file('channel_picture')->getClientOriginalName();
                $request->file('photo')->move('uploads/', 'uploads/'.$input['channel_picture']);

            }
        return $this->channel->update($input);


    }
    public function delete(){

    }




}