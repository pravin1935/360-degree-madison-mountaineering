<?php

namespace App\Collections;

Class Collection {

	public static $images = array(
					  28=>array('sd_img'=>'terai_tv_SD','hd_img'=>'terai_tv_HD'),
					  7=>array('sd_img'=>'SonyTV_SD','hd_img'=>'SonyTV_HD'),
					  34=>array('sd_img'=>'News24_SD','hd_img'=>'News24_HD'),
					  13=>array('sd_img'=>'NepalTV_SD','hd_img'=>'NepalTV_HD'),
					  29=>array('sd_img'=>'nbex_mountain_tv_SD','hd_img'=>'nbex_mountain_tv_HD'),
					  1=>array('sd_img'=>'Kantipur_TV_SD','hd_img'=>'Kantipur_TV_HD'),
					  2=>array('sd_img'=>'ImageChannel_SD','hd_img'=>'ImageChannel_HD'),
					  27=>array('sd_img'=>'Himalaya_TV_SD','hd_img'=>'Himalaya_TV_HD'),
					  19=>array('sd_img'=>'colors_SD','hd_img'=>'colors_HD'),
					  3=>array('sd_img'=>'Avenues_SD','hd_img'=>'Avenues_HD'),
					  14=>array('sd_img'=>'ABC_SD','hd_img'=>'ABC_HD'),
					  4=>array('sd_img'=>'comedy_SD','hd_img'=>'comedy_HD'),
					  17=>array('sd_img'=>'events-shows_SD','hd_img'=>'events-shows_HD'),
 5=>array('sd_img'=>'zeetv_SD','hd_img'=>'zeetv_HD'),

 21=>array('sd_img'=>'GlobalNRN_SD','hd_img'=>'GlobalNRN_HD'),

 23=>array('sd_img'=>'TodayHigh_SD','hd_img'=>'TodayHigh_HD'),

 20=>array('sd_img'=>'StarPlus_SD','hd_img'=>'StarPlus_HD'),

 6=>array('sd_img'=>'LifeOK_SD','hd_img'=>'LifeOK_HD'),

 15=>array('sd_img'=>'SpecialNews_SD','hd_img'=>'SpecialNews_HD'),

 10=>array('sd_img'=>'PopSongs_SD','hd_img'=>'PopSongs_HD'),

 24=>array('sd_img'=>'Community_SD','hd_img'=>'Community_HD'),

 32=>array('sd_img'=>'TalkShows_SD','hd_img'=>'TalkShows_HD'),

 11=>array('sd_img'=>'NepaliLokGeet_SD','hd_img'=>'NepaliLokGeet_HD'),

 33=>array('sd_img'=>'MNTV_SD','hd_img'=>'MNTV_HD'),

 9=>array('sd_img'=>'NepaliMovies_SD','hd_img'=>'NepaliMovies_HD'),

8=>array('sd_img'=>'NepaliEvents_SD','hd_img'=>'NepaliEvents_HD'),

22=>array('sd_img'=>'MahaProgram_SD','hd_img'=>'MahaProgram_HD'),

31=>array('sd_img'=>'HongKong_SD','hd_img'=>'HongKong_HD'),

12=>array('sd_img'=>'HindiSongs_SD','hd_img'=>'HindiSongs_HD'),

16=>array('sd_img'=>'HindiMovies_SD','hd_img'=>'HindiMovies_HD'),

18=>array('sd_img'=>'EnglishMovies_SD','hd_img'=>'EnglishMovies_HD'),

 26=>array('sd_img'=>'NRNVoice_SD','hd_img'=>'NRNVoice_HD'),
					  );

}