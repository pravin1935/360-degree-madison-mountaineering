<?php

use App\Helpers\Generator;

function generateRand(){
    $generator = new Generator();
    return $generator->getToken(20);
}

function makeBatFile($filename,$script){
    $file = fopen($filename,"w");
    return fwrite($file, $script);
}