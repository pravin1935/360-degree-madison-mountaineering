<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Video;
use App\Channel;
use App\SubChannel;
use App\Image;
use App\Comment;
use App\Type;
use App\User;
use Input;
use Auth;
use View;

class SiteController extends Controller
{

    public $video;
    public $channel;
    public $image;
    public $subchannel;

    public function __construct(Video $video, Channel $channel, Image $image , SubChannel $subchannel){
        

         $this->video = $video;
         $this->image = $image ; 
         $this->channel = $channel;
         $this->subchannel = $subchannel;
        // if(Auth::user())
        // {
        //     $user=Auth::user();
        //     $this->video = $video->where('public_private' ,'=' , 'yes')->orWhere('user_id' , '=' , $user->id);
        //     $this->channel = $channel;
        //     $this->image = $image->where('public_private' , '=' , 'yes')->orWhere('user_id' , '=' , $user->id);            
        //     }

        // else{
        
        //     $this->video = $video->where('public_private' ,'=' , 'yes');
        //     $this->channel = $channel;
        //     $this->image = $image->where('public_private' , '=' , 'yes');

        
        // }
        
    }



    public function index(){
        
        $featured = $this->video->orderBy('date_uploaded','ASC')->where('featured','=','yes')->paginate(6);
        $latestvideo1 =  $this->video->where('category' , '!=' , 'plain' )->orderBy('date_uploaded','DESC')->paginate(10);
        $mostviewd = $this->video->orderBy('number_of_views','DESC')->paginate(6);
        
        $videos = $this->video->where('category' , '!=' , 'plain' )->orderBy('date_uploaded','DESC')->paginate(6);
        $promoted  = $this->video->where('promoted','=','yes')->orderBy('date_uploaded','ASC')->paginate(12);
        $latestvideo =  $this->video->orderBy('date_uploaded','DESC')->paginate(14);
        $channels = $this->channel->orderBy('channel_id' , 'ASC')->paginate(7);
        $fimage = $this->image->where('featured','=','yes')->where('isthumb' , '=' , 'no')->orderBy('date_uploaded' , 'ASC')->paginate(6);
       
        $mimage = $this->image->orderBy('number_of_views' , 'DESC')->paginate(8);
        $mimages = $this->image->orderBy('number_of_views' , 'DESC')->paginate(6);

        return view('home.index',compact('videos','featured','promoted','mostviewd','latestvideo','channels', 'latestvideo1' , 'mimage','mimages' , 'fimage'));
    }

   


    public function subchannel($sub_channel_id){

        $videos = $this->video->where('sub_channel_id','=',$sub_channel_id)->orderBy('date_uploaded','DESC')->paginate(50);
        // $videoplain = $this->video->where('category' , 'plain')->where('sub_channel_id','=',$sub_channel_id)->orderBy('date_uploaded','DESC')->paginate(50);
        $images = $this->image->where('sub_channel_id','=',$sub_channel_id)->where('isthumb' , '=' , 'no')->orderBy('date_uploaded','DESC')->paginate(50);
        $videos->setPath('');
        $channels = $this->channel->orderBy('channel_id' , 'ASC')->paginate(7);
        return view('home.subchannel',compact('videos','images' , 'channels' , 'videoplain' , 'sub_channel_id'));
    }



    public function channel($channel_name){

        $videos = \DB::table('videos')->where('channel_id','=',$channel_name)->orderBy('channel_id','DESC')->paginate(50);
        $images = $this->image->where('channel_id','=',$channel_name)->where('isthumb' , '=' , 'no')->orderBy('date_uploaded','DESC')->paginate(50);
        $videos->setPath('');
        $channels = $this->channel->orderBy('channel_id' , 'ASC')->paginate(7);
        $subchannels = $this->subchannel->where('channel_id' , $channel_name)->with(['user'])->get(); 
        return view('home.channel',compact('videos','images' , 'channels' , 'channel_name' , 'subchannels'));
    }






    public function channel_image($subchannel_name){
        
        $sub_channel_id = $this->subchannel->where('sub_channel_name',$subchannel_name)->first()->sub_channel_id;
        $images = $this->image->where('sub_channel_id','=',$sub_channel_id)->orderBy('date_uploaded','DESC')->paginate(50);
        $videos= "";
     
       return view('home.channel',compact('videos' , 'images'));
    }





    public function featureVideo(){
        $channels = $this->channel->where('type_id',1)->select('channel_id')->get();
        $videos = $this->video->where('featured','=','yes')->orderBy('date_uploaded','ASC')->whereIn('channel_id', $channels)->paginate(50);
        $videos->setPath('');
        return view('home.feature',compact('videos'));
    }



    public function promotedVideo(){
        $videos = $this->video->where('promoted','=','yes')->paginate(50);
        $videos->setPath('');
        return view('home.promote',compact('videos'));
    }



    public function detailVideo(Request $request, $id ){


        $page_was_refreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';
        
        $video = new Video;
        $video = $video->find($id);
        $video_privacy = $video->public_private;
        $videoid = $video->video_id;
        
        if ( $video_privacy == 'private') {
            if ( ! \Auth::user() ) {

                return view('home.session_end');
                
            }

            $user_login = \Auth::user()->id;
            $user_video = $video->user_id;

            if($user_login !== $user_video)
            {
                return view ('home.link_broken');
            }



        }

        if(! ($page_was_refreshed ) )
        {
            $video->increment('number_of_views');    
        }
        $channel = $this->video->find($id)->channel()->first();
        $channel_id = $channel->channel_id;

        
        if ($video_privacy == 'yes') {
       
                 $related_video = $video->where('channel_id','=',$channel_id)
                                     ->where('public_private' , '=' ,'yes')
                                     ->orderBy('date_uploaded','DESC')
                                     ->paginate(8);

                $featured   =  $this->video->where('featured','=','yes')
                                ->where('public_private' , '=' ,'yes')
                                ->orderBy('date_uploaded','ASC')
                                ->paginate(15);




        }

        else{

                    $user = \Auth::user();
                        if ( is_null( $user )){
                              return view('home.session_end');
                            }
                    $id = $user->id;
                    $related_video = $video->where('channel_id','=',$channel_id)
                                     ->where('user_id' , '=' , $id)
                                     ->where('public_private' , '=' ,'private')   
                                     ->orWhere('public_private' , '=' ,'yes')   
                                     ->orderBy('date_uploaded','DESC')
                                     ->paginate(8);


                    $featured   =  $this->video->where('featured','=','yes')
                                     ->where('user_id' , '=' , $id)
                                     ->where('public_private' , '=' ,'private')   
                                     ->orWhere('public_private' , '=' ,'yes')   
                                     ->orderBy('date_uploaded','DESC')
                                      ->paginate(10);


                  
        }

              return view('home.detail',compact('video','related_video' , 'featured' , 'videoid'));
           
       
         
    }

    
    public function detailplainVideo(Request $request, $id ){


        $page_was_refreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';
        
        $video = new Video;
        $video = $video->where('category' , 'plain')->where('indexer' , $id)->first();
        $video_privacy = $video->public_private;
        $videoid = $video->video_id;
        
        if ( $video_privacy == 'private') {
            if ( ! \Auth::user() ) {

                return view('home.session_end');
                
            }

            $user_login = \Auth::user()->id;
            $user_video = $video->user_id;

            if($user_login !== $user_video)
            {
                return view ('home.link_broken');
            }



        }

        if(! ($page_was_refreshed ) )
        {
            $video->increment('number_of_views');    
        }
        $channel = $this->video->find($id)->channel()->first();
        $channel_id = $channel->channel_id;

        
        if ($video_privacy == 'yes') {
       
                $related_video = $video->where('category' , 'plain')->where('channel_id','=',$channel_id)
                                     ->where('public_private' , '=' ,'yes')
                                     ->orderBy('date_uploaded','DESC')
                                     ->paginate(8);

                $featured   =  $this->video->where('category' , 'plain')->where('featured','=','yes')
                                ->where('public_private' , '=' ,'yes')
                                ->orderBy('date_uploaded','ASC')
                                ->paginate(15);




        }

        else{

                    $user = \Auth::user();
                        if ( is_null( $user )){
                              return view('home.session_end');
                            }
                    $id = $user->id;
                    $related_video = $video->where('category' , 'plain')
                                     ->where('category' , 'plain')
                                     ->where('channel_id','=',$channel_id)
                                     ->where('user_id' , '=' , $id)
                                     ->where('public_private' , '=' ,'private')   
                                     ->orWhere('public_private' , '=' ,'yes')   
                                     ->orderBy('date_uploaded','DESC')
                                     ->paginate(8);


                    $featured   =  $this->video->where('category' , 'plain')
                                     ->where('category' , 'plain')
                                     ->where('featured','=','yes')
                                     ->where('user_id' , '=' , $id)
                                     ->where('public_private' , '=' ,'private')   
                                     ->orWhere('public_private' , '=' ,'yes')   
                                     ->orderBy('date_uploaded','DESC')
                                     ->paginate(10);

         
        }


        
        
                    return view('home.detailplain',compact('video','related_video' , 'featured' , 'videoid'));
           
       
         
    }

     public function detailImage(Request $request,$id){


      // mancha le afno private ra public video matrae hernu parcha ... code ma arkako ni private herna cha 

        $page_was_refreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';
        
        $image = new Image;
        $image = $image->find($id);
        $image_privacy = $image->public_private;

        

        
        if ( $image_privacy == 'private') {
            
            if ( ! \Auth::user()) {
                return view('home.session_end');
            }

            $user_login = \Auth::user()->id;
            $user_image = $image->user_id;

            if($user_login !== $user_image)
            {
                return view ('home.link_broken');
            }

        }

        if(! ($page_was_refreshed ) )
        {
            $image->increment('number_of_views');    
        }
        $channel = $image->find($id)->channel()->first();
        $channel_id = $channel->channel_id;

        
        if ($image_privacy == 'yes') {
       
                 $related_image = $image->where('channel_id','=',$channel_id)
                                     ->where('public_private' , '=' ,'yes')
                                     ->orderBy('date_uploaded','DESC')
                                     ->paginate(7);


                $featured_image   =  $image->where('featured','=','yes')
                                  ->where('public_private' , '=' ,'yes')
                                  ->orderBy('date_uploaded','ASC')
                                  ->paginate(15);




        }

        else{

                    $user = \Auth::user();
                        if ( is_null( $user )){
                              return view('home.session_end');
                            }
                    $id = $user->id;
                    $related_image = $image->where('channel_id','=',$channel_id)
                                     ->where('user_id' , '=' , $id)
                                     ->where('public_private' , '=' ,'private')   
                                     ->orWhere('public_private' , '=' ,'yes')   
                                     ->orderBy('date_uploaded','DESC')
                                     ->paginate(7);

                    $featured_image = $image->where('featured','=','yes')
                                     ->where('user_id' , '=' , $id)
                                     ->where('public_private' , '=' ,'private')   
                                     ->orWhere('public_private' , '=' ,'yes')   
                                     ->orderBy('date_uploaded','DESC')
                                     ->paginate(15);

                  
        }

                    return view('home.image',compact('image','related_image' , 'featured_image'));
            
        }

    public function fullVideo(Request $request,$id){
  
        return $this->video->find($id)->channel;
  
    }


    public function demo(Request $request){
      
        $video =  Input::has('video') ? $request->get('video') : $this->video->orderBy('date_uploaded','DESC')->first()->video_id;
        return view('partials.video',compact('video'));
    }
    

    public function searchVideos(Request $request){

        $param = $request->get('title');
         $user=Auth::user();
         if($user)
         {
            $videos = \DB::table('videos')->where(function($q) use ($param){
                                                                            $q->where('title' , 'LIKE' , '%'.$param.'%')
                                                                            ->orwhere('description' , 'LIKE' , '%'.$param.'%');
                                                                      })
                                                                ->where(function($q) use ($param , $user){
                                                                            $q->where('public_private' ,'=' , 'yes')
                                                                            ->orWhere('user_id' , '=' , $user->id);
                                                                      })->paginate(20);


            $images = \DB::table('images')->where(function($q) use ($param){
                                                                            $q->where('title' , 'LIKE' , '%'.$param.'%')
                                                                            ->orwhere('description' , 'LIKE' , '%'.$param.'%');
                                                                      })
                                                                ->where(function($q) use ($param , $user){
                                                                            $q->where('public_private' ,'=' , 'yes')
                                                                            ->orWhere('user_id' , '=' , $user->id);
                                                                      })->paginate(20);
        }

        else{
                        
                        $videos = \DB::table('videos')->where('public_private' , 'yes')
                                                                ->where(function($q) use ($param){
                                                                            $q->where('title' , 'LIKE' , '%'.$param.'%')
                                                                            ->orwhere('description' , 'LIKE' , '%'.$param.'%');
                                                                      })
                                                                ->paginate(20);

                        
                        $images = \DB::table('images')->where('public_private' , 'yes')
                                                                ->where(function($q) use ($param){
                                                                            $q->where('title' , 'LIKE' , '%'.$param.'%')
                                                                            ->orwhere('description' , 'LIKE' , '%'.$param.'%');
                                                                      })
                                                                ->paginate(20);
                        
                         
            }   


        
        return view('home.search',compact('videos','images'));

        
    }

    public function imagechannel(Request $request , $name){

        $image = new Image;
        
        if(Auth::user())
        {
            $user=Auth::user();
            $this->image = $image->where('public_private' , '=' , 'yes')->orWhere('user_id' , '=' , $user->id);            
            }

        else{
        
            $this->image = $image->where('public_private' , '=' , 'yes');
        
        }

        $subchannel = new SubChannel;
        $sub_ch_id = $subchannel->where('sub_channel_name' , $name)->get();
        $data = $image->where ('sub_channel_id' , $sub_ch_id[0]->sub_channel_id)->where('isthumb' , 'no')->get();
        // return $data;
        // return  $this->image->with(['subchannel'])->get();
        // $data = $this->image->with(['subchannel'=>function($query) use ($name){ $query->where('sub_channel_name' ,'=', $name); }])->where('isthumb', 'no')->get();

        return view('home.360_image' , compact('data'));
    }



    public function videochannel(Request $request , $name){

        
         if(Auth::user())
        {
            $video = new Video;
            $user=Auth::user();
            $this->video = $video->where('public_private' , '=' , 'yes')->orWhere('user_id' , '=' , $user->id);            
        }

        else{
            $video = new Video;
            $this->video = $video->where('public_private' , '=' , 'yes');
        
        }

        $subchannel = new SubChannel;
        $sub_channel_selected = $subchannel->where('sub_channel_name' , $name)->first();
        $id= $sub_channel_selected->sub_channel_id;

        $data = $this->video->where('sub_channel_id' , $id)->get();

        return view('home.360_video' , compact('data'));
    }


    public function adminview(){
        
        $user_admin = User::where('role' , 1)->get();
        $user = User::where('role' , 0)->get();
        // return view('home.user_list' , ['useradmin'=>$user_admin , 'user'=> $user]);

        return view('home.admin' , ['useradmin'=>$user_admin , 'user'=> $user]);
    }


    public function subchannelplain(Request $request)
    {
      $subchannelid = $request->name;
      $plainvideolist = Video::where('category' , 'plain')->where('sub_channel_id' ,$subchannelid )->get();
      // return ("i am here");
      return response()->json(['data'=>$plainvideolist]);

    }



    public function aboutUs(Request $request){
    return view('about');
    }

    public function termsofservice(){
    return view('home.termsofservice');
    }

    public function privacypolicy(){
    return view('home.privacypolicy');
    }

    public function visitCount($count=1){       
    }

    public function videoById($id,$string){
        return redirect('detail/'.$id);
    }
    
    public function playerCanvas()
    {
        return view('canvas');
    }

}