<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
// use Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
	

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
	
	  protected $redirectPath = '/';

	  
     protected $loginPath = '/promoted-video';

	  // protected $redirectTo = '/';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

        /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
    
       
       $credentials = $request->only('email' , 'password');
       \Log::info($credentials);

       $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);
        
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        
        if (Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        // return redirect($this->loginPath())
        //     ->withInput($request->only($this->loginUsername(), 'remember'))
        //     ->withErrors([
        //         $this->loginUsername() => $this->getFailedLoginMessage(),
        //     ]);
         return $this->sendFailedLoginResponse($request);
    }

        protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->loginUsername() => $this->getFailedLoginMessage(),];

        // if ($request->expectsJson()) {
            return response()->json($errors, 422);
        // }

        // return redirect()->to('/')
        //     ->withInput($request->only($this->loginUsername(), 'remember'))
        //     ->withErrors($errors);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        //dd($data);
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],

            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


}
