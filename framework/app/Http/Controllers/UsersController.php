<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Hash;
use App\Video;
use App\Image;
use App\Channel;
use Intervention\Image\ImageManagerStatic as Img;

class UsersController extends Controller
{
    protected $video;

    protected $image;
    protected $channel;

    public function __construct(Video $video,Image $image, Channel $channel){
        $this->video = $video;
        $this->image = $image;
        $this->channel = $channel;
        $user = \Auth::user();
        if ( is_null( $user )){
          return view('home.session_end');
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {    
     $user = \Auth::user();
     if ( is_null( $user )){
          return view('home.session_end');
    }
    
         $user=Auth::user();
         $videos = $this->video->orderBy('date_uploaded','DESC')->where('user_id',"=",$user->id)->paginate(30);
         $images = $this->image->orderBy('date_uploaded','DESC')->where('user_id',"=",$user->id)->paginate(30);

      
        return view('user.index',compact('user','videos','images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {    

        $user = \Auth::user();
            if ( is_null( $user )){
              return view('home.session_end');
            }

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $extension2 = $avatar->getClientOriginalExtension();
            // $new = uniqid();
            // dd($new);
            ini_set('memory_limit','256M');
            // dd($extension2);
            $filename = uniqid().'.'.$extension2;
            Img::make($avatar)->resize(300, 200)->save('uploads/avatars/'.$filename);
            // $request->file('avatar')->move('uploads/avatars',$filename);

            $user=Auth::user();
            $user->avatar = $filename;
            // $user->save();
        }

        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->save();


        return redirect()->action('UploadsController@index');
        

    }

    public function adduser(Request $request){

       $user = \Auth::user();
            if ( is_null( $user )){
              return view('home.admin');
            }
        return view('home.adduser');
        

    }
   
   public function user_data_add(Request $request){

        $user_match = User::where('email' , $request->email)->first();
        
        if($user_match){
            $request->session()->flash('status' , 'no');
            return view('home.adduser');
        }
        $request->merge(['password'=>Hash::make($request->password) , 'key'=>12]);
        $user = \App\User::create($request->all());
        return view('home.added');
   
   }

   public function edituser(Request $request){

        $user_admin = User::where('role' , 1)->get();
        $user = User::where('role' , 0)->get();
        return view('home.user_list' , ['useradmin'=>$user_admin , 'user'=> $user]);
   }


   public function deleteuser(Request $request){

        $user = User::find($request->user_id)->delete();

   }
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
