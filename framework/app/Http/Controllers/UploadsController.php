<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Video;
use Auth;
use User;
use App\Channel;
use App\SubChannel;
use Imge;
use App\Image;
use File;
use URL;
use FFMpeg;

class UploadsController extends Controller
{
     // public $_SESSION['ids'] = array();
    // public function __construct(){
    //     $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
            $user = \Auth::user();
            if ( is_null( $user )){
              return view('home.session_end');
            }

           
            $data = $user;
            return view('user.user',compact('user','data'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */



    public function input(Request $request){

        $user = \Auth::user();

        if ( is_null( $user ))
        {
              return view('home.admin');
        }
        $subchanneloption = SubChannel::all();
        return view('home.upload_form'  , compact('subchanneloption'));
    }
    public function create()

    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $user=Auth::user();
        // dd($request);
        if(Input::hasFile('image')){ 
            $files=Input::file('image');
            $file_count = count($files);
           // $uploadcount = 76;
            foreach ($files as $file){ 
                    $image = new Image;
                    $subchannel = new SubChannel;
                    
                    $type = $file->getMimeType();

                    $sub_channel_name = Input::get('subchannel');

                    $channel = Input::get('channel');
                    
                    if (strpos($type ,'image')!==false){ 
                        $image->channel_id = Input::get('channel');
                        $image->type = $file->getClientOriginalExtension();
                        $image->title = Input::get('title');
                        $image->description = Input::get('description');
                        $image->image_id = generateRand();
                        $image->featured = 'yes';
                        $image->user_id = $user->id;
                        $image->public_private =Input::get('image_privacy');
                        //$image->indexer = $uploadcount; 
                        //$uploadcount++;
                        //$sub_channel_id = sha1(time());
                        //$image->sub_channel_id = 152;
                        $fileName = $image->image_id.'.'.$image->type;
                        $image->save();

                        $result = $subchannel->where('user_id',"=",$user->id)->where("channel_id", '=', $channel)->where("sub_channel_name",'=',$sub_channel_name)->get();
                        // dd($result->count());
                        foreach ($result as $data) {
                            $prev_channel_id = $data->sub_channel_id;
                       }
                                   
                        if (! $result->count()){
                             $subchannel->sub_channel_name = $sub_channel_name;
                             $subchannel->channel_id = Input::get('channel');
                             $subchannel->has_vids = 'yes';
                             $subchannel->user_id = $user->id;
                             $subchannel->save();
                             $image->sub_channel_id = $subchannel->sub_channel_id;
                             $image->save();
                        }
                        else{
                            $image->sub_channel_id = $prev_channel_id;
                            $image->save();
                        }

                        $file->move('uploads',$fileName);
                        
                    }
                    else{
                        return("please upload the file as per specified");
                    }

                    

                    
                  
                }
            }

        if(Input::hasfile('video')){
            $file_video=Input::file('video');
            $file_image=Input::file('thumbnail');
            $image = new Image;
            $video = new Video;
            $subchannel = new SubChannel;

            $type = $file_video->getMimeType();
            $sub_channel_name = Input::get('subchannel');
            $channel = Input::get('channel');

            if (strpos($type ,'video')!==false){ 
                        $video->channel_id = Input::get('channel');
                        $video->type = $file_video->getClientOriginalExtension();
                        $video->thumb = $file_image->getClientOriginalExtension();
                        $video->description = Input::get('description');
                        $video->title = Input::get('title');
                        $video->video_id = str_random(6);
                        $video->featured = 'yes';
                        $video->promoted = 'yes';
                        $video->user_id = $user->id;


                        // $image->type = $file_image->getClientOriginalExtension();
                        // $video->thumb = $image->type;
                        // $image->image_id = $video->video_id;
                        // $image->isthumb = true;

                        $fileName_video = $video->video_id.'.'.$video->type;
                        $filename_image = $video->video_id.'.'.$video->thumb;
                        // $image->save();
                        $video->save();

                        $result = $subchannel->where('user_id',"=",$user->id)
                                             ->where("channel_id", '=', $channel)
                                             ->where("sub_channel_name",'=',$sub_channel_name)
                                             ->get();

                        foreach ($result as $data) {
                            $prev_channel_id = $data->sub_channel_id;
                       }
                                   
                        if (! $result->count()){
                             $subchannel->sub_channel_name = $sub_channel_name;
                             $subchannel->channel_id = Input::get('channel');
                             $subchannel->has_vids = 'yes';
                             $subchannel->user_id = $user->id;
                             $subchannel->save();
                             $video->sub_channel_id = $subchannel->sub_channel_id;
                             $video->save();
                        }

                        else{
                            $video->sub_channel_id = $prev_channel_id; 
                            $video->save();
                            }

                        $file_video->move('uploads',$fileName_video);
                        $file_image->move('uploads',$filename_image);
                        
                    }
                    else{
                         
                        return("please upload the file as per specified");
                    }



        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($type,$id)
    {

        if($type == "image"){
            $images = new Image;
            $images=$images->with('subchannel')->find($id);
            return response()->json($images);
        }
        elseif($type == "video"){
            $videos = new Video;
            $videos=$videos->with('subchannel')->find($id);
            return response()->json($videos);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $type, $id )
    {


      if($type == "image"){


          \Log::info("image is here");
          $thumb = $request->file('image');
          $image = Image::find($id);
          $image->title = $request->title;
          $name = $image->image_id;
          $filename = $name ."thumb" . "." .$image->type;
          \Log::info($filename);

          $link = 'uploads';
          
          if($thumb){
            $s3 = \Storage::disk('s3');
            $filepath = '/uploads/images/' .$filename;
            $s3->put($filepath , fopen($thumb , 'r+' ));
            // $thumb->move($link ,$filename);            
          
          }

          $image->description = $request->description;
          $image->public_private =$request->image_privacy;
          $image->save();
          return response()->json($image);

        }

        elseif($type == "video"){

        $thumb = $request->file('image');

        $video = Video::find($id);
        $video->title = $request->title;
        
        $link = 'uploads'; 
        $filename = $video->video_id . "." . "png";
        
        if($thumb){
          $thumb->move($link,$filename);
        }
        
        $video->description = $request->description;
        $video->public_private =$request->video_privacy;
        $video->save();

        return response()->json($video);
        }
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($type, $id)
    {
        if($type == "image"){
            $image = Image::destroy($id);

        return response()->json($image);
        }

        elseif($type == "video"){
            $video = Video::destroy($id);
            return response()->json($video);

        }
        

    }


    public function data_store(Request $request)
    {
         $user = \Auth::user();
        if ( is_null( $user )){
          return view('home.session_end');
        }
        $subchannel = New SubChannel;
        $channel_id = $request->channel;
        $request->session()->put('channel', $channel_id);
        $sub_channel_name = $request->sub_channel_name;   
        $request->session()->put('sub_channel_name', $sub_channel_name);
        $title = $request->title;
        $request->session()->put('title', $title);
        $description = $request->description;
        $request->session()->put('description', $description);

        $result = $subchannel->where('user_id',"=",$user->id)
                             ->where("channel_id", '=', $channel_id)
                             ->where("sub_channel_name",'=',$sub_channel_name)->get();

        if (! $result->count()){
                             $subchannel->sub_channel_name = $sub_channel_name;
                             $subchannel->channel_id = $channel_id;
                             $subchannel->has_vids = 'yes';
                             $subchannel->user_id = $user->id;
                             $subchannel->save();
                             $sub_channel_id = $subchannel->sub_channel_id;
                             
                             
                                         
        }
        
        else{    
            foreach ($result as $data) {
                            $sub_channel_id = $data->sub_channel_id;
                }
        }
        
        $request->session()->put('sub_channel_id', $sub_channel_id);
        $data = $request->session()->all();

    }

    public function dropzone( Request $request){
        $user=Auth::user();

        if ( is_null( $user )){
          return view('home.session_end');
        }
        
        $form_data = $request->all();
        $photo = $form_data['file'];

        $s3 = \Storage::disk('s3');



        $thumb = "no";
        $type = $photo->getMimeType();
        $orgname = $photo->getClientOriginalName();
        $origextension = $photo->getClientOriginalExtension();
        $nameonly = substr($orgname, 0 , strlen($orgname) - strlen($origextension) -1  );

        $image = new Image ; 
        $name = str_random(7);
        $image->channel_id = session()->get('channel');
        $image->type = $origextension;
        $image->title = session()->get('title');
        $image->description = session()->get('description');
        $image->image_id = $name;
        $image->isthumb = $thumb;

        $image->featured = 'yes';
        $image->user_id = $user->id;
        $image->public_private = session()->get('privacy');
        
        if ( $image->public_private == null) {
            
            $image->public_private = 'yes';
        }
        
        $image->sub_channel_id = session()->get('sub_channel_id');

        $name1=public_path(). "\\".$name."." .$origextension ;
        $filename = $name . '.' .$origextension;
        $thumbname = $name .'thumb'. '.' .$origextension;
        $link = 'uploads';
        $image->save();

        // Thumb for image part please dont change me 

        $img = Imge::make($photo->getRealPath());
        
        $img->resize(450 , 270 , function($constraint){
                                                       $constraint->aspectRatio();
                                                     })->save($link.'/'.$thumbname);

        $resource = $img->stream()->detach();


        $filepath_thumb = '/uploads/images/' .$thumbname;
        $filepath = '/uploads/images/' .$filename;

        $s3->put($filepath_thumb, $resource);
        $s3->put($filepath , fopen($photo , 'r+' ));
        // $photo->move($link,$filename);
               
    }


    public function dropzone2( Request $request){
        $user=Auth::user();


        if ( is_null( $user )){
          return view('home.session_end');
        }

        $s3 = \Storage::disk('s3');
        $form_data = $request->all();
        $photo = $form_data['file'];
        $type = $photo->getMimeType();
        $thumb = "yes";
        $orgname = $photo->getClientOriginalName();
        $origextension = $photo->getClientOriginalExtension();

        $image = new Image ; 
        $video = new Video ;
        $name = str_random(7);
        $image->channel_id = session()->get('channel');
        $image->type = $origextension;
        $image->title = session()->get('title');
        $image->description = session()->get('description');
        $image->image_id = $name;
        $image->isthumb = "yes";
        
        if($type == '360')
        {
          $image->category = '360';

           if ( !empty(session()->get('id'))) {
                
                $image->image_id = session()->get('id');
                $name = session()->get('id');
                // $video->where('video_id' ,'=', $image->image_id )->update(['thumb' => $origextension]);
                session()->forget('id');

            }

            else {
                 
                session()->put('id' , $name);
                session()->put('thumb' ,$origextension );    

            }


        }

        else{

            $image->category = 'plain';
            if ( !empty(session()->get('plainid'))) {
                
                $image->image_id = session()->get('plainid');
                $name = session()->get('plainid');
                // $video->where('video_id' ,'=', $image->image_id )->update(['thumb' => $origextension]);
                session()->forget('plainid');

            }

            else {
                 
                session()->put('plainid' , $name);
                session()->put('thumb' ,$origextension );    

            }
        }

        // if (session()->get('vidid')) {
        //     // get the video which is just uploaded and link that to thumb image
        // }
        
        $image->featured = 'yes';
        $image->user_id = $user->id;
        $image->public_private = session()->get('privacy');
        $image->sub_channel_id = session()->get('sub_channel_id');


        
           
        

         $filename = $name . '.' .'png';
         $link = 'uploads';
         $image->save(); 
         $filepath = '/uploads/' .$filename;
         // $s3->put($filepath , fopen($photo , 'r+' ));
         $photo->move($link,$filename);
          
    
    }



        public function dropzone3( Request $request , $type){

            $user=Auth::user();

            if ( is_null( $user )){
              return view('home.session_end');
            }
           
        $s3 = \Storage::disk('s3');
        $form_data = $request->all();
        $file_video = $form_data['file'];
        // $type = $file_video->getMimeType();
        // $orgname = $file_video->getClientOriginalName();
        $origextension = $file_video->getClientOriginalExtension();
        // $nameonly = substr($orgname, 0 , strlen($orgname) - strlen($origextension) -1  );
        $name = str_random(7);
        // $address = base_path() .'\\' . 'uploads' . '\\'; 
        // $name1=public_path(). "\\".$name."." .$origextension ;

        $ffmpeg = 'C:\\ffmpeg\\bin\\ffmpeg';
        $image_file = 'uploads/' .$name ."." ."png";
        $size = "360x270";
        $getfromsecond = 1;
        $cmd = " ffmpeg -i $file_video -an -ss $getfromsecond -s $size $image_file 2>&1";
        
        
        $video = new Video ; 
        
        $video->channel_id = session()->get('channel');
        $video->type = $origextension;
        $video->title = session()->get('title');
        $video->description = session()->get('description');
        $video->video_id = $name;
        $video->featured = 'yes';
        $video->user_id = $user->id;
        $video->public_private = session()->get('privacy');
        $video->sub_channel_id = session()->get('sub_channel_id');
        $video->thumb ='png';
        if($type == '360')
        {
          
          $video->category = '360';

          if ( !empty(session()->get('id'))) {

                $video->video_id = session()->get('id');
                $name = session()->get('id');
                // $video->thumb = session()->get('thumb');
                session()->forget('thumb');
                session()->forget('id');
                // return (["thumb already exist" , $name]);
            }

            else{

                session()->put('id' , $name);
                session()->put('thumbnail' , 'no');
                // session()->put('command' , $cmd);
                shell_exec($cmd);
                // return (["there is no exist" , $name]);
            }


        
        }

        else{

            $video->category = "plain";
            if ( !empty(session()->get('plainid'))) {

                  $video->video_id = session()->get('plainid');
                  $name = session()->get('plainid');
                  // $video->thumb = session()->get('thumb');
                  session()->forget('thumb');
                  session()->forget('plainid');
                  // return (["thumb already exist" , $name]);
             }


             else{

                session()->put('plainid' , $name);
                session()->put('thumbnail' , 'no');
                // session()->put('command' , $cmd);
                shell_exec($cmd);
                // return (["there is no exist" , $name]);
            }



         }

        


        

        $filename = $name . '.' .$origextension;
        
        $video->channel_id = session()->get('channel');
        $video->type = $origextension;
        $video->title = session()->get('title');
        $video->description = session()->get('description');
        $video->video_id = $name;
        $video->featured = 'yes';
        $video->user_id = $user->id;
        $video->public_private = session()->get('privacy');
        if ( $video->public_private == null) {

            $video->public_private = 'yes';
        }
        

        $video->sub_channel_id = session()->get('sub_channel_id');
        $link = 'uploads';
        $video->save();

        $number_image = $user->videos->count();
        $number_videos = $user->images->count();
        session()->put('images_count' , $number_image);
        session()->put('videos_count' , $number_videos);

        $filepath = '/uploads/videos/' .$filename;
        $s3->put($filepath , fopen($file_video , 'r+' ));

        // $file_video->move($link,$filename);
        $this->xmlcreate($name  , $filename );

    }





    public function xmlcreate($name , $filename){
      
      $xml_file = $name . '.xml';
      $fh = fopen($xml_file, 'w');
      $xml_text = "";
      $xml_text .= ' <krpano>

                <action name="startup" autorun="onstart">
                  
                  if(device.panovideosupport == false,
                    error("Sorry, but panoramic videos are not supported by your current browser!");
                    ,
                    loadscene(videopano);
                    );
                </action>

                <scene name="videopano" title="">
                  
                  <!-- include the videoplayer interface / skin (with VR support) -->
                  <include url="skin/videointerface.xml" />

                  <!-- include the videoplayer plugin -->
                  <plugin name="video"
                          url.html5="%SWFPATH%/plugins/videoplayer.js"
                          url.flash="%SWFPATH%/plugins/videoplayer.swf"
                          pausedonstart="true"
                          loop="true"
                          volume="1.0"
                          onloaded="add_video_sources();"
                          />

                  <!-- use the videoplayer plugin as panoramic image source -->
                  <image>
                    <sphere url="plugin:video" />
                  </image>

                  <!-- set the default view -->
                  <view hlookat="0" vlookat="0" fovtype="DFOV" fov="130" fovmin="75" fovmax="150" distortion="0.0" />

                  <!-- add the video sources and play the video -->
                  <action name="add_video_sources">';

                $xml_text .= "videointerface_addsource('1024x512',";
                $xml_text .=  "'https://s3-us-west-1.amazonaws.com/madisontv/uploads/videos/" ;
                $xml_text .= $filename ;
                $xml_text .= "',";
                $xml_text .=  " '%CURRENTXML%//uploads/" . $name.".png');";
                
                $xml_text .=  '
                      if(device.ios,
                      videointerface_play("1024x512");
                      ,
                      videointerface_play("1024x512");
                      );
                  </action>

                </scene>
              </krpano>';

            fwrite($fh, $xml_text);
            fclose($fh);
            

    }


    public function deleteupload(){


    }

    public function privacy_check( Request $request)
    {
        if ($request->data == 'clearall'){

            $request->session()->put('privacy' , 'yes');
            return $request->session()->all();
        }

        $request->session()->put('privacy',$request->data);
        $request->session()->forget('privacy');
        return $request->session()->all();



    }

    public function user_data(Request $request){

        $user = Auth::user();

        if(!$user){

            return view('home.session_end');
        
        }

        $exist =  session()->get('thumbnail');


        if ($exist == 'no') {
            // return ('there is no thumbnail until now here ');
            // $command = session()->get('command');
            // // shell_exec($command);

            
            session()->forget('command');
            $id = session()->get('id');
            session()->forget('id');
            session()->forget('thumbnail');
            $image =  new Image;
            $image->image_id = $id;
            $image->type = 'png';
            $image->isthumb = 'yes';
            $image->save();

            $user_name1 = $user->first_name;
            $user_name2 = $user->last_name;
            $username = $user_name1 . ' '. $user_name2;
            $number_image = $user->images->count();
            $number_videos = $user->videos->count();
            session(['name'=>$username , 'number_image'=>$number_image , 'number_videos'=>$number_videos]);
          
          }

        // $url = $_SERVER['DOCUMENT_ROOT'] .'/uploads/default_thumbnail.png';
        // $destination_folder = $_SERVER['DOCUMENT_ROOT'].'/uploads/';


        // $newfname = $destination_folder .$id.'.png'; //set your file ext
    
        // $file = fopen ($url, "rb");

        // if ($file) {
        //   $newf = fopen ($newfname, "a"); // to overwrite existing file

        //   if ($newf)
        //   while(!feof($file)) {
        //     fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );

        //   }
        // }

        // if ($file) {
        //   fclose($file);
        // }

        // if ($newf) {
        //   fclose($newf);
        // } 



          


            else{
              session()->forget('id');
              session()->forget('thumbnail');
              return("you already have thumbnail") ;
            }

       

    }


}
