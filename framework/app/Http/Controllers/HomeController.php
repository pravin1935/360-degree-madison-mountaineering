<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Channel;
use App\SubChannel;
use App\Type;
use App\Video;
use Input;
use App\Image;
use App\User;
use App\Collections\Collection;
use Illuminate\Support\Facades\DB;
use Auth;

class HomeController extends Controller
{
    
    public function categories(){

	//return "here";
        $lists = Type::find(1)->channels;
		
        $images = Collection::$images;
		
        $content = view('home.category',compact('lists','images'));
       
        return response($content)->header('Content-Type', 'application/xml');
		

    }


	

	public function playListAll(){
		//return "developing";
		//$limit = 8;
		//$sql = "SELECT * FROM videos WHERE approved = 'yes' AND public_private = 'public' ORDER BY date_uploaded DESC LIMIT $limit";
	//Video::where('channel_id',$channel_id)->orderBy('indexer','DESC')->take('50')->get();

		$videos = Video::where(['approved'=>'yes'])->orderBy('date_uploaded','DESC')->take(8)->get();
		$content = view('home.play_list_all',compact('videos'));
	       
	        return response($content)->header('Content-Type', 'application/xml');


	}

	public function playBackUrl($id,Request $request){
		$utc = $request->get('utc');
		$hash = $request->get('hash');
		$key = '1234';
		$apphash = md5($key.$utc);
		//$newUtc = (new DateTime(gmdate("Y-m-d H:i:s")))->getTimestamp();
		$video = Video::find($id);

		return "http://cloud.truestreamz.com/videochautari/".$video->video_id.".mp4/playlist.m3u8";
	}


	public function jsonCategories(){


	$lists = Type::find(1)->channels;
	$data['categories']['sd_img'] = '/var/www/videosite/images/videochautari.png';
	$data['categories']['hd_img'] = '/var/www/videosite/images/missing.png';
	$data['categories']['category'] = [
			'title'=>'RECENT VIDEOS',
			'description'=>'Recent Videos',
			'feed'=>'http://my.videochautari.com/json/videos-list-total/150',
			'sd_image'=>'http://www.videochautari.com/images/HD-SD/default_SD.png',
			'sd_image'=>'http://www.videochautari.com/images/HD-SD/default_HD.png'
		];
	$images = Collection::$images;
		foreach($lists as $list){
	if(!empty($images[$list['channel_id']]))
		{
			$sd_image = 'http://www.videochautari.com/images/HD-SD/'.$images[$list['channel_id']]['sd_img'].'.png';
			$hd_image = 'http://www.videochautari.com/images/HD-SD/'.$images[$list['channel_id']]['hd_img'].'.png';
		}
		else
		{
			$sd_image = 'http://www.videochautari.com/images/HD-SD/default_SD.png';
			$hd_image = 'http://www.videochautari.com/images/HD-SD/default_HD.png';
		}

		$data['category'][] = [
		'title'=>$list->channel_name,
		'description'=>$list->channel_description,
	'sd_img'=>$sd_image,
	'hd_img'=>$hd_image,
	'leaf'=>[
		'title'=>$list->channel_name,
		'description'=>'',
		'feed'=>"http://my.videochautari.com/json/videos-list-total/$list->channel_id"
		]
			
			];
	}
	return $data;
	        
			
	        
	}

	public function jCategories(){


	$lists = Type::find(1)->channels;

	$data['category'][] = [
			'title'=>'RECENT VIDEOS',
			'description'=>'Recent Videos',
			'sd_image'=>'http://www.videochautari.com/images/HD-SD/default_SD.png',
			'hd_image'=>'http://www.videochautari.com/images/HD-SD/default_HD.png',
			'leaf'=>[
		'title'=>'Recent Videos',
		'description'=>'Recent Videos',
		'feed'=>'http://my.videochautari.com/json/videos-list-total/150',
		]

		];
	$images = Collection::$images;
		foreach($lists as $list){
	if(!empty($images[$list['channel_id']]))
		{
			$sd_image = 'http://www.videochautari.com/images/HD-SD/'.$images[$list['channel_id']]['sd_img'].'.png';
			$hd_image = 'http://www.videochautari.com/images/HD-SD/'.$images[$list['channel_id']]['hd_img'].'.png';
		}
		else
		{
			$sd_image = 'http://www.videochautari.com/images/HD-SD/default_SD.png';
			$hd_image = 'http://www.videochautari.com/images/HD-SD/default_HD.png';
		}

		$data['category'][] = [
		'title'=>$list->channel_name,
		'description'=>$list->channel_description,
	'sd_img'=>$sd_image,
	'hd_img'=>$hd_image,
	'leaf'=>[
		'title'=>$list->channel_name,
		'description'=>'',
		'feed'=>"http://my.videochautari.com/json/videos-list-total/$list->channel_id"
		]
			
			];
	}
	return $data;
	        
			
	        
	    }


	    public function videoListTotal(Request $request){
			$channel_id = $request->get('channel_id');
			$lists = ($channel_id!=150) ? Video::limit(30)->where('channel_id',$channel_id)->orderBy('date_uploaded','DESC')->get()  :  Video::limit(30)->orderBy('date_uploaded','DESC')->get();
			
	        $content = view('home.video-list-total',compact('lists'));
			
	        return response($content)->header('Content-Type', 'text/xml');

	    }

	public function jsonVideoListTotal($channel_id){
	$lists = ($channel_id!=150) ? Video::limit(30)->where('channel_id',$channel_id)->orderBy('date_uploaded','DESC')->get()  :  Video::limit(30)->orderBy('date_uploaded','DESC')->get();
			$data = [];

		foreach($lists as $list){


				$vid = $list->video_id;
			$large_player_thumb = $vid.'.jpg';
			$thumb_file = "uploads/videos/mp4/player_thumbs/$large_player_thumb";

		$data['content'][] = [
		'sdImage'=> "http://www.videochautari.com/".$thumb_file,
		'hdImage'=>"http://www.videochautari.com/".$thumb_file,
		'contentId'=>$vid,
		'title'=>htmlentities($list->title,ENT_COMPAT | ENT_XML1),
		'quality'=>'SD',
		'date_uploaded'=>$list->date_uploaded->format('Y-m-d'),
		'format'=>'mp4',
		'description'=>$list->description,
		'media'=>[
			'views'=>($list->number_of_views==null)?'10':$list->number_of_views,
			'videoLength'=>($list->video_length==null)?'00:05:40':$list->video_length,
			//'streamUrl'=>url('video/playbackurl/'.$list->indexer)
			'streamUrl'=>"http://cloud.truestreamz.com/videochautari/".$list->video_id.".mp4/playlist.m3u8"

		]

	];
		
		
		}

			
	      return $data;
	    }


	public function jsonVideoListTotall($channel_id){
	$lists = ($channel_id!=150) ? Video::limit(30)->where('channel_id',$channel_id)->orderBy('date_uploaded','DESC')->get()  :  Video::limit(30)->orderBy('date_uploaded','DESC')->get();
			$data = [];

		foreach($lists as $list){


				$vid = $list->video_id;
			$large_player_thumb = $vid.'.jpg';
			$thumb_file = "uploads/videos/mp4/player_thumbs/$large_player_thumb";

		$data['content'][] = [
		'sdImage'=> "http://www.videochautari.com/".$thumb_file,
		'hdImage'=>"http://www.videochautari.com/".$thumb_file,
		'contentId'=>$vid,
		'title'=>htmlentities($list->title,ENT_COMPAT | ENT_XML1),
		'quality'=>'SD',
		'date_uploaded'=>$list->date_uploaded->format('Y-m-d'),
		'format'=>'mp4',
		'description'=>$list->description,
		'media'=>[
			'views'=>$list->number_of_views,
			'videoLength'=>$list->video_length,
			'streamUrl'=>url('video/playbackurl/'.$list->indexer)
			//'streamUrl'=>"http://cloud.truestreamz.com/videochautari/".$list->video_id.".mp4/playlist.m3u8"

		]

	];
		
		
		}

			
	      return $data;
	    }


    public function Config(Request $request){
	$version = $request->get('version');
	$current_version = '3.1.0';
	
	$data = ['updateRequired'=>false];
	if($version<$current_version){
	
	$data = ['updateRequired'=>true,'updateLocation'=>'http://my.videochautari.com/'.$current_version.'.apk'];
	}
	return $data;

	}

	public function nonYoutubeCategories(){
		$radios = \Config::get('images');
		$channels = \DB::table('channels')
				->select('channel_id', 'channel_name', 'channel_description')
				->whereIn('channel_id',[19,7,14,20,5,1,13,3,28,19,27,2,29,34,26])
				->orderBy('channel_name')
				->get();
		$content = view('home.non_youtube_category',compact('channels','images'));
       
        return response($content)->header('Content-Type', 'application/xml');

	}

	
	public function nonYoutubeVideos(Request $request){
	$channel_id = $request->get('channel_id');
		//$sql = "SELECT * FROM videos WHERE channel_id = ".$_GET['channel_id']." and embed_id IS NULL ORDER BY indexer DESC LIMIT 50";
		if($channel_id!=150) 
			$videos = Video::where('channel_id',$channel_id)->orderBy('indexer','DESC')->take('50')->get();
		else{
			//"SELECT video_id, title, description,embed_id,indexer,title_seo,number_of_views,video_length FROM videos WHERE embed_id IS NULL  ORDER BY indexer DESC LIMIT 100";
			$videos = Video::orderBy('indexer','desc')->take(100)->get();
		}
		$content = view('home.non_youtube_video',compact('videos','images'));
       
        	return response($content)->header('Content-Type', 'application/xml');


	}

	public function getdata(Request $request)
	{
		
		// return $request;
		$model = new \App\Channel;
		$user = new User;

		$data = Video::where('featured' , 'yes')->where('category' , '!=' , 'plain' )->orderBy('date_uploaded' , "ASC")->paginate(6);

		
		// $data = $model->with(['videos'=>function($query){
		// 								$query->where('category' , '=' , '360')
		// 									  ->orderBy('date_uploaded','DESC');}])->paginate(6);
		// 

		$datas = [];
		foreach ($data as $val) {
				// return ($value->user_id);
				$this_channel = $model->find($val->channel_id);
				$channel = $this_channel->channel_name;
				$date = $val->date_uploaded->diffForHumans();
				$first_name = $user->find($val->user_id)->first_name;
				$datas[] = [
					'channel_name'	=>$channel,
					'title'			=> str_limit($val->title, $limit = 20, $end = '...'),
					'description'	=> str_limit($val->description, $limit = 40, $end = '...'),
					'image'			=> $val->video_id,
					'thumb'         => $val->thumb,
					'indexer'		=> $val->indexer,
					'date_uploaded' => $date,
					'first_name'	=> $first_name
				];
			}

			return response()->json(['data'=>$datas]);
		}

		//return $datas;
		// return response()->json(['data'=>$datas]);



public function videoplain(Request $request)
	{
		
		// return $request;

		$model = new \App\Channel;
		$user = new User;
		$data = $model->with(['videos'=>function($query){
					$query->where('category' , 'like' , 'plain');
					}])->get();
		// return($data);
		$datas = [];
		foreach ($data as $value) {
			$channel = $value->channel_name;
			foreach ($value->videos as $val) {
				$date = $val->date_uploaded->diffForHumans();
				$first_name = $user->find($val->user_id)->first_name;
				$datas[] = [
					'channel_name'	=>$channel,
					'title'			=> str_limit($val->title, $limit = 20, $end = '...'),
					'image'			=> $val->video_id,
					'view'			=> $val->number_of_views,
					'thumb'         => $val->thumb,
					'indexer'		=> $val->indexer,
					'date_uploaded' => $date,
					'first_name'	=> $first_name ,
					'description'	=> str_limit($val->description , $limit=10 , $end = '...')
				];
			}
		}

		//return $datas;
		return response()->json(['data'=>$datas]);

}


public function videolatestplain(Request $request , $section)
	{
		
		// return $request;

		$model = new \App\Channel;
		$user = new User;

		if( $section == 'lvplain')
		{
					$data = $model->with(['videos'=>function($query){
										$query->where('category' , 'like' , 'plain')
											  ->orderBy('date_uploaded','DESC');}])->get();
					
		}

		if( $section == 'mvplain')
		{
					// return('most viewed');
					$data = $model->with(['videos'=>function($query){
													$query->where('category' , 'like' , 'plain')
													->orderBy('number_of_views','ASC');
													}])->get();
					
		}

		if( $section == "fvplain")

		{
					$data = $model->with(['videos'=>function($query){
									$query->where('category' , 'like' , 'plain')
									->orderBy('number_of_views','DESC');
								}])->get();
	    }

		// return($data);
		$datas = [];
		foreach ($data as $value) {
			$channel = $value->channel_name;
			foreach ($value->videos as $val) {
				$date = $val->date_uploaded->diffForHumans();
				$first_name = $user->find($val->user_id)->first_name;
				$datas[] = [
					'channel_name'	=>$channel,
					'title'			=> str_limit($val->title, $limit = 20, $end = '...'),
					'image'			=> $val->video_id,
					'views'			=> $val->number_of_views,
					'thumb'         => $val->thumb,
					'indexer'		=> $val->indexer,
					'date_uploaded' => $date,
					'first_name'	=> $first_name , 
					'description'	=> str_limit($val->description , $limit=10 , $end = '...')
				];
			}
		}

		//return $datas;
		return response()->json(['data'=>$datas]);

}


public function userdata()
	{
			$video = new Video;
			$channel = new Channel; 
			$image = new Image;
			$user = new User;
			$subchannel = new SubChannel;
			$data = $channel->with(['subchannels'=>function($query){
							$query->with(['videos'=>function($query){
										$query->where('user_id','=','1');},
												'image'=>function($query){
										$query->where('user_id','=','1');}
										]);
							}])->get();
				
			//return $data;
			/*$data = $channel->with(['videos'=>function($query){$query->where('user_id','=','0');} , 'images'=>function($query) {$query->where('user_id','=','1');},'subchannels'])->get(); */
			return view('user.user')->with(['data'=>$data]);
	}

	public function search(Request $request)
	{




		$output = "";
		
		$count = strlen($request->search);

		
		if ($request->search != ""  && $count>2 ){
				if(Auth::user())
				{		
						$user = Auth::user();
						$param = $request->search;
				    	$video_suggestions = DB::table('videos')->where(function($q) use ($param){
			    											          		$q->where('title' , 'LIKE' , '%'.$param.'%')
																			->orwhere('description' , 'LIKE' , '%'.$param.'%');
				    											      })
				    											->where(function($q) use ($param , $user){
			    											          		$q->where('public_private' ,'=' , 'yes')
			    											          		->orWhere('user_id' , '=' , $user->id);
				    											      })->get();



				    	$image_suggestions = DB::table('images')->where(function($q) use ($param){
			    											          		$q->where('title' , 'LIKE' , '%'.$param.'%')
																			->orwhere('description' , 'LIKE' , '%'.$param.'%');
				    											      })
				    											->where(function($q) use ($param , $user){
			    											          		$q->where('public_private' ,'=' , 'yes')
			    											          		->orWhere('user_id' , '=' , $user->id);
				    											      })->get();


						
						
						

				}

			else{

						$param = $request->search;							
						$video_suggestions = DB::table('videos')->where('public_private' , 'yes')
				    											->where(function($q) use ($param){
			    											          		$q->where('title' , 'LIKE' , '%'.$param.'%')
																			->orwhere('description' , 'LIKE' , '%'.$param.'%');
				    											      })
																->get();

						
						$image_suggestions = DB::table('images')->where('public_private' , 'yes')
				    									->where(function($q) use ($param){
			    						          		$q->where('title' , 'LIKE' , '%'.$param.'%')
															->orwhere('description' , 'LIKE' , '%'.$param.'%');
				    											      })
																->get();
						
						
			}




			if ($image_suggestions) {
						foreach ($image_suggestions as $key => $image_suggestion) {
						$url_i =  url();

						$url_i = $url_i .'/image/' . $image_suggestion->indexer;
						$output.= '<tr >
										<td class="font-alt" >
											<a href='.$url_i.'><h6>'.$image_suggestion->title.'</h6></a><span >'.substr($image_suggestion->description, 0, 45).'</span>
										</td>
								    </tr>';
	}

					}

					if ($video_suggestions) {
						foreach ($video_suggestions as $key => $video_suggestion) {
						$url_v =url();

						$url_v = $url_v. "/detail/" . $video_suggestion->indexer;
						
						$output .= "<tr>
											<td>
												<a href='".$url_v."'>
												<h6>
													".$video_suggestion->title."
												</h6>
												</a>
												<span>
													".substr($video_suggestion->description , 0, 45)."
												</span>
											</td>
							</tr>";
					}

					}


					return Response($output);


	// 				if ($image_suggestions) {
	// 					foreach ($image_suggestions as $key => $image_suggestion) {
						
	// 					$output.= '<tr >
	// 									<td >
	// 										<a href="image/' .$image_suggestion->indexer.'"><h5>'.$image_suggestion->title.'</h5></a><span >'.$image_suggestion->description.'</span>
	// 									</td>
	// 							    </tr>';
	// }

	// 				}

	// 				if ($video_suggestions) {
	// 					foreach ($video_suggestions as $key => $video_suggestion) {
	// 					$url =url();
						
	// 					$output .= "<tr>
	// 										<td>
	// 											<a href='detail/".$video_suggestion->indexer."'>
	// 											<h5>
	// 												".$video_suggestion->title."
	// 											</h5>
	// 											</a>
	// 											<span>
	// 												".$video_suggestion->description."
	// 											</span>
	// 										</td>
	// 						</tr>";
	// 				}

	// 				}
					return Response($output);
		}


				
		}



		public function suggest(Request $request)
	{

		$output = "";
		
		$tx_data = $request->suggest;
		$count = strlen($tx_data[0]);


		if ($tx_data[0] != ""  && $count>2 ){
				
			

						$param = $tx_data[0];
						$prev_select = $tx_data[1];
						$channel_data = DB::table('channels')->where('channel_name_seo' ,'=', $prev_select)->get();

						
						// return $channel_data[0]->channel_name_seo;
						$name_suggestions = DB::table('sub_channels')->where('sub_channel_name' ,'like' , '%'.$param.'%' )
																	 ->where('channel_id' ,$channel_data[0]->channel_id)
																	 ->get();

						$datum = array();
						
						// return $name_suggestions[0]->sub_channel_name;
						
						foreach ($name_suggestions as $data) 
						{
							$data_in_array = $data->sub_channel_name;
							$datum[] = $data_in_array;
						}

						
						return Response($datum);
		
		}


				
		}




  public function channeloption(Request $request , $option)
  {


		$model = new \App\SubChannel;
		$user = new User;
		$channel_id = $request->channel_id;
		
		$mountain = $model->where('channel_id' , $channel_id);
		if( $option == 'cvideo')
		{
					$data = $mountain->with(['videos'=>function($query){
											$query->where('category' ,  '!=' , 'plain')->orderBy('date_uploaded','DESC');
											} , 'user'])->get();
					
		}

		if( $option == 'cplain')
		{
					$data = $mountain->with(['videos'=>function($query){
													$query->where('category' , 'plain')->orderBy('number_of_views','ASC');
													} , 'user'])->get();
		}

		// $datas = [];
		// foreach ($data as $value) {
		// 	$sub_channel = $value->sub_channel_name;
		// 	foreach ($value->videos as $val) 
		// 	{
		// 		$date = $val->date_uploaded->diffForHumans();
		// 		$first_name = $user->find($val->user_id)->first_name;
		// 		$datas[] = [
		// 			'sub_channel_name'	=> $sub_channel,
		// 			'title'				=> str_limit($val->title, $limit = 20, $end = '...'),
		// 			'image'				=> $val->video_id,
		// 			'views'				=> $val->number_of_views,
		// 			'thumb'        		=> $val->thumb,
		// 			'indexer'			=> $val->indexer,
		// 			'date_uploaded'		=> $date,
		// 			'first_name'		=> $first_name
		// 		];
		// 	}
		// }

		//return $datas;
		return response()->json(['data'=>$data]);



    }
}
