<?php

/* Authentication/Resiter routes */
// Auth::routes();


Route::post('register',[
  'as'=>'post_register',
  'uses'=>'Auth\AuthController@postRegister'
  ]);

Route::get('login',[
  'as'=>'post_login',
  'uses'=>'Auth\AuthController@postLogin'
  ]);



// Route::get('login', ['as' => 'post_login' , function($request){

//   var_dump($request);
// }]);



Route::post('login',[
  'as'=>'post_login',
  'uses'=>'Auth\AuthController@postLogin'
  ]);



Route::get('logout',[ 
  'as'=> 'get_logout', 
  'uses'=> 'Auth\AuthController@getLogout'
  ]);


Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');

Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('/admin' , 'SiteController@adminview');


/*Users profile route */

Route::resource('profile', 'UsersController');

//new pravin
Route::any('display/latestvideos' , ['uses'=>'HomeController@getdata']);
Route::any('display/plainlatestvideos/{section}' , ['uses'=>'HomeController@videolatestplain']);


// Route::any('upload/show' , ['uses'=>'HomeController@userdata']);
// Route::post('upload/add',['uses'=>'uploadcontroller@uploadfile']);

Route::any('upload' , 'UploadsController@input');
Route::post('uploads/store','UploadsController@store');
Route::get('upload/show','UploadsController@index');
Route::delete('uploads/{type}/{id}','UploadsController@destroy');
Route::get('uploads/{type}/{id}','UploadsController@edit');
Route::post('uploads/{type}/{id}','UploadsController@update');

Route::any('search' , ['uses'=>'HomeController@search']);
Route::any('suggest' , ['uses'=>'HomeController@suggest']);

Route::post('privacy' , 'UploadsController@privacy_check');

//croppic controller
Route::get('test','UploadController@getTest');
Route::post('avtupload', 'UploadController@postUpload');
Route::post('avtcrop', 'UploadController@postCrop');

get('/',['uses'=>'SiteController@index']);
// get('index.html',['uses'=>'SiteController@playerCanvas']);

Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

get('image/channel/{Name}' ,'SiteController@channel_image');
get('channel/{channel_name}',['uses'=>'SiteController@channel']);
get('subchannel/{channel_name}',['uses'=>'SiteController@subchannel']);
get('termsofservice',['uses'=>'SiteController@termsofservice']);
get('privacypolicy',['uses'=>'SiteController@privacypolicy']);
get('about-us',['uses'=>'SiteController@aboutUs']);
get('feature-video',['uses'=>'SiteController@featureVideo']);
get('promoted-video',['uses'=>'SiteController@promotedVideo']);
get('detail/{id}',['uses'=>'SiteController@detailVideo']);
get('image/{id}',['uses'=>'SiteController@detailImage']);

get('full/{id}',['uses'=>'SiteController@fullVideo']);
//
post('detail/{id}',['uses'=>'SiteController@detailVideo']);
get('search-videos',['uses'=>'SiteController@searchVideos']);
get('video/playbackurl/{id}','HomeController@playBackUrl');
get('demo',['uses'=>'SiteController@demo']);
get('videos/{id}/{string}',['uses'=>'SiteController@videoById']);
get('channel/subchannel/{channel_id}','\Videochautari\Channel\Controllers\ChannelController@subChannel');



Route::post('display/latestplainvideos' , ['uses'=>'SiteController@subchannelplain']);

Route::any('categories', ['uses'=>'HomeController@categories']);
Route::any('json/categories', ['uses'=>'HomeController@jsonCategories']);
Route::any('json/categories2', ['uses'=>'HomeController@jCategories']);
Route::any('detailplain/{id}',['uses'=>'SiteController@detailplainVideo']);


Route::any('categories_total.php', ['uses'=>'HomeController@categories']);
Route::any('videos_list_total.php', ['uses'=>'HomeController@videoListTotal']);

Route::any('playlist-all.php', ['uses'=>'HomeController@playListAll']);

//categories_total.php jsonVideoListTotal_2
Route::any('json/videos-list-total/{channel_id}', ['uses'=>'HomeController@jsonVideoListTotal']);
Route::any('json/videos-list-total/{channel_id}', ['uses'=>'HomeController@jsonVideoListTotal']);
Route::any('json/videos-list-totall/{channel_id}', ['uses'=>'HomeController@jsonVideoListTotall']);

Route::get('api/categories_nonyoutube',['uses'=>'HomeController@nonYoutubeCategories']);
Route::get('categories_nonyoutube.php',['uses'=>'HomeController@nonYoutubeCategories']);

//
//http://www.videochautari.com/?channel_id=150

Route::get('api/videos_nonyoutube',['uses'=>'HomeController@nonYoutubeVideos']);
Route::get('videos_nonyoutube.php',['uses'=>'HomeController@nonYoutubeVideos']);

Route::any('config', ['uses'=>'HomeController@Config']);



Route::any('videos-list-total/{channel_id}', ['uses'=>'HomeController@videoListTotal']);



// Route::get('admin',function(){
//         return view('admin.auth.login');

// });


Route::get('welcome',function(){
        return "you are welcome!!!";

});

Route::get('dashboard',function(){
    return view('admin.dashboard');
});


Route::post('auth/login', 'AdminController@postLogin');
//Route::post('front/login', 'FrontUserController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
//Route::post('auth/login',function(){
//    return "post";
//});

//Admin section
Route::group(['prefix' => 'dashboard'], function () {
   Route::resource('channels','\Videochautari\Channel\Controllers\ChannelController');
   Route::resource('videos','\Videochautari\Video\Controllers\VideoController');
   Route::resource('images','\Videochautari\Video\Controllers\ImageController');
Route::get('videos\delete\{id}','\Videochautari\Video\Controllers\VideoController@delete');

    Route::resource('types','\Videochautari\Channel\Controllers\TypeController');
    Route::get('channels/{channel_id}/addsubchannel',['uses'=>'\Videochautari\Channel\Controllers\ChannelController@addSubChannel']);
    Route::get('channels/{channel_id}/viewsubchannel',['uses'=>'\Videochautari\Channel\Controllers\ChannelController@viewSubChannel']);
    Route::post('channels/{channel_id}/addsubchannel',['uses'=>'\Videochautari\Channel\Controllers\ChannelController@subchannelStore','as'=>'admin.subchannels.store']);
    Route::post('videos/add-video',['uses'=>'\Videochautari\Video\Controllers\VideoController@addVideo','as'=>'admin.addvideo']);

});

Route::post('file-upload','UploadsController@dropzone');
Route::post('file-upload-thumb/{type}','UploadsController@dropzone2');
Route::post('file-upload-video/{type}','UploadsController@dropzone3');




Route::get('session/upload_store','UploadsController@data_store');
Route::get('session/user','UploadsController@user_data');


Route::get('360image/{name}' , 'SiteController@imagechannel');
Route::get('360video/{name}' , 'SiteController@videochannel');

// auto complete

// Route::get('search',array('as'=>'search','uses'=>'SearchController@search'));
// Route::get('autocomplete',array('as'=>'autocomplete','uses'=>'SearchController@autocomplete'));

Route::get('add/user' , 'UsersController@adduser');
Route::post('add/userdata' , 'UsersController@user_data_add');
Route::get('edit/user' , 'UsersController@edituser');
Route::post('/delete_user' , 'UsersController@deleteuser');


// route for channel butoon option 
 Route::post('display/channeloption/{option}'  , 'HomeController@channeloption');