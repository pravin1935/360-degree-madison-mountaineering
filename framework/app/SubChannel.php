<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubChannel extends Model
{
    protected $table = 'sub_channels';
    protected  $fillable = ['sub_channel_name','sub_type_id','sub_channel_description','date_created','sub_channel_picture','parent_channel_id'];
    public $timestamps = false;
    protected $primaryKey = 'sub_channel_id';
    public $timestamp = false;

    public function images(){
    	return $this->hasMany('App\Image');
    }

    public function videos(){
    	return $this->hasMany('App\Video');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function channel()
    {
        return $this->belongsTo('App\Channel','channel_id');
    }

}
