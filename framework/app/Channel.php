<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $table = 'channels';

 protected $primaryKey = 'channel_id';
    public $timestamp = false;


    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function videos(){
    	return $this->hasMany('App\Video');
    }

    public function images(){
        return $this->hasMany('App\Image');
    }

    public function subchannels(){
    	return $this->hasMany('App\SubChannel');
    }


}
