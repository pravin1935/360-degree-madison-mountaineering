<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	protected $table = 'images';

    protected $primaryKey = 'indexer';

    protected $fillable = ['image_id',
        'type',
        'response_id',
        'channel_id',
        'sub_channel_id',
        'user_id',
        'viewtime',
        'title',
        'title_seo',
        'description',
        'tags',
        'channel',
        'date_uploaded',
        'location_recorded',
        'allow_comments',
        'allow_embedding',
        'allow_ratings',
        'approved',
        'featured',
        'promoted',
        'flag_counter',
        'image_type'
    ];
    public $timestamps = false;

  
    
    public function scopeFlaggedItem($query,$flag,$value){
    	return $query->where($flag,'=',$value);
    }

    public static  function boot(){
        parent::boot();
        static::creating(function($model){
            $model->date_uploaded = date('Y-m-d H:i:s');
        });
    }

 protected $dates = ['date_uploaded'];

    public function channel()
    {
        return $this->belongsTo('App\Channel','channel_id');
    }
    public function comments(){
    	return $this->hasMany('App\Comment','video_id');
    }

    public function subchannel()
    {
        return $this->belongsTo('App\SubChannel','sub_channel_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}

