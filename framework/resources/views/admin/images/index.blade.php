@extends('admin.master')
@section('content')
    <a href="{{url('dashboard/images/create')}}" class="btn btn-primary">Create</a>
    <table class="table table-bordered">
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>date_created</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
        @foreach($images as $image)
            <tr>
                <td>{{$image->title}}</td>
                <td>{{$image->description}}</td>
                <td>{{$image->date_uploaded}}</td>
                <td><img class="img-responsive" width="200" height="200" src="{{asset('uploads/'.$image->image_id.'.'.$image->type)}}" /></td>
                <td>
                    <a href="{{url('dashboard/images/'.$image->indexer.'/edit')}}">Edit</a>
		                <a href="{{url('dashboard/images/delete/'.$image->indexer)}}">Delete</a>

               </td>
            </tr>
        @endforeach
    </table>
{!! $images->render() !!}

@stop