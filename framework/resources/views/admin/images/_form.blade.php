      {!! csrf_field() !!}
      <!--  <div class="form-group">
      <label class="label-control" for="">Video Type</label>

      {!! Form::select('type',[''=>'--Select a format--','mp4'=>'mp4', 'm4v'=>'m4v'],null,['class'=>'form-control']); !!}
      </div> -->
      <div class="form-group">
            <label class="label-control" for="">Channel Name</label>
            {!! Form::select('channel_id',$channels,null,['class'=>'form-control channel_id']); !!}
      </div>


      <div class="form-group">
            <label class="label-control" for="">Channel Name</label>
            {!! Form::select('sub_channel_id',$sub_channels,null,['class'=>'form-control sub_channel_id']); !!}
      </div>

      <div class="form-group">
            <label class="label-control" for="">Title</label>
            {!! Form::text('title',null,['class'=>'form-control']); !!}
      </div>

      <div class="form-group">
            <label class="label-control" for="">Title Seo</label>
            {!! Form::text('title_seo',null,['class'=>'form-control']); !!}
      </div>

      <div class="form-group">
            <label class="label-control" for="">Description</label>
            {!! Form::textarea('description',null,['class'=>'form-control']); !!}
      </div>

      <div class="form-group">
            <label class="label-control" for="">Tags</label>
            {!! Form::textarea('tags',null,['class'=>'form-control tags']); !!}
      </div>

      <div class="form-group">
            <label class="label-control" for="">Feature</label>
            {!! Form::select('featured',['yes'=>'yes','no'=>'no'],null,['class'=>'form-control featured']); !!}
      </div>

      <div class="form-group">
            <label class="label-control" for="">Promoted</label>
            {!! Form::select('promoted',['yes'=>'yes','no'=>'no'],null,['class'=>'form-control promoted']); !!}
      </div>

      
      <div class="form-group">
            <label class="label-control" for="">Choose Image to upload</label>
            <input type="file" name="image">
      </div>

      <div class="form-group">
      <input type="submit" name="submit" value="Save" class="btn btn-danger"/>
      </div>
