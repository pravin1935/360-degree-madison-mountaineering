@extends('admin.master')
@section('content')
   {!! Form::model($channel, array('route' => array('admin.channels.update', $channel->channel_id), 'method' => 'put')) !!}
        @include('admin.channels._form')
   {!! Form::close() !!}
@stop