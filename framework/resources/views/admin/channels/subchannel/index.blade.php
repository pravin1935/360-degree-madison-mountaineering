@extends('admin.master')
@section('content')

    <table class="table table-bordered">
        <tr>
            <th>Channel Name</th>
            <th>channel_description</th>
            <th>date_created</th>
            <th>Action</th>
        </tr>
        @foreach($channels as $channel)
            <tr>
               <td>{{$channel->sub_channel_name}}</td>
               <td>{{$channel->sub_channel_description}}</td>
               <td>{{$channel->date_created}}</td>
               <td>
                    <a href="{{url('admin/channels/'.$channel->channel_id.'/edit')}}">Edit</a>
                    <a href="{{url('admin/channels/'.$channel->channel_id.'/delete')}}">Delete</a>
                    <a href="{{url('admin/channels/'.$channel->channel_id.'/addsubchannel')}}">SubChannel</a>
                    <a href="{{url('admin/channels/'.$channel->channel_id.'/viewsubchannel')}}">ViewChannel</a>
               </td>
            </tr>
        @endforeach
    </table>


@stop