@extends('admin.master')
@section('content')
    <form action="{{url('admin/channels/'.$channel_id.'/addsubchannel')}}" enctype="multipart/form-data" method="post">
        @include('admin.channels.subchannel._form')
    </form>
@stop