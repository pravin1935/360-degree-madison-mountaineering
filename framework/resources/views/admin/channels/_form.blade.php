 {!! csrf_field() !!}
        <div class="form-group">
            <label class="label-control" for="">Channel Name</label>

             {!! Form::text('channel_name',null,['class'=>'form-control']); !!}
        </div>
        <div class="form-group">
            <label class="label-control" for="">Type</label>
            {!! Form::select('type_id', $types,null,['class'=>'form-control']); !!}

        </div>
        <div class="form-group">
            <label class="label-control" for="">channel_name_seo</label>
              {!! Form::text('channel_name_seo',null,['class'=>'form-control']); !!}

        </div>
         <div class="form-group">
            <label class="label-control" for="">Channel Description</label>
             {!! Form::textarea('channel_description',null,['class'=>'form-control']); !!}

        </div>
         <div class="form-group">
            <label class="label-control" for="">Channel Image</label>
            <input type="file" name="sub_channel_picture" class="form-control"/>
         </div>
          <div class="form-group">

             <input type="submit" name="submit" class="btn btn-danger"/>
          </div>
