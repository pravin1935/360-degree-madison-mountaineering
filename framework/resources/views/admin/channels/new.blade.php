@extends('admin.master')
@section('content')
    <form action="{{route('admin.channels.store')}}" enctype="multipart/form-data" method="post">
        @include('admin.channels._form')
    </form>
@stop