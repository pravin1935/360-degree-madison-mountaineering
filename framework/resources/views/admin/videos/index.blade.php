@extends('admin.master')
@section('content')
    <a href="{{url('dashboard/videos/create')}}" class="btn btn-primary">Create</a>
    <table class="table table-bordered">
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>date_created</th>
            <th>Video Length</th>
            <th>Length</th>
            <th>Action</th>
        </tr>
        @foreach($videos as $video)
            <tr>
                <td>{{$video->title}}</td>
                <td>{{$video->description}}</td>
                <td>{{$video->date_uploaded}}</td>
                <td>{{$video->video_length}}</td>
                <td><img class="img-responsive" width="200" height="200" src="{{asset('uploads/'.$video->video_id.'.'.$video->thumb)}}" /></td>
                <td>
                    <a href="{{url('dashboard/videos/'.$video->indexer.'/update')}}">Edit</a>
		                <a href="{{url('dashboard/videos/delete/'.$video->indexer)}}">Delete</a>

               </td>
            </tr>
        @endforeach
    </table>
{!! $videos->render() !!}

@stop