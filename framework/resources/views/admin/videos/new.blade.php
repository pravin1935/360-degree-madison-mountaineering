@extends('admin.master')
@section('css')
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
@stop

@section('content')

<div class="uploading" style="display:none;">
    <img src="{{asset('images/loading.gif')}}" alt=""/>
</div>
    <form id="videoForm" action="{{route('dashboard.videos.store')}}" enctype="multipart/form-data" method="post">
        @include('admin.videos._form')
    </form>


@stop
@section('script')

<script>
$(document).ready(function(){
    $('#browser').on('click',function(){
        $('.uploading').css('display','block');
    });

    $('.channel_id').on('change',function(){
        var channel_id = $(this).val();
console.log(channel_id);
        $.ajax({
            url: "{{url('channel/subchannel')}}/"+channel_id,
            type: "GET",
            data: {channel_id:channel_id},
            success: function(res){
            var option = '';
            if(res.length >= 1){
              $.each(res,function(id,obj){
                    option += "<option value='"+obj.sub_channel_id+"'>";
                    option += obj.sub_channel_name;
                    option += "</option>";
              });

            } else {
                 option += "<option value=''>";
                option += '--Sub channel not added--';
                option += "</option>";
            }


               $('.sub_channel_id').html(option);
            }

        });
    });
});


</script>




@stop