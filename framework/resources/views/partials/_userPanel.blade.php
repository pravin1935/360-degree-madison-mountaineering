<div class="navbar-custom-menu">
  
  <ul class="nav navbar-nav user-content" style="padding-top:17px !important;">
    @if(Auth::check())
    <!-- Messages: style can be found in dropdown.less-->
    <!-- <li class="dropdown messages-menu">
      
      <a href="#" class="dropdown-toggle toggle1" data-toggle="dropdown">
        <i class="fa fa-envelope-o"></i>
        <span class="label label-success">4</span>
      </a>
      <ul class="dropdown-menu menu_side">
        <li class="header">You have 4 messages</li>
        <li>
          
          <ul class="menu">
            <li>
              <a href="#">
                <div class="pull-left">
                
                  <img src="uploads/beauty.jpg" class="img-circle"  alt="User Image">
                </div>
                
                <h4>
                  Support Team
                  <small><i class="fa fa-clock-o"></i> 5 mins</small>
                </h4>
                
                <p>Why not buy a new awesome theme?</p>
              </a>
            </li>
            
          </ul>
          
        </li>
        <li class="footer"><a href="#">See All Messages</a></li>
      </ul>
    </li> -->
    <!-- /.messages-menu -->

    <!-- Notifications Menu -->
    <!-- <li class="dropdown notifications-menu">
     
      <a href="#" class="dropdown-toggle toggle1" data-toggle="dropdown">
        <i class="fa fa-bell-o"></i>
        <span class="label label-warning">10</span>
      </a>
      <ul class="dropdown-menu" style="position:absolute !important;left:auto !important;right: 0px !important;">
        <li class="header">You have 10 notifications</li>
        <li>
          
          <ul class="menu menu_side">
            <li>
              <a href="#">
                <i class="fa fa-users text-aqua"></i> 5 new members joined today
              </a>
            </li>
            
          </ul>
        </li>
        <li class="footer"><a href="#">View all</a></li>
      </ul>
    </li>

    <li class="dropdown tasks-menu">
      <a href="#" class="dropdown-toggle toggle1" data-toggle="dropdown">
        <i class="fa fa-flag-o"></i>
        <span class="label label-danger">9</span>
      </a>
      <ul class="dropdown-menu">
        <li class="header">You have 9 tasks</li>
        <li>
          <ul class="menu menu_side">
            <li>
              <a href="#">
                
                <h3>
                  Design some buttons
                  <small class="pull-right">20%</small>
                </h3>
                <div class="progress xs">
                 
                  <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                    <span class="sr-only">20% Complete</span>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </li>
        <li class="footer">
          <a href="#">View all tasks</a>
        </li>
      </ul>
    </li> -->
    <!-- User Account Menu -->
    <li class="dropdown user user-menu">
      <!-- Menu Toggle Button -->
      <a href="#" class="dropdown-toggle toggle1" data-toggle="dropdown">
        <!-- The user image in the navbar-->
        <img src="{{asset('uploads/avatars/'.Auth::user()->avatar)}}" class="user-image" alt="User Image">
        <!-- hidden-xs hides the username on small devices so only the image appears. -->
        <span style="color: black;" class="font-alt">{{Auth::user()->first_name}} </span>
      </a>
      <ul class="dropdown-menu menu_side">
        <!-- The user image in the menu -->
        <li class="user-header">
          <img src="{{asset('uploads/avatars/'.Auth::user()->avatar)}}" class="img-circle" style= "margin-left:0px !important;"alt="User Image">

          <p class="font-alt">
            {{Auth::user()->first_name}}
            <br>
            <small style="font-size: 11px;">{{Auth::user()->email}}</small>
            
            <small style="font-size: 9px;text-transform: lowercase;">Member since {{Auth::user()->created_at}}</small>

          </p>
        </li>
        <!-- Menu Body -->
        <!-- <li class="user-body">
          <div class="row">
            <div class="col-xs-4 text-center">
              <a href="#">Followers</a>
            </div>
            <div class="col-xs-4 text-center">
              <a href="#">Upgrade</a>
            </div>
            <div class="col-xs-4 text-center">
              <a href="#">Friends</a>
            </div>
          </div>
        </li> -->
        <!-- Menu Footer-->
        <li class="user-footer" style="text-align: center;">
          

          <div class="inlined">
           @if(Auth::check())
                <a href="{{url('upload/show')}}" class="btn btn-warning btn-flat" style="color: white !important;" >Uploads</a>
            @else
               <a href="#" data-toggle="modal" data-target="#myModal" >Upload <i class="fa fa-upload" aria-hidden="true"></i></a>
            @endif
          </div>

         <div class="inlined">
          <a href="{{route('get_logout')}}" class="btn btn-danger btn-flat"  style="color: white !important;">Sign out</a>
        </div>
      </li>
    </ul>
  </li>
  @else

  <!-- <button type="button" class="btn btn-link" id ="register" data-toggle="modal" data-target="#myModal" data-backdrop="static" style="color: white; font-size: 16px;margin-bottom: 8px;border: 2px solid;text-decoration: none;">Join Now</button> -->
  @endif
</ul>

</div>