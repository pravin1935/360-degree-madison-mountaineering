<div class="sections">
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
</div>

<div class="sections">
            <h2 class="heading">Comments ({{count($comments)}})</h2>
    <div class="clearfix"></div>
        <div id="comments">
            @foreach($comments as $comment)
                <div class="media">
                    
                    <div class="media-body">
                        <h4 class="media-heading"><a href="#">{{$comment->name}}</a></h4>
                        <time datetime="2014-05-12T17:02">{{$comment->created_at}}</time>
                        <P>{{$comment->comments}}</P>
                        <div class="clearfix"></div>
                        <a class="btn btn-primary btn-xs backcolor" href="#">Reply</a>
                    </div>
                </div>
            @endforeach
        </div>
</div>
<div class="sections">
    <h2 class="heading">Leave Reply</h2>
    <div class="clearfix"></div>
    <div id="leavereply">
        {!! Form::open() !!}
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Your Name</label>
                        
                        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Your Name']) !!}
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Email Address</label>
                        {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Your Email']) !!}
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Website</label>
                        {!! Form::text('url',null,['class'=>'form-control','placeholder'=>'Your Url']) !!}
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Your Comments</label>
                       
                        {!! Form::textarea('comments',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-primary backcolor">Submit</button>
                </div>
            </div>
       {!! Form::close() !!}
    </div>
</div>