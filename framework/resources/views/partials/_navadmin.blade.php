

<nav class=" navbar-default  navbar-custom" role="navigation">
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              </button>
                            </div>
                            <div class="collapse navbar-collapse" id="navbar-collapse-1">
                              <ul class="nav navbar-nav nav-mega " id="mainnav">
                                <li class="active1"><a href="{{url('/')}}" style="color:#4aa706;" style="text-transform: none;">Home</a></li>
                                <li><span class="seperator">|</span></li>

                                <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">360 IMAGES</a>
                      <ul class="dropdown-menu">

                        @foreach ($shareadd['channel_list'] as $channelname)

                          <li class="dropdown" style="width: 100%"><a class="dropdown-toggle" href="#" data-toggle="dropdown"> {{$channelname->channel_name_seo }}</a>
                          <ul class="dropdown-menu">
                            @foreach($channelname->subchannels as $subchannelname)
                            <li><a href="{{url('360image/'.$subchannelname->sub_channel_name)}}" style="width: 100%;">{{$subchannelname->sub_channel_name}}</a></li>
                            @endforeach
                          </ul>
                        </li>

                        @endforeach
                        
                        </ul>
                    </li>


                              <!--   <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">360 Video  <b class="caret"></b></a> 
                                  <ul class="dropdown-menu" >
                                    <li ><a href="{{url('360video/Travel')}}">Travel</a></li>
                                    
                                    <li><a href="{{url('360video/Events')}}">Events</a></li>
                                    <li class="divider1"></li>
                                    <li ><a href="{{url('360video/Comedy')}}">Comedy</a></li>
                                    <li class="divider1"></li>
                                    <li><a href="{{url('360video/Action')}}">Actions</a></li>
                                    <li class="divider1"></li>
                                    <li >
                                      <a href="{{url('360video/Sports')}}" >Sport</a>
                                    </li>
                                    <li class="divider1"></li>
                                    <li >
                                      <a href="{{url('360video/Music')}}" >Music</a>
                                      
                                    </li>
                                  </ul>
                                </li> -->
                             
 <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">360 VIDEOS</a>
                      <ul class="dropdown-menu">
                        @foreach ($shareadd['channel_list'] as $channelname)

                          <li class="dropdown" style="width: 100%"><a class="dropdown-toggle" href="#" data-toggle="dropdown"> {{$channelname->channel_name_seo }}</a>
                          <ul class="dropdown-menu">
                            @foreach($channelname->subchannels as $subchannelname)
                            <li><a href="{{url('360video/'.$subchannelname->sub_channel_name)}}" style="width: 100%;">{{$subchannelname->sub_channel_name}}</a></li>
                            @endforeach
                          </ul>
                        </li>

                        @endforeach

                    </ul>
                    </li>



                               <!--  <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">360 image  <b class="caret"></b></a>
                                  <ul class="dropdown-menu">
                                    <li ><a href="{{url('360image/Travel')}}"> Travel</a></li>
                                    <li class="divider1"></li>
                                    <li ><a href="{{url('360image/Events')}}">Events</a></li>
                                    <li class="divider1"></li>
                                    <li><a href="{{url('360image/Comedy')}}">Comedy</a></li>
                                    <li class="divider1"></li>
                                    <li><a href="{{url('360image/Action')}}">Action</a></li>
                                    <li class="divider1"></li>
                                    <li class="dropdown ">
                                      <a href="{{url('360image/Sports')}}" class="dropdown-toggle" data-toggle="dropdown">Sports</a>  
                                    </li>
                                    <li class="divider1 "></li>
                                    <li class="dropdown ">
                                      <a href="{{url('360image/Music')}}" class="dropdown-toggle" data-toggle="dropdown">Music</a>
                                    </li>
                                     
                                  </ul>
                                </li> -->
                               
                                <li><a href="#" style="text-transform: none;">CONTACT US <i class="fa fa-rocket" aria-hidden="true" ></i> </a></li>
                   
                                
                                 @if(Auth::check())
                                <li><a href="{{url('upload')}}" style="text-transform: none;" >UPLOAD <i class="fa fa-upload" aria-hidden="true"></i></a></li>
                                  @if(Auth::user()->role == 1)
                                  <li><a href="#" data-toggle="modal" data-target="#myModalas1" style="text-transform: none;"><span class="sign-up-span" > ADD USER  </span></a></li>
                                  @else
                                  @endif

                                @else
                                <!-- <li><a href="#" data-toggle="modal" data-target="#myModal" style="text-transform: none;">Upload <i class="fa fa-upload" aria-hidden="true"></i></a></li> -->
                                @endif
                               
  
                                
                                </ul>

                                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12 search-col" >
                                 @include('partials._search')
                               </div>
                            </div>


                            
                            <!-- /.navbar-collapse -->
                          </nav>