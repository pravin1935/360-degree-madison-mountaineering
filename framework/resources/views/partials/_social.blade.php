<ul class="social" style="padding-left: 0px;">
        
         <li>
          <a href="https://www.facebook.com/MadisonMtng" class="icon_ref">
            <i class="fa fa-facebook" style="color: #3b5998;"></i>
          </a>
        </li>

        <li>
            <a href="https://twitter.com/madisonmtng" class="icon_ref">
              <i class="fa fa-twitter" style="color: #00aced;"></i>
            </a>
         </li>

        <li>
          <a href="https://youtube.com/madisonmtng" class="icon_ref">
            <i class="fa fa-youtube" style="color: #bb0000;"></i>
          </a>
        </li>

        <li>
          <a href="https://instagram.com/madisonmtng" class="icon_ref">
            <i class="fa fa-instagram" style="color: #5e3e23;"></i>
          </a>
        </li>
        
        <li>
          <a href="https://plus.google.com/madisonmtng" class="icon_ref">
            <i class="fa fa-google-plus" style="color: #dd4b39;"></i>
          </a>
        </li>
</ul>