<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title>@yield('title_page')</title>

<!--// Responsive //-->
<meta name="description" content="watch and upload 360 degree video and image (media). You can play and watch video and image on vr online . Upload and watch 360 degree image and video later by uploading by making it public or private on your privacy setting . ">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="True">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="google-site-verification" content="ZCbxF8XyqckugccW_t7mVBE6suqpUrAKwwE3o9sGu2c" />
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link rel="shortcut icon" href="http://madisonmountaineering.com/wp-content/uploads/2014/02/fav.png" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

@yield('metacontent')

<div id="fb-root"></div>

<script>
var baseUrl = "{{url('/')}}";
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '475885515933278',
      xfbml      : true,
      version    : 'v2.6'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));



//   $(document).ready(function(){
//     $('#mainnav li ul').hide()
//     $('#mainnav li').on('mouseenter', function(){
//         $(this).css('background','yellow');
//         $(this).find('ul').slideDown(700);
//     });
//     $('#mainnav li').on('mouseleave', function(){
//         $(this).css('background','none');
//         $(this).find('ul').slideUp(700);
//     });
// });
</script>

<!-- Go to www.addthis.com/dashboard to customize your tools 
<link href="http://vjs.zencdn.net/5.0.0/video-js.css" rel="stylesheet">
-->

<link rel="stylesheet" href="{{asset('css/style.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/AdminLTE.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato:regular:italic:bold:bolditalic|Rosario:regular:italic:bold:bolditalic|Open+Sans:400italic,300,400,600,700&amp;subset=latin,latin-ext">

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Oswald" />
<link rel="stylesheet" href="{{asset('css/jquery.bxslider.css') }}">
<link href="https://fonts.googleapis.com/css?family=Mogra" rel="stylesheet">

<link rel="stylesheet" href="{{asset('assets/owl.carousel.min.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('assets/owl.theme.default.css') }}" rel="stylesheet" />
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{asset('js/slick/slick.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('js/slick/slick-theme.css')}}"/>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">

<link href="{{asset('images/favicon.ico') }}" rel="shortcut icon" type="image/x-icon" />



<!-- List of style for second version change  -->

<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<link href="{{asset('js/lib/animate.css/animate.css') }}" rel="stylesheet">
<link href="{{asset('js/lib/components-font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{asset('js/lib/et-line-font/et-line-font.css') }}" rel="stylesheet">
<link href="{{asset('js/lib/flexslider/flexslider.css') }}" rel="stylesheet">
<link href="{{asset('js/lib/owl.carousel/dist/assets/owl.carousel.min.css') }}" rel="stylesheet">
<link href="{{asset('js/lib/owl.carousel/dist/assets/owl.theme.default.min.css') }}" rel="stylesheet">
<link href="{{asset('js/lib/magnific-popup/dist/magnific-popup.css') }}" rel="stylesheet">
<link href="{{asset('js/lib/simple-text-rotator/simpletextrotator.css') }}" rel="stylesheet">
    <!-- Main stylesheet and color file-->
<link href="{{asset('css/style/style.css') }}"  rel="stylesheet">
<link id="color-scheme" href="{{asset('css/style/colors/default.css') }}"  rel="stylesheet">



<link href="{{asset('css/style/favicon.ico') }}" rel="shortcut icon" type="image/x-icon" />
<link href="{{asset('css/style/favicon.ico') }}" rel="shortcut icon" type="image/x-icon" />


<style>
video {
    width: 100%;
    max-width: 100%;
    width: 100%;
   
}
</style>
<!--<script src="http://vjs.zencdn.net/ie8/1.1.0/videojs-ie8.min.js"></script>-->