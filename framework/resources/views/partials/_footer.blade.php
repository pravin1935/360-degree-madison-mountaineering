<div class="footer container-fluid">
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
                <a href="http://madisonmountaineering.com/">
              <img class="footer_logo percent" src="http://madisonmountaineering.com/wp-content/uploads/2014/03/MM_footer_logo.png" >
            </a>
              
            </div>

            <div class="row">
              <span>Copyright © 2017 Madison Mountaineering</span><br>
              <span>Website by Linster Creative</span>

              <div class="footer_icon">
                  <a class="test" href="#" data-toggle="tooltip" data-placement="top" title="Hooray!" href="https://www.facebook.com/MadisonMtng" data-rel="tooltip" data-position="bottom" data-original-title="Facebook">
                    <i class="fa fa-facebook"></i>
                  </a>

                  <a class="test" href="#" data-toggle="tooltip" data-placement="top" title="Hooray!"  href="https://twitter.com/madisonmtng" data-rel="tooltip" data-position="bottom" data-original-title="Twitter">
                    <i class="fa fa-twitter"></i>
                  </a>

                  <a class="test" href="#" data-toggle="tooltip" data-placement="top" title="Hooray!" href="https://www.youtube.com/user/MadisonMtng" data-rel="tooltip" data-position="bottom" data-original-title="YouTube">
                    <i class="fa fa-youtube"></i>
                  </a>

                  <a class="test" href="#" data-toggle="tooltip" data-placement="top" title="Hooray!"  href="https://instagram.com/madisonmtng" data-rel="tooltip" data-position="bottom" data-original-title="Instagram">
                    <i class="fa fa-instagram"></i>
                  </a>

                  <a class="test" href="#" data-toggle="tooltip" data-placement="top" title="Hooray!"  href="https://plus.google.com/+MadisonMountaineering" data-rel="tooltip" data-position="bottom" data-original-title="Google+">
                    <i class="fa fa-google-plus"></i>
                  </a>

              </div>
            </div>
          </div>
          </div>
        </div>