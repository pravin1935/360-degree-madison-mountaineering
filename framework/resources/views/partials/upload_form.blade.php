@extends('master') @section('metacontent')


<link rel="stylesheet" href="{{asset('css/demo.css')}}">
<link rel="stylesheet" href="{{asset('css/paper-bootstrap-wizard.css')}}">
<link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
  <!-- <link rel="stylesheet" href="dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"> -->
<style type="text/css">

    hr.message-inner-separator{
    clear: both;
    margin-top: 10px;
    margin-bottom: 13px;
    border: 0;
    height: 1px;
    background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
    background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
    background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
    background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}

.sections {
background: linear-gradient(#f1f1ff2e , #c3bfbf4a), url('images/back_star.jpg');
}


</style>
    

 
@stop @section('content')
<div class="sections">

 


   <!--  <h4 class="title">Upload 360 image/video to Paracosma tv</h4>
    <i class="fa fa-cloud-upload image-avatar upload-icon" aria-hidden="true" style="font-size: 48px; margin: 0% 42% 4% 42%;"></i>
    <div class="gap"></div> -->
    <div class="well"><h4 class="upload">Upload 360 image/video to Paracosma tv</h4></div>
    <div class="wizard">
        <div class="wizard-inner">
            <div class="connecting-line"></div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#step1" data-toggle="tab"  class="tab-href" aria-controls="step1" role="tab" title="Step 1">
                        <span class="round-tab">
                                <i class="glyphicon glyphicon-pencil"></i>
                        </span>
                    </a>
                </li>
               
                <li role="presentation" class="disabled">
                    <a href="#step3" class="tab-href" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                        <span class="round-tab">
                                <i class="glyphicon glyphicon-picture"></i>
                        </span>
                    </a>
                </li>
                <li role="presentation" class="disabled">
                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                        <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        
            <div class="tab-content">
                <div class="tab-pane active" role="tabpanel" id="step1">
                    <div class="step1">
                        <div class="col-sm-offset-1 col-sm-10 upload_view_tab">
                        <div class="upload-gap-high"></div>
                        <div class="form">
                            

                            <div class="gap"></div>
                            <div class="row">
                                
                                <div class="col-sm-4 col-xs-12 " style="margin-bottom: 12px;">
                                    <!-- <select name="channel" id="cusSelectbox " >
                                            <option value="1">Travel</option>
                                            <option value="2">Action</option>
                                            <option value="3">Sports</option>
                                            <option value="4">Music</option>
                                            <option value="5">Events</option>
                                            <option value="6">Comedy</option>
                                            <option value="7">Nepali Events</option>
                                        </select> -->
                                   
                                      <select class="" id="channel123" name="channel">
                                            <option class="channel_list" value="1">ASIA</option>
                                            <option class="channel_list" value="2">AUSTRALIA and OCEANIA</option>
                                            <option class="channel_list" value="3">ANTARTICA</option>
                                            <option class="channel_list" value="4">EUROPE</option>
                                            <option class="channel_list" value="5">AFRICA</option>
                                            <option class="channel_list" value="6">SOUTH AMERICA</option>
                                            <option class="channel_list" value="7">NORTH AMERICA</option>
                                            
                                      </select>


                                       
                            </div>

                            <div class="col-sm-4 col-xs-12 dropdown1 ">
                            <div id="page-wrapper1">
                                  
                                  
                                  <div class="group">
                                 
                                  <input type="text" id="default" list="languages" class="form-control group-input pad suggest" placeholder="Name of mountain ">
  
                                <datalist id="languages">
                                    @foreach($subchanneloption as $listitem)
                                    
                                        <option value={{$listitem->sub_channel_name}}></option>

                                    @endforeach
                                
                                </datalist>

                                </div>
                           
                                </div>
                            </div>
                               
                            

                            <div class="col-sm-4 col-xs-12 dropdown1">
                                        <div class="group">
                                            <input type="text" class="form-control group-input pad" placeholder="Title of the File " name="title" id="title1">
                                        </div>
                                        </div>
                            </div>


                            <div class="row">
                                <div class="font-alt">
                                    <h4 class="box-title" style="color: white;">Information</h4>
                                </div>
                                <div class="box-body1">
                                    <div class="row">
                                        
                                        <!-- <div class="col-xs-3">
                                            <select class="form-control privacy" name="privacy" id="privacy">
                                                  <option value="public" >PUBLIC</option>
                                                  
                                                  <option title="private">PRIVATE</option>
                                            </select>

                                        </div> -->
                                    </div>

                                    <!-- <div class="row">
                                        <div class="wizard-card" data-color="red">
                                            <div class="">
                                                <div class="col-sm-3">
                                                    <div class="choice" data-toggle="wizard-checkbox">
                                                        <input type="radio" name="job" value="private">
                                                        <div class="card card-checkboxes card-hover-effect ">
                                                            <i class="fa fa-lock" aria-hidden="true"></i>
                                                            <p>PRIVATE</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="choice active" data-toggle="wizard-checkbox">
                                                        <input type="radio" name="job" value="public">
                                                        <div class="card card-checkboxes card-hover-effect">
                                                            <i class="fa fa-universal-access" aria-hidden="true"></i>
                                                            <p>PUBLIC</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                    <hr>


                                </div>
                                <!-- /.box-body -->
                            </div>
                            <div class="box box-success info_sm ">
                                <div class="box-header">
                                    <h4 class="">Description
                                    </h4>
                                </div>



                                <!-- /.box-header -->
                                <div class="form-group ">
                                      <textarea class="form-control" rows="5" id="description1" name="description"></textarea>
                                </div>
                            </div>

                            <h6 class="font-alt" style="color: white;">Detail text to descibe the event or file </h6>
                            <div id="summernote"></div>
                            </div>
                        </div>



                   <ul class="list-inline pull-right">
                    <div class="upload-gap-high"></div>
                                <li class="right">
                                    <button type="button" class="btn btn-primary next-step" disabled="disabled" >Save and continue</button>
                                </li>
                   </ul>
                    
                </div>
              </div>
                <div class="tab-pane" role="tabpanel" id="step3">
                    <div class="step33">
                    <div class="col-md-12 upload-drop">
                        <span class="upload font-alt">Select image file</span>
                        <div class="gap"></div>







                        <form action="{{url('file-upload')}}"
                              class="dropzone"
                              id="my-awesome-dropzone-image" method="POST">

                              <div class="fallback">
                                    <input name="file" type="file"  />
                              </div>
                        </form>
                    </div>
                    
                    <div class="video_upload_section">
                        <div class="video_upload">
                            <div class="col-md-6 upload-drop">
                                <span class="upload font-alt">Select Video file</span>
                                <div class="gap"></div>
                                <form action="{{url('file-upload-video')}}" class="dropzone" id="my-awesome-dropzone-video">

                                     <div class="fallback">
                                            <input name="file2" type="file"  />
                                      </div>
                                </form>

                            </div>

                            <div class="col-md-6 upload-drop">
                                <span class="upload font-alt">Select Thumbnail image of video </span>
                                <div class="gap"></div>
                                <form action="{{url('/file-upload-thumb')}}"
                                      class="dropzone"
                                      id="my-awesome-dropzone-thumb" >
                                </form>
                                <span id="newimage"></span>
                            </div>
                        </div>

                        </div>
                         <!-- <button type="button" class="btn btn-primary add_more">ADD +</button> -->

                    </div>
                    <ul class="list-inline pull-right">
                        <!-- <li>
                            <button type="button" class="btn btn-default prev-step">Previous</button>
                        </li> -->
                        <li>
                            <button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane" role="tabpanel" id="complete">
                    <div class="step44">
                        <div class="container">
                            <div class="row">
                           <div class="col-md-12 text-center ">
                            <div class="panel panel-default">
                              <div class="userprofile social ">
                                <div class="userpic"> <img src="https://bootdey.com/img/Content/avatar/avatar6.png" alt="" class="userpicimg"> </div>
                                <h3 class="username">{{\Session::get('name')}}</h3>
                                <p>pravin@paracosma.com</p>
                                
                              </div>

                              <div class="col-md-12 border-top border-bottom">

                                <h2 class="module-title font-alt">
                                    Thanks for your Help  , support and effort to make this better .
                                </h2>
                                <!-- <ul class="margin_ul" role="tablist">
                                  <li role="presentation">
                                    <h3><i class="fa fa-file-video-o space3" style="color: #167fa9;" aria-hidden="true"></i>
                                        {{\Session::get('number_videos')}}
<br>
                                      <small class="under_line">Videos</small> </h3>
                                  </li>
                                  <li role="presentation">
                                    <h3><i class="fa fa-file-picture-o space3" style="color: #167fa9;" aria-hidden="true"></i>
                                        {{\Session::get('number_image')}}<br>
                                      <small class="under_line">Images</small> </h3>
                                  </li>
                                  
                                </ul> -->
                                
                                
                              </div>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                        </div>

                </div>
                <div class="clearfix"></div>
            </div>
    </div>

</div>
<script type="text/javascript">


   

    var userClicked = false;
        $( window ).unload(function() {

            var answer = confirm("you wanna <b> leave </b>");
                if (answer) {
                    alert("hello");
                    // $.ajax({
                    //   method: "GET",
                    //   url: "{{url('session/user')}}",
                    //   data: 'data'
                    // })
                    //   .done(function( msg ) {
                    //     alert( "Data Saved: " + msg );
                    //   });
                
                }
                else {
                alert("sorry my mistake ")
                }
        });

   


    $(document).ready(function() {
      
        


            console.log("sample space");
           
            // Dropzone.options.myAwesomeDropzoneThumb = {
            //     accept: function(file, done) {
            //         console.log(file);
            //         if (file.type != "image/jpeg") {
            //             done("Error! Files of this type are not accepted");
            //         console.log("this is errror");
            //         }
            //         else { done(); }
            //     }
            // }

            // Dropzone.options.myAwesomeDropzoneThumb = {
            //     paramName: "file2",
            //     maxFilesize: 1,
            //     acceptedFiles : 'image/*' , 
                
            //     init: function(){
            //         this.on("complete" , function(data){
            //             var res = eval('(' + data.xhr.responseText + ')');
            //             $('#newimage').text(res.message);
            //         });

            //         this.on('maxfilesexceeded' , function(data){
            //             this.removeFile(data);
            //         });
            //     }


            // };

          });

</script>

@stop @section('sidebar') @include('home.sidebar') @stop