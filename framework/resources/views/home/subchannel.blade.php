@extends('master')
@section('content')

                 <div class="sections">
                 
                        <div class="subchannelplainlink">
                            @if( !empty($videos))
                            <div class="upload-gap"></div>
                            <h2 class="heading font-alt">Videos List By Categories</h2>
                            <hr>
                            <ul class=" nav-tabs" style="display: inline-block; margin-left:3px; position:relative;  background: none; top: 17px;">
                                  
                                  <li class="subchanneltab1" ><a class="ajax btn" href="#"><span  style="color:#222222; cursor: pointer;" >360-video</span></a></li>
                                  <li class="subchanneltab2" id="subchannelplain" data-videoid= {{$sub_channel_id}} ><a class="ajax btn" href="`#"><span style="color: red;" >plain Video</span></a>

                                  </li>
                        </ul>


                            <div class="clearfix"></div>
                            <div class="subchannelplain">
                            </div>
                            <div class="subchannel360">
                            <div class="row post-columns"> 
                            @foreach($videos->chunk(6) as $chunk) 
                                @foreach($chunk as $video)   
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12  "> 
                                        <!-- Video Box Start -->
                                        <div class="videobox2 card" style="background: white;">
                                            <figure> 
                                                <!-- Video Thumbnail Start --> 
                                                <a href="{{url("detail/$video->indexer")}}">
                                                
                                                    <img src="{{ asset('uploads/'.$video->video_id.'.'.$video->thumb) }}" class="img-responsive hovereffect" 
                                                    style="height:200px;"  alt="" />
                                                
                                                </a> 
                                                <!-- Video Thumbnail End --> 
                                                <!-- {{-- Video Info Start --}}
                                                {{-- div class="vidopts">
                                                    <ul>
                                                        <li><i class="fa fa-heart"></i>{{$video->number_of_views}}</li>
                                                        <li><i class="fa fa-clock-o"></i>{{$video->video_length}}</li>
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                </div> --}}
                                                {{-- Video Info End  --}} -->
                                            </figure>
                                            <!-- Video Title Start -->
                                            <h4 class="font-alt"><a href="{{url("detail/$video->indexer")}}">{{str_limit($video->title,30)}}</a></h4>
                                            <!-- Video Title End --> 
                                        </div>
                                        <!-- Video Box End --> 
                                    </div>
                                
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                        <!-- <div class="row"> 
                            @foreach($videos->chunk(6) as $chunk)
                             
                            @foreach($chunk as $video)  

                            <ul class="categories visible-xs">
                                <li style="padding: 5px 10px; border-bottom: 1px solid #fff; margin-bootm: 4px; font-size: 16px;">
                                <a style="font-weight: normal; font-size: 14px; color: #1F97D4" href="{{url("detail/$video->indexer")}}">{{str_limit($video->title,35)}}</a></li>
                            </ul>

                        @endforeach
                        @endforeach

                       </div> -->

                            @endif
                        </div>
                          

                          @if( !empty($images))
                            <div class="upload-gap"></div>
                            <h2 class="heading font-alt">Images List By Categories</h2>
                            <hr>
                            <div class="clearfix"></div>
                            <div class="row"> 
                            @foreach($images->chunk(6) as $chunk)
                             
                            @foreach($chunk as $image)   
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12  "> 
                                <!-- Video Box Start -->
                                <div class="videobox2 card" style="background: white;">
                                    <figure> 
                                        <!-- Video Thumbnail Start --> 
                                        <a href="{{url("image/$image->indexer")}}">
                                            <img src="{{ asset('uploads/'.$image->image_id.'.'.$image->type) }}" class="img-responsive hovereffect" style="height:200px;"  alt="" />
                                        </a> 
                                        <!-- Video Thumbnail End --> 
                                       
                                    </figure>
                                    <!-- Video Title Start -->
                                    <h4><a href="{{url("image/$image->indexer")}}">{{str_limit($image->title,30)}}</a></h4>
                                    <!-- Video Title End --> 
                                </div>
                                <!-- Video Box End --> 
                            </div>
                            
                            @endforeach    
                            @endforeach
                        </div>

                        <!-- <div class="row">
                             @foreach($images->chunk(6) as $chunk)
                             
                            @foreach($chunk as $image)  
                            <ul class="categories visible-xs">
                                <li style="padding: 5px 10px; border-bottom: 1px solid #fff; margin-bootm: 4px; font-size: 16px;">
                                <a style="font-weight: normal; font-size: 14px; color: #1F97D4;" 
                                href="{{url("image/$image->indexer")}}">{{str_limit($image->title,35)}}</a></li>
                            </ul>
                            @endforeach
                        @endforeach
                        </div> -->

                        

                            @endif
                       
                    </div> 
@stop
@section('sidebar')

@include('home.sidebar')

@stop