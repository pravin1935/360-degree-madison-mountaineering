<!DOCTYPE html>
<html lang="en">

<head>
<script type="text/javascript">var baseURL = "{{url('/')}}";</script>
@include('partials._head')
<!--<script src="http://vjs.zencdn.net/ie8/1.1.0/videojs-ie8.min.js"></script>-->
@yield('css')

<style type="text/css">
  .one {
            background: url('http://res.cloudinary.com/dxssokt4h/image/upload/v1510575871/one_uj1ubs.jpg') center no-repeat;
            height: 600px;
            animation: change .5s infinite;
        }
        
        @keyframes change {
            0%,
            20% {
                background: url('http://res.cloudinary.com/dxssokt4h/image/upload/v1510575871/one_uj1ubs.jpg') center no-repeat;
            }
            21%,
            40% {
                background: url('http://res.cloudinary.com/dxssokt4h/image/upload/v1510575870/two_yjigkh.jpg') center no-repeat;
            }
            41%,
            60% {
                background: url('http://res.cloudinary.com/dxssokt4h/image/upload/v1510575871/three_dvhqjq.jpg') center no-repeat;
            }
            61%,
            80% {
                background: url('http://res.cloudinary.com/dxssokt4h/image/upload/v1510575870/two_yjigkh.jpg') center no-repeat;
            }
            80%,
            100% {
                background: url('http://res.cloudinary.com/dxssokt4h/image/upload/v1510575871/five_tuf8ob.jpg') center no-repeat;
            }
        }
</style>
</head>
 <body >

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
$(function() {


 
    function readURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#blah2').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#imgInp2").change(function() {

      console.log('image changed');
      readURL(this);
    
    });

});
 

$(document).ready(function(){
    
});
    
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '2013056728933412',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();   
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Wrapper Start -->
<div class="wrapper"> 
    <!-- Header Start -->
     <header>
        <!-- Header Top Strip Start -->
        
        <!-- Header Top Strip End -->
        <!-- Logo + Search + Advertisment Start -->
      <div class="logobar">
            <div class="custom-container">
                <div class="row">
                    <!-- Logo Start -->
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-7 ">
                        <div class="logo">
                            <a href="{{url('/')}}"><img class="image-logo" src="{{ asset('images/logo/eee.png') }}"   alt="paracosmatv" /></a>
                        </div>
                    </div>


                    
                    <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 nav-menu" >
                          <div id="navbar" class="nav-col">
                         @include('partials._navadmin')
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-2 col-sm-12 col-xs-6">
                        @include('partials._userPaneladmin')
                    </div>
                </div>
  
            </div>
        </div>
        <!-- Logo + Search + Advertisment End -->
       
    </header class='main-header'>

 <div class="modal fade" id="myModal" role="dialog" tabindex="-1"  hidden="true" style="background-image: url(/paracosmatv/uploads/form-back.jpg);">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Welcome To Pracosma tv</h4>
        </div>
        <div class="modal-body">
              <div class="row">    
        <div id="loginbox"  class="mainbox col-md-12 col-md-offset-0 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div style="padding:5px 0px 0px 0px;" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                        <form id="loginForm" class="form-horizontal" role="form" method="POST" action = "{{route('post_login')}}">
            {{ csrf_field()}}
                            <figure style="text-align: center;">
                              <img src="{{asset('uploads/img_avatar.png')}}" class="img-circle image-avatar" />
                            </figure>
                                    
                            <div style="margin-bottom: 25px" class="input-group form-group" id="email-div">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="email" type="email" class="form-control" name="email" value="" placeholder="username or email" title="Please enter your email" required="">
                                        {{-- <div class="help-block">
                                          <strong id="form-errors-email"></strong>
                                        </div>
                                        <span class="help-block small">Your email</span> --}}                                        
                            </div>
                                
                            <div style="margin-bottom: 25px" class="input-group form-group" id="password-div">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input title="Please enter your password" id="password" type="password" class="form-control" name="password" placeholder="********" value="" required="">
{{-- 
                                        <div class="help-block">
                                          <strong id="form-errors-password"></strong>
                                        </div>
                                        <span class="help-block small">Your strong password</span> --}}
                            </div>

                            <div class="form-group" id="login-errors">
                              <div class="help-block">
                                <strong id="form-login-errors"></strong>
                              </div>
                            </div>

                            <div>
                            <div class="help-block" style="color:#A52A2A">
                              <div id="form-errors-email"></div><br>
                              <div id="form-errors-password"></div><br>
                                <div id="form-login-errors"></div>
                              </div>
                            </div>
                               
                            <div class="input-group form-group"">
                                      <div class="checkbox">
                                        <label>
                                          <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                        </label>
                                      </div>
                            </div>


                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">

                                      <input type="submit" id="sign_in" value="Sign In" class="btn btn-success">
                                      <div style="font-size: 100%;position: relative;top: 3px;border-bottom: 1px solid; margin-left: 12px;display: inline-block;"></div>
                                    </div>
                                </div>

  
                        </form>     



                        </div>                     
                    </div>  
        </div>




    </div>
    
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


<div class="modal fade" id="myModalas1" role="dialog" tabindex="-1"  hidden="true" style="background-image: url(/paracosmatv/uploads/form-back.jpg);">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title font-alt">THIS IS FORM TO ADD ADMIN !!!!</h4>
        </div>
        <div class="modal-body">
              <div class="row">    
        <div id="loginbox"  class="mainbox col-md-12 col-md-offset-0 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title font-alt">NEW MEMBER TO JOIN </div>
                            
                        </div>  
                        <div class="panel-body" >
                            <form id="registerForm" class="form-horizontal" role="form" method="POST" action="{{route('post_register')}}">
              {{ csrf_field()}}
                                
                                <div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                    <span></span>
                                </div>
                                    
                                
                                  
                                <div class="form-group" id="register-first-name">
                                    <label for="firstname" class="col-md-3 control-label">First Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="eg:John" required="" title="Please enter you name">
                                        <span class="help-block">
                                          <strong id="register-errors-first-name"></strong>
                                        </span>
                                        <span class="help-block small">Your first name</span>
                                    </div>
                                </div>
                                <div class="form-group" id="register-last-name">
                                    <label for="lastname" class="col-md-3 control-label">Last Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="eg:Poudel" required="" title="Please enter you name" value="">
                                        <span class="help-block">
                                          <strong id="register-errors-last-name"></strong>
                                        </span>
                                        <span class="help-block small">Your Last name</span>
                                    </div>
                                </div>

                                <div class="form-group" id="register-email">
                                    <label for="email" class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="eg:example@mail.com" required="" title="Please enter you email" value="">
                                        <span class="help-block">
                                        <strong id="register-errors-email"></strong>
                                        </span> 
                                        <span class="help-block small">Your email</span>
                                    </div>
                                </div>
                                    
                                
                                <div class="form-group" id="register-password">
                                    <label for="password" class="col-md-3 control-label">Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="password" placeholder="eg:X8dfl90E0" required="" title="Please enter your password" value="">
                                        <span class="help-block">
                                        <strong id="register-errors-password"></strong>
                                        </span> 
                                        <span class="help-block small">Your strong password</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">Confirm Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="eg:X8dfl90E0">
                                        <span class="help-block">
                                        <strong id="form-errors-password-confirm"></strong>
                                        </span> 
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="icode" class="col-md-3 control-label">Invitation Code</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="icode" placeholder="eg:f0l9X8dE0">
                                    </div>
                                </div>
                                <div class="form-group" id="login-errors">
                                  <span class="help-block"><strong id="form-login-errors"></strong></span>
                                </div>

                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Sign Up</button>
                                        <span style="margin-left:8px;">or</span>  
                                    </div>
                                </div>
                                
                                <!-- <div style="border-top: 1px solid #999; padding-top:10px"  class="form-group">
                                    
                                    <div>&nbsp</div>
                                    <div class="col-md-offset-3 col-md-9" style="padding: 0px;">
                                        <a href="redirect"><button id="btn-signup"type="button" class="btn btn-primary"> <i class="fa fa-facebook-official space" aria-hidden="true"></i>Log in with facebook</button></a>   
                                        <button id="btn-fbsignup" type="button" class="btn btn-danger" ><i class="fa fa-google" aria-hidden="true"></i> Sign Up with Google</button>
                                    </div>                                         
                                        
                                </div> -->
                                
                                
                                
                            </form>
                         </div>
                    </div>
        </div>




       
    </div>
    
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>







<div id="editform" class="modal fade" role="dialog" style="z-index: 9900;">
  <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Form </h4>
        </div>
        <div class="modal-body">
              <div class="container-fluid">    
                <div id="loginbox"  class="mainbox ">                    
                    <div class="panel panel-info" >
                            <div style="padding:5px 0px 0px 0px;" class="panel-body" >

                                <div style="display:none;"  id="login-alert" class="alert alert-danger col-sm-12"></div>
                                    
                                <form id="upload_form" name = "upload_form" action="#" enctype="multipart/form-data" method="PUT" class="form-horizontal" role="form">
                                    
                                    <i class="fa fa-cloud-upload image-avatar" aria-hidden="true"  style="font-size: 48px; margin: 0% 42% 4% 42%;"></i>
                                    
                                    <!-- <div style="margin-bottom: 25px" class="input-group"> -->
                                               <!--  <span class="input-group-addon"><i class="fa fa-tag" aria-hidden="true"></i></span>
                                                <button type="button" class="btn btn-success" data-target="#channel_menu" data-toggle="collapse" style="margin-right: 8px;">
                                                  <span class="option">Channel</span><span class="caret"></span>
                                                </button> -->
                                               <!--  <i class="fa fa-pencil-square-o space" aria-hidden="true" style="font-size: 15px;position: relative;top:2px;"></i>
                                                <input type="text" id="subchannel" name="subchannel" style="width: 70%; height: 35px; border:none; border-bottom: 1px solid;" placeholder="Name of your album " value=""/> -->
                                               <!--  <div class="collapse" id="channel_menu">
                                                    <select class="collapse1" role="menu" id ="channel" name="channel" style="width: 100%; padding: 1.5% 0%;" value="">
                      
                                                              <option value="1">Travel</option>
                                                              <option value="2">Action</option>
                                                              <option value="3">Sports</option>
                                                              <option value="4">Music</option>
                                                              <option value="5">Events</option>
                                                              <option value="6">Comedy</option>
                                                              <option value="7">Nepali Events</option>

                                                    </select>
                                                </div> -->

                                    <!-- </div> -->
                                    
                                    <div style="margin-bottom: 25px" class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" id = "title" name="title" placeholder="Title of the File" value="" style="border: 2px solid  #6b6b6b;" />
                                   </div>

                                   <div style="margin-bottom: 25px" class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" id="description"  name="description" placeholder="Information about File " value="" style="border: 2px solid  #6b6b6b;"/>
                                   </div>

                                   <input type='file' name="thumbput" id="imgInp2" />
                                        <img id="blah2" src="#" alt="Browse image for thumbnail to change thumbnail" style="text-align: center;max-width: 90%;font-size: 16px;font-family: Roboto Condensed, sans-serif; border-bottom:2px solid #ff8201;" 
                                         />
                                    
                                    <div class="margin-bottom"></div>
                                                   <!--  <div class="privacy1">
                                                        <select class="collapse1 privacy-select" style="padding: 1.5% 0% !important; width: 27%;" role="menu" name="privacy" id ="video_privacy" >
                          
                                                                  <option value="yes">public</option>
                                                                  <option value="no">private</option>            
                                                       </select>
                                                   </div> -->
                                                    <!-- <input type = "file" name="video"  id ="video" style="margin: 0% 43%;color: red;"> <br> -->
                                    
                                                    <hr>
                                                    <br>
                                                    <!-- <input type = "file" name="video"  id ="video" style="margin: 0% 43%;color: red;"> <br> -->
                                                    <p class = "help-block">All this field are assets for your file so you could easily search and locate in future.</p> <hr style="border-top: 1px solid #969696;">

                                                    
                                                    <button type="submit" id="submit" class="btn btn-danger" >submit</button>


                                                   
                                                    <h3 id="status"></h3>
                                                    <p id="loaded_n_total"></p>
                                                    </form> 
                                                    </div>
                                    </div>

                                        



                                </div>                     
                            </div>  
                </div>
              </div>
        </div>
</div>











  





  






    <!-- Header End -->
    
    <!-- Contents Start -->
    
    <div class="">
        <div class="custom-container contents">
            <div class="row flex-container"> 
           
               @yield('breadcumb')
                <!-- Bread Crumb End -->
                <!-- Content Column Start -->

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 slider-360 first" style="padding-right: 12px !important;padding-left:0px !important;">
                @if(Auth::user())
                    <h2 class="font-alt center">HERE WE ARE ARE <b>{{Auth::user()->first_name}} </b>!!!!!</h2>
                    <h4 class="font-alt center">Let's  <b>Go</b>  !!!</h4>


                @else
                 <h2 class="font-alt center">LET'S GET IN ADMIN !!!!!</h2>
                   <h4 class="font-alt center">Click  <b>JOIN NOW</b> BUTTON</h4>


                @endif
                <div class="row">
                  <div class="col-md-4 hidden-md hidden-xs">
                    <div class="one">
                     </div>
                  </div>

                  <div class="col-md-8 col-sm-12" style="overflow-y: scroll;height: 333px;position: relative;right: 10px;">
                    <hr>
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th class="font-alt">Firstname</th>
                            <th class="font-alt">Lastname</th>
                            <th class="font-alt">Email</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($user as $user1)
                          <tr id="trow{{$user1->id}}">
                            <td class="font-alt">{{$user1->first_name}}</td>
                            <td class="font-alt">{{$user1->last_name}}</td>
                            <td class="font-alt">{{$user1->email}}</td>
                            <td><button data-list_id="{{$user1->id}}" class="btn btn-danger user_delete" >DELETE</button></td>
                          </tr>
                          @endforeach
                        
                        </tbody>
                      </table>
                  </div>
                </div>
                     


                </div>


                <div class="">
       
        <div class=" col-lg-offset-1 col-lg-2 col-md-2 col-sm-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua card " style="box-shadow:2px 15px 22px #afa5a5">
            <div class="inner">
              <h3 class="font-alt">{{ $shareadd['videocount'] }}</h3>

              <p class="font-alt" style="color: white;" >VIDEOS</p>
            </div>
            <div class="icon" style="font-size: 40px;">
              <i class="fa fa-video-camera"></i>
            </div>
            
          </div>
        </div>


        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua card" style="box-shadow:2px 15px 22px #afa5a5" >
            <div class="inner">
              <h3 class="font-alt">{{ $shareadd['videoview'] }}</h3>

              <p class="font-alt" style="color: white;" >VIDEOS VIEWS</p>
            </div>
            <div class="icon" style="font-size: 40px;">
              <i class="fa fa-eye"></i>
            </div>
            
          </div>
        </div>


        <!-- ./col -->
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green card" style="box-shadow:2px 15px 22px #afa5a5" >
            <div class="inner">
              <h3 class="font-alt">{{ $shareadd['imagecount'] }}<sup style="font-size: 20px"></sup></h3>

              <p class="font-alt" style="color: white;" >IMAGES</p>
            </div>
            <div class="icon" style="font-size: 40px;" >
              <i class="fa fa-picture-o" style="font-size: 40px;" ></i>
            </div>
            
          </div>
        </div>


        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green card" style="box-shadow:2px 15px 22px #afa5a5" >
            <div class="inner">
              <h3 class="font-alt">{{ $shareadd['imageview'] }}<sup style="font-size: 20px"></sup></h3>

              <p class="font-alt" style="color: white;">IMAGES VIEWS</p>
            </div>
            <div class="icon" style="font-size: 40px;" >
              <i class="fa fa-eye" ></i>
            </div>
            
          </div>
        </div>


        <!-- ./col -->
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow card" style="box-shadow:2px 15px 22px #afa5a5" >
            <div class="inner">
              <h3>{{ $shareadd['usercount'] }}</h3>

              <p class="font-alt" style="color: white;">REGISTERED ADMIN</p>
            </div>
            <div class="icon" style="font-size: 40px;" >
              <i class="fa fa-user-plus"  ></i>
            </div>
            
          </div>
        </div>


        

        <!-- ./col -->
        <!-- <div class="col-lg-3 col-xs-6">
          
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div> -->
       
      </div>

                <!-- Content Column End --> 
                <!-- Gray Sidebar Start -->
                
                <!-- Gray Sidebar End --> 
            </div>
        </div>
    </div>
    <!-- Contents End -->
  
  <!--- -->
  
    <div class="custom-container ">
  <!--- -->
    <!-- Footer Start -->
   @include('partials._footer')
    </div>
<!-- Wrapper End --> 
<!--// Javascript //-->
@include('partials._js')
@yield('js')

<script type="text/javascript">
  $('.user_delete').click(function(){

     var entry = confirm("want to delete one of your user ???" );
      //console.log('helloo');
      if(entry){
        var $user_id = $(this).data('list_id');

        $()

      $.ajax({
      method: "POST",
      url:"{{url('/delete_user')}}",
      data: {'user_id':$user_id}
    })

      .done(function( msg ) {
        alert( "Data Saved: " + $user_id );
        
        console.log(row_id);
        
        // $(row_id).hide();
        // console.log('tr'+'#trow' . $user_id);

        // console.log($('button').data($user_id).parent().parent());
      });

      

      }
      location.reload(true);
      

    });
</script>
</body>
</html>