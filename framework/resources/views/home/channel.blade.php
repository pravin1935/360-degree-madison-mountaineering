@extends('master')
@section('content')

                 <div class="sections">
    
                        <ul class="nav nav-tabs" style="display: inline-block; margin-left:3px; position:relative;  background: none; top: 82px;margin-bottom: 65px;">
                                  <li  id="cimage" ><a class="ajax btn font-alt" href="#"><span  style="color:#222222; cursor: pointer;" >360-Image</span></a></li>
                                  <li class="limage1 ctab" id="cvideo" data-videoid = {{$channel_name}}><a class="ajax btn font-alt" href="#"><span  >360-Video</span></a></li>
                                  <li class=" ctab " id="cplain" data-videoid = {{$channel_name}} ><a class="ajax btn font-alt" href="#"><span>plain Video</span></a>
                                  </li>

                        </ul>
                        <div class="upload-gap-high "></div>
                        <ul>
                           
                            <li class="360_image_drop" style="float: right;">
                                          <label for="id_label_single">
                                            Select mountain name for quick finding :
                                           <select class="mountain-list-of-continent" name="mountain_name_360_image">
                                            @foreach( $subchannels->chunk(6) as $subchannel1 )
                                              @foreach($subchannel1 as $subchannel)
                                                 <?php
                                                    $var = count($subchannel->images->where('isthumb' , 'no'));
                                                    ?>
                                                  @if($var > 0)
                                                <option value={{$subchannel->sub_channel_id}}>{{$subchannel->sub_channel_name}}</option>

                                                  @endif
                                                @endforeach
                                            @endforeach
                                        
                                    
                                    </select>
                                  </label>
                                        </li>
                          
                              <li class="360_video_drop" style="display: none;float: right;">
                                        <label for="id_label_single">
                                          Select mountain name for quick finding :
                                         <select class="mountain-list-of-continent" name="mountain_name_360_video">
                                    @foreach( $subchannels->chunk(6) as $subchannel1 )
                                      @foreach($subchannel1 as $subchannel)
                                        <?php
                                        $var = count($subchannel->videos->where('category' , '360'));
                                        ?>
                                        @if($var > 0)
                                           <option value="{{$subchannel->sub_channel_id}}">
                                            {{$subchannel->sub_channel_name}}
                                           </option>
                                        @endif
                                      @endforeach
                                    @endforeach
                                  
                                  </select>
                                </label>
                            </li>

                          <li class="plain_video_drop" style="display: none;float: right;" >
                                        <label for="id_label_single">
                                          Select mountain name for quick finding :
                                         <select class="mountain-list-of-continent" name="mountain_name_plain">
                                            @foreach( $subchannels->chunk(6) as $subchannel1 )
                                              @foreach($subchannel1 as $subchannel)

                                              <?php
                                              $var1 = count($subchannel->videos->where('category' ,'plain'));
                                              ?>

                                               @if($var1 > 0)
                                                  <option value="{{$subchannel->sub_channel_id}}" style="border-bottom: 1px solid black;">
                                                    {{$subchannel->sub_channel_name}}</option>

                                               @endif
                                            @endforeach
                                            @endforeach
                                  
                                  </select>
                                </label>
                              </li>
                          
                        </ul>

                        <div>
                        <a id="button1"></a>
                        <style type="text/css">
                          #button1 {
                              display: inline-block;
                              background-color: #FF9800;
                              width: 50px;
                              height: 50px;
                              text-align: center;
                              border-radius: 4px;
                              position: fixed;
                              bottom: 30px;
                              right: 30px;
                              transition: background-color .3s, 
                                opacity .5s, visibility .5s;
                              opacity: 0;
                              visibility: hidden;
                              z-index: 1000;
                            }
                            #button1::after {
                              content: "\f135";
                              font-family: FontAwesome;
                              font-weight: normal;
                              font-style: normal;
                              font-size: 2em;
                              line-height: 50px;
                              color: #fff;
                            }
                            #button1:hover {
                              cursor: pointer;
                              background-color: #333;
                            }
                            #button1:active {
                              background-color: #555;
                            }
                            #button1.show {
                              opacity: 1;
                              visibility: visible;
                            }

                            /* Styles for the content section */

                            .content {
                              width: 77%;
                              margin: 50px auto;
                              font-family: 'Merriweather', serif;
                              font-size: 17px;
                              color: #6c767a;
                              line-height: 1.9;
                            }
                            @media (min-width: 500px) {
                              .content {
                                width: 43%;
                              }
                              #button1 {
                                margin: 30px;
                              }
                            }
                            .content h1 {
                              margin-bottom: -10px;
                              color: #03a9f4;
                              line-height: 1.5;
                            }
                            .content h3 {
                              font-style: italic;
                              color: #96a2a7;
                            }
                        </style>
                          
                          <script type="text/javascript">
                            $(document).ready(function() {
                                  $('.mountain-list-of-continent').select2({width: '100%'});

                                  $('.mountain-list-of-continent').change(function(){
                                    var data12 = $(this).val();
                                    $('html, body').animate({
                                        scrollTop: $("#" + data12).offset().top - 40
                                    }, 2000);
                                  });

                                  var btn = $('#button1');

                                    $(window).scroll(function() {
                                      if ($(window).scrollTop() > 300) {
                                        btn.addClass('show');
                                      } else {
                                        btn.removeClass('show');
                                      }
                                    });

                                    btn.on('click', function(e) {
                                      e.preventDefault();
                                      $('html, body').animate({scrollTop:0}, '300');
                                    });




                              })
                          </script>

                         <div class = "cvideo">
                         </div>

                         <div class = "cimage">
                         </div>
                         
                         <div class = "cplain">
                         </div>

                         <div class="cimage"> 
                          
                          <section class='' id='news'>
                          <div class=" multi-columns-row post-columns">

                            

                           @foreach( $subchannels->chunk(6) as $subchannel1 )
                             @foreach($subchannel1 as $subchannel)
                                    <div class="gap"></div>
                                      <div class="gap">
                                      </div>
                                      <?php
                                      $var = count($subchannel->images->where('isthumb' , 'no'));
                                      ?>
                                    @if($var > 0)
                                    <div class="row" id="{{$subchannel->sub_channel_id}}">
                                    <h4 class="module-title font-alt" style="text-align:left;"> 
                                      {{$subchannel->sub_channel_name}}
                                    </h4>
                                    <hr> 
                                     @foreach($subchannel->images as $image)
                                      @if($image->isthumb == 'no' )
                                      <a href="{{asset('/image/' . $image->indexer )}}"> <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                        <div class="post mb-20" >
                                        <div class="post-thumbnail">
                                        <img src="{{asset($shareadd['images_url'] . $image->image_id.'thumb.'.$image->type)}} " alt="Blog-post Thumbnail" style="max-height: 282px; width: 100%;" />
                                        <h4 class="post-title font-alt">
                                        {{$image->title}} 
                                        </h4>

                                        <div class="post-meta font-alt">
                                         {{$subchannel->user->first_name}} &nbsp;|  {{$image->number_of_views }} views 
                                        </div>

                                        </div><div class="post-entry"><p>
                                        {{$image->description}}
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                </a>
                                @endif
                                @endforeach  
                                </div>                                  
                                
                            @endif
                            @endforeach
                            @endforeach
                            </div>
              
              </section>
               
                          </div>
                       
                    </div> 
@stop
@section('sidebar')

@include('home.sidebar')

@stop