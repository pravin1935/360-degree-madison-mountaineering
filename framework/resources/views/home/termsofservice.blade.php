@extends('master')
@section('content')
<div class="container">
<div class="content" style="margin-top:0;padding-top:0;">
		<div class="row-fluid">
			
			<h3>Terms of Service</h3>
<p>This online video portal Video Chautari Service Agreement is dated as is made by Video Chautari Collectively as ("Provider") and Online Users ("Users").</p>
<h4>Section 1 General Provisions</h4>
<h4>Article 1 Scope of Application</h4>
<p>These Terms of Service shall apply to Provider and Users. </p>
<h4>Article 2 Amendments to these Terms of Service</h4>
<p>1. The Company may amend these Terms of Service without the consent from the Subscribers. </p>
<h4>Article 3 Notices, Communications, etc.</h4>
<p>Any notices, communications, etc. from the Company to the Users will be posted on the web site of the Provider. The Users shall, from time to time, access the start-up screen or the web site of the Provider to check for any notice or communication posted thereon. If any notice shall be provided by the Provider to the Users in accordance with these Terms of Service or any other individual terms, the Provider may, deliver the notice to the Users, post it on the start-up screen or the web site of the Provider. Any notice or communication shall be deemed to have been duly served to the Users 24 hours after being posted on the start-up screen or the web site of the Provider, regardless of whether or not the Users have checked for the notice or communication posted thereon.</p>
<h4>Article 4 Method of Provision of the Service</h4>
The Service shall be provided by the method specified in item 1 of Appendix 1.
<h4>Article 5 Service Items</h4>
Service items shall be specified per each category of Service.
<h4>Section 2 Service Use Agreement</h4>
An agreement for the use of the Service shall be made individually for each Service item to be used by the Users.
<h4>Section 3 Application for Use</h4>
A person who wishes to apply for an agreement for the use of the Service (here in after referred to as the "Application for Use") shall submit to the Provider an application containing the following information in the form prescribed by the Provider. The name or trade name and contact details of the person who applies for the Application for Use; the name of the representative if the person is a legal person; The item of the Service to be applied for; and Other information necessary to use the Service.
			
						
		</div>
	</div>
</div>
@stop