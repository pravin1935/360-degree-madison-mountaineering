@extends('master') 

@section('metacontent')

@stop @section('content')

<style type="text/css">
	
	.center{
		text-align: center;
		vertical-align: center;
		
	}


.card {
	background-color:white;
	width:100%;
	margin:50px 0 100px 0;
	border-radius: 8px;
	box-shadow: 0px 10px 20px -6px rgba(242,112,63,.5);
	border-top: 1px solid rgba(242,112,63,.2);
	overflow:hidden;
	cursor:pointer;
}

.card:before,
.card:after {
	content: '';
	display: block;
	height: 15px;
	border-radius: 0px 0px 4px 4px;
	position:absolute;
	bottom:100px;
	background-color: #F2C5B3;
}
.card:before {
	width:85%;
	margin-left:3.5%;
	margin-bottom:-10px;
	z-index: -1;
	box-shadow: 0px 10px 20px -6px rgba(242,112,63,.5);
}
.card:after {
	width:75%;
	z-index: -2;
	margin-left:9%;
	margin-bottom:-20px;
	height: 30px;
	background-color: #F59A78;
}

/* Inside Card */

/*img {
	width:100%;
	height: auto;
}*/

h1 {
	font-family: 'Poppins';
	text-align:center;
	color: #92D1FA;
}

p {
	font-family: 'Roboto';
	text-align:center;
	margin: 0 20px;
	color: gray;
}


/* Interactive States */

.card, .card:after, .card:before {
	transition: all .5s ease;
}

.card:hover {
	margin-top: 45px;
	box-shadow: 0px 15px 30px -6px rgba(242,112,63,.4);
}

</style>

<div class="section">

<!--made by vipul mirajkar thevipulm.appspot.com-->
<div id="content" class="center">
<h1>
         <a href="#session_end" class="typewrite" data-period="2000" data-type='[ "Sorry, Your Session is Expired", "Please login to access your account  ", "Click Sign-up/login to login ", "Click Home to go main page" ]'>
    <span class="wrap"></span>
  </a>
</h1>
<hr>
	 <div class="row">
		<div class="col-sm-4 col-sm-offset-4">
			<div class="card">
				<img src="https://d13yacurqjgara.cloudfront.net/users/1174720/screenshots/3084779/cafe6_2x-01.png">
				<h1>Session Expired</h1>
				<hr>
				<h4>Sorry for inconvenience , long inactivity kill the session for your secrity .</h4>
				<p>Please click login to log-in again and enjoy your account service .</p>
				<hr>
				
				<button data-toggle="modal" data-target="#myModal" type="button" class="btn btn-danger">Log-in </button>
				<div class="upload-gap"></div>
			</div>
		</div>
	</div>

</div>


</div>

<script type="text/javascript">
	//made by vipul mirajkar thevipulm.appspot.com
var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };
</script>


@stop @section('sidebar') @include('home.sidebar') @stop