@extends('master')
@section('metacontent')

<script type="text/javascript">
    var load_image = "{{url($shareadd['images_url'].$image->image_id.'.'.$image->type)}}";
</script>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript">
    stLight.options({
        publisher: "8be29c34-6b8c-478f-95b8-09c2a81749b7",
        doNotHash: false,
        doNotCopy: false,
        hashAddressBar: false
    });

</script>
    

<meta property="og:url"           content="{{ url("detail/$image->indexer") }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{$image->title}}" />
    <meta property="og:description"   content="{{$image->description}}" />
	<meta property="og:site_name" content="my.videochautari.com" />
    <meta property="og:image"         content="{{ asset('uploads/'.$image->image_id.'.jpg')}}" />
	<link rel="image_src" type="image/jpeg" href="{{ asset('uploads/'.$image->image_id.'.jpg') }}" />

<link rel='stylesheet' type='text/css' href="{{ asset('style.css') }}" />
<link rel="stylesheet" type="text/css" href="{{asset('vrview/examples/style.css')}}"/>

@stop
@section('title_page')
Paracosma TV | {{$image->title}}
@stop


@section('breadcumb')

 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <ol class="breadcrumb">
    <li><a href="{{url('/')}}">Home</a></li>
    <li class="active">{{$image->title}}</li>
  </ol>
</div>
@stop


@section('content')
<div class="videoplayersec">
        <div class="vidcontainer">
            <div class="row"> 
                <!-- Video Player Start -->
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="playeriframe">
                    <!-- Video Player Start -->
                    <div id="vrview"></div>
                    </div>

                    <div class="row"> 
                    <div class="col-md-12 clip_info_div">
                      <span class="clip_info_title font-alt">{{$image->title}}</span><br>
                      <span class="upload_detail "> <span class="space">From</span>              
                      <span class="gap_h font-alt"><b> {{App\User::find($image->user_id)->first_name}}</b></span><i class="fa fa-clock-o space font-alt " aria-hidden="true"></i>
                      {{$image->date_uploaded->diffForHumans()}}
                      </span>
                    </div>
                        <!-- Uploader Start -->
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 uploader font-alt">
                            <figure class="uploader-image"> 
                            <a href=""><img src="{{asset('uploads/avatars/'.(App\User::find($image->user_id)->avatar))}}" class="hundred" alt="" style="border-radius: 50%;"></a> 
                            </figure>
                            
                        </div>
                        
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 stats">
                            <hr class="visible-xs">
                            <ul class="icon_data_list">
                                <!-- <li class="likes ">
                                     <span class="icon_data">
                                     <a class="btn btn-link" id="like_button" data-id={{$image->indexer}}>
                                     <span class="love_btn"> 
                                        <i class="fa fa-heart-o video-under " aria-hidden="true"></i></span></a><span>1</span>
                                     
                                     </span>

                                </li> -->

                                <li class="views space3">
                                    <span class="icon_data"><i class="fa fa-eye video-under space2" aria-hidden="true" ></i><span>{{$image->number_of_views}}</span></span>
                                </li>

                                <li >
                                <div class="aboutuploader">
                                    <br>
                                    <a class="btn btn-danger btn-xs " href="{{url("channel/".App\Channel::find($image->channel_id)->channel_name)}}" style="color:white !important; letter-spacing:0px;">View All OF THIS CHANNEL</a> </div>
                                </li>

                                <li class="share">
                                  <!-- <div class="fb-share-button share" data-href="{{ url(" image/$image->indexer") }}" data-layout="button_count" style="position:relative;bottom:8px; margin-right:8px;">
                                  </div> -->
                                </li>
                            </ul>

                                
                                </div>
                                <div class="col-sm-12 col-xs-12">
                                    <div class="detail">
                                      <h4 class="description font-alt lower_case">Detail:</h4>
                                      <p class="font-alt lower_case">
                                        {{$image->description}}
                                      </p>
                                    </div>
                                </div>



                              <div class="col-sm-12">
                              <!-- Contenedor Principal -->
                                <!-- <div class="comments-container">
                                <div class="border_bottom">
                                  <h2>Comments</h2>
                                </div>

                                <div class="border_bottom row">
                                <div class="col-md-2 no-gap-right col-xs-3">
                                  <div class="comment_user"><img src="{{url("uploads/avatars/avatars.png")}}" class="hundred" alt=""></div>
                                </div>
                                <div class="col-md-10 no-gap-left">
                                  <div class="comment-box">
                                        <div class="comment-head">
                                          <h4 class="comment-user by-author"><a href="#profile">Pravin poudel</a></h4>
                                        </div>
                                        <form>
                                        <div class="form-group">
                                          <textarea name="comment" class="form-control" placeholder="put your comment here ..........."></textarea>
                                          <div class="gap"></div>
                                         <button type="submit" class="btn btn-success right">
                                          <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                              Send
                                        </button>
                                        </div>
                                        </form>
                                      </div>
                                    </div>
                                </div>
                                <div class="comment-div">
                                    <ul id="comments-list" class="comments-list">
                                      <li>
                                        <div class="comment-reply">
                                          
                                          <div class="comment-avatar"><img src="{{url("uploads/avatars/avatars.png")}}" class="hundred" alt=""></div>
                                          
                                          <div class="comment-box">
                                            <div class="comment-head">
                                              <h4 class="comment-user by-author"><a href="#profile">Pravin poudel</a></h4>
                                              <span> 20 minutes ago</span>
                                             <span class="reply_btn"><i class="fa fa-reply "></i></span> 
                                             <span class="love_btn"> <i class="fa fa-heart-o like_btn"></i></span>
                                            </div>
                                            <div class="comment-content">
                                              222On a dark desert highway, cool wind in my hair
                                              Warm smell of colitas, rising up through the air
                                              Up ahead in the distance, I saw a shimmering light
                                              My head grew heavy and my sight grew dim
                                            </div>
                                            <div class="upload-gap"></div>
                                          </div>
                                        </div>
                                        
                                        <ul class="comments-list reply-list">
                                          <li>
                                            
                                            <div class="comment-reply">
                                              <div class="comment-avatar"><img src="{{url("uploads/avatars/avatars.png")}}" class="hundred" alt=""></div>
                                            
                                            <div class="comment-box">
                                                  <div class="comment-head">
                                                    <h4 class="comment-user"><a href="profile">Kamal khanal</a></h4>
                                                    <span> 10 minutes ago</span>
                                                    <span class="reply_btn"><i class="fa fa-reply "></i></span> 
                                                    <span class="love_btn"> <i class="fa fa-heart-o like_btn"></i></span>
                                                  </div>
                                                  <div class="comment-content">
                                                            On a dark desert highway, cool wind in my hair
                                                            Warm smell of colitas, rising up through the air
                                                            Up ahead in the distance
                                                  </div>
                                            </div>
                                            </div>
                                          </li>

                                          <li>
                                            
                                            <div class="comment-reply">
                                            <div class="comment-avatar"><img src="{{url("uploads/avatars/avatars.png")}}" class="hundred" alt=""></div>
                                           
                                            <div class="comment-box">
                                              <div class="comment-head">
                                                <h4 class="comment-user by-author"><a href="http://creaticode.com/blog">Pravin poudel</a></h4>
                                                <span> 10 minutes ago</span>
                                                <span class="reply_btn"><i class="fa fa-reply "></i></span> 
                                             <span class="love_btn"> <i class="fa fa-heart-o like_btn"></i></span>
                                              </div>
                                              <div class="comment-content">
                                               On a dark desert highway, cool wind in my hair
                                                Warm smell of colitas, rising up through the air
                                                Up ahead in the distance
                                              </div>
                                            </div>
                                            </div>
                                          </li>
                                        </ul>
                                      </li>

                                      <li>
                                        
                                          
                                          <div class="comment-reply">
                                          <div class="comment-avatar"><img src="{{url("uploads/avatars/avatars.png")}}" class="hundred" alt=""></div>
                                          
                                          <div class="comment-box">
                                            <div class="comment-head">
                                              <h4 class="comment-user"><a href="http://creaticode.com/blog">Pravin poudel</a></h4>
                                              <span> 10 minutes ago</span>
                                              <span class="reply_btn"><i class="fa fa-reply "></i></span> 
                                             <span class="love_btn"> <i class="fa fa-heart-o like-btn"></i></span>
                                            </div>
                                            <div class="comment-content">
                                             On a dark desert highway, cool wind in my hair
                                              Warm smell of colitas, rising up through the air
                                              Up ahead in the distance, I saw a shimmering light
                                              My head grew heavy and my sight grew dim
                                            </div>
                                          </div>
                                        </div>
                                      </li>
                                    </ul>
                                  </div>
                              </div> -->
                              </div>
                            </div>


                        
                        

                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <ul class="media-list main-list">
           @foreach($related_image as $most)
              <li class="media">
                <a href="{{url("image/$most->indexer")}}" class="pull-left">
                    <div class="media-left">
                      <img src="{{asset($shareadd['images_url'] . $most->image_id.'thumb.'.$most->type)}}" class="media-object media-image"  alt="" />
                    </div> 
                </a>
               
                <div class="media-body">
                  <h4 class="media-heading">
                      <a href="{{url("image/$most->indexer")}}">
                        <h4 class="font-alt">{{str_limit($most->title,20)}}</h4>
                      </a>
                      
                  </h4>
                  <p class="by-author">By {{App\User::find($most->user_id)->first_name}}</p>
                  <p class="by-author"> <i class="fa fa-clock-o" aria-hidden="true"></i> {{$most->date_uploaded->diffForHumans()}}</p>
                </div>
              </li>
              @endforeach
        
      </ul>
          </div>
                       
                    </div>
                    

                </div>
            </div>
        </div>
    </div>
  
   

@stop

@section('js')
<script type="text/javascript" src="{{asset('js/imageVR.js') }}"></script>
@stop


@section('sidebar')
 @include('home.sidebar')
@stop