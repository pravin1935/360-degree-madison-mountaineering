<!DOCTYPE html>
<html>
<head>
	<title>Madison | ADD USER</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<!-- Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,600' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,900,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Ultra' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css'>
	<style type="text/css">
	@import url(https://fonts.googleapis.com/css?family=Roboto:400,500,700);

body {
  background-color: #59ABE3;
  font-family: 'Roboto';
}

.center {
  /*position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;*/
  text-align: center;
  position: relative;
  margin: 0 auto;
}

.button{
  font-size: 19px;
  overflow: visible;
  border-radius: 3px;
  position: relative;
  background-color: #ECFBFF;
  border: 2px solid #A6E0EE;
  color: #2D7586;
  display: block;
  /*margin: 13% auto;*/
  height: 60px;
  width: 200px;
  cursor: pointer;
}

.conf-modal {
  width: 290px;
  max-width: 80%;
  height: 250px;
  background-color: #fafafa;
  border-radius: 3px;
  box-shadow: 0 12px 36px 16px rgba(0, 0, 0, 0.24);
}

.conf-modal h1 {
  font-size: 24px;
  font-weight: 500;
  line-height: 10px;
  display: inline-block;
}

.title-text {
  display: inline-block;
  height: 35px;
  line-height: 52px;
  margin-left: 72px;
  margin-top: 22px;
}

.success h1 {
  color: #26cf36;
}

.title-icon {
  width: 27px;
  height: 27px;
  display: inline-block;
  margin-right: 10px;
  margin-left: 30px;
  margin-top: 30px;
  position: absolute;
}

.conf-modal p {
  color: #737373;
  padding: 15px 30px;
  font-size: 16px;
  line-height: 24px;
}

/*.modal-footer {
  background: red;
}*/

.modal-footer .conf-but {
  display: inline-block;
  float: right;
  margin-right: 15px;
  margin-top: 5px;
  text-transform: uppercase;
  font-weight: 800;
  color: #4c4c4c;
  background: none;
  padding: 10px 15px;
  border-radius: 4px;
}

.modal-footer .conf-but:hover {
  background: #eee;
  cursor: pointer;
  opacity: .8;
}

.modal-footer .conf-but.green {
  color: #26cf36;
}
	</style>
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-sm-12">
        <div class=" "><h1 class="center">WELCOME ADMIN!</h1></div>
        <a href="{{url('/')}}">
              <button class="btn btn-success button center">GO HOME</button>  
          </a>
      </div>
    </div>
  </div>
  



<div>
     <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.indigo-pink.min.css">
    <!-- Material Design Lite Font Icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  </head>
  <body>

  <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
      <thead>
        <tr>
          <th class="mdl-data-table__cell--non-numeric">Material</th>
          <th>Quantity</th>
          <th>Unit price</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="mdl-data-table__cell--non-numeric">Acrylic (Transparent)</td>
          <td>25</td>
          <td>$2.90</td>
        </tr>
        <tr>
          <td class="mdl-data-table__cell--non-numeric">Plywood (Birch)</td>
          <td>50</td>
          <td>$1.25</td>
        </tr>
        <tr>
          <td class="mdl-data-table__cell--non-numeric">Laminate (Gold on Blue)</td>
          <td>10</td>
          <td>$2.35</td>
        </tr>
      </tbody>
      <tfoot>
        <td class="mdl-data-table__cell--non-numeric" colspan="3">
          This is a footer. Add notes here.
        </td>
      </tfoot>
    </table>

    <!-- Material Design Lite JavaScript -->
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.min.js"></script>

</div>
</body>
</html>