@extends('master')
@section('metacontent')
<script type="text/javascript">
    var playing_video = "{{url( $shareadd['videos_url'].$video->video_id.'.'.$video->type)}}";
</script>


<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript">
    stLight.options({
        publisher: "8be29c34-6b8c-478f-95b8-09c2a81749b7",
        doNotHash: false,
        doNotCopy: false,
        hashAddressBar: false
    });

</script>
    

<meta property="og:url"           content="{{ url("detail/$video->indexer") }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{$video->title}}" />
    <meta property="og:description"   content="{{$video->description}}" />
  <meta property="og:site_name" content="my.videochautari.com" />
    <meta property="og:image"         content="{{ asset('uploads/'.$video->video_id.'.jpg')}}" />
  <link rel="image_src" type="image/jpeg" href="{{ asset('uploads/'.$video->video_id.'.jpg') }}" />

<link rel='stylesheet' type='text/css' href="{{ asset('style.css') }}" />
<link rel="stylesheet" type="text/css" href="{{asset('vrview/examples/style.css')}}"/>

@stop

@section('title_page')
Paracosma TV | {{$video->title}}
@stop

@section('breadcumb')

 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <ol class="breadcrumb">
    <li><a href="{{url('/')}}">Home</a></li>
    <li class="active">{{$video->title}}</li>
  </ol>
</div>
@stop
@section('content')
<div class="videoplayersec">
        <div class="vidcontainer">
            <div class="row"> 
                <!-- Video Player Start -->
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    
                      <video id="SimplePlay" controls controlsList="nodownload" playsinline poster="{{ asset('uploads/'.$video->video_id.'.'.$video->thumb) }}">
                        <source src="{{url( $shareadd['videos_url'].$video->video_id.'.'.$video->type)}}" type="video/mp4">
                        <source src="http://techslides.com/demos/sample-videos/small.mp4" type="video/mp4">
                        Your browser doesn't support HTML5 video tag.
                      </video>

                    <div class="row"> 
                    <div class="col-md-12 clip_info_div">
                      <span class="clip_info_title font-alt">{{$video->title}}</span><br>
                      <span class="upload_detail font-alt "> <span class="space">From</span>              
                      <span class="gap_h"><b> {{App\User::find($video->user_id)->first_name}}</b></span><i class="fa fa-clock-o space" aria-hidden="true"></i>
                      {{$video->date_uploaded->diffForHumans()}}
                      </span>
                    </div>
                        <!-- Uploader Start -->
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 uploader">
                            <figure class="uploader-image"> 
                            <a href=""><img src="{{asset('uploads/avatars/'.(App\User::find($video->user_id)->avatar))}}" class="hundred" alt="" style="border-radius: 50%;"></a> 
                            </figure>
                            
                        </div>
                        <!-- Uploader End --> 
                        <!-- Video Stats Start -->
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 stats">
                            
                            <ul class="icon_data_list">
                                <!-- <li class="likes ">
                                     <span class="icon_data">
                                     <a class="btn btn-link" id="like_button" data-id={{$video->indexer}}>
                                     <span class="love_btn"> 
                                        <i class="fa fa-heart-o video-under " aria-hidden="true"></i>
                                        <span>1</span>
                                     </span>
                                     </a>
                                     </span>

                                </li> -->

                                <li class="views space3">
                                    <span class="icon_data"><i class="fa fa-eye video-under space2" aria-hidden="true" ></i><span>{{$video->number_of_views}}</span></span>
                                </li>

                                <li >
                                <div class="aboutuploader hidden-xs">
                                    <br>
                                    <a class="btn btn-danger btn-xs " href="{{url("channel/".App\Channel::find($video->channel_id)->channel_name)}}" style="color:white !important; letter-spacing:0px;">Watch All OF THIS CHANNEL</a> </div>
                                </li>

                                <!-- <li class="share">
                                  <div class="fb-share-button share" data-href="{{ url(" detail/$video->indexer") }}" data-layout="button_count" style="position:relative;bottom:8px; margin-right:8px;">
                                  </div>
                                </li> -->
                            </ul>

                                
                                </div>
                                <div class="col-sm-12 col-xs-12">
                                    <div class="detail">
                                      <h4 class="description font-alt ">Detail:</h4>
                                      <p class="font-alt lower_case">
                                        {{$video->description}}
                                      </p>
                                    </div>
                                </div>



                              <div class="col-sm-12">
                              <!-- Contenedor Principal -->
                                
                              </div>
                            </div>


                        
                        

                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <ul class="media-list main-list">
           @foreach($related_video as $most)
              <li class="media">
                <a href="{{url("detailplain/$most->indexer")}}" class="pull-left">
                    <div class="media-left">
                      <img src="{{ asset('uploads/'.$most->video_id.'.'.$most->thumb)  }}" class="media-object media-image"  alt="" />
                    </div> 
                </a>
               
                <div class="media-body">
                  <h4 class="media-heading">
                      <a href="{{url("detaiplainl/$most->indexer")}}">
                        <h4>{{str_limit($most->title,17)}}</h4>
                      </a>
                  </h4>
                  <p class="by-author">By {{App\User::find($most->user_id)->first_name}}</p>
                  <p class="by-author"> <i class="fa fa-clock-o" aria-hidden="true"></i> {{$most->date_uploaded}}</p>
                </div>
              </li>
              @endforeach
        
      </ul>
          </div>
                        <!-- Video Share End --> 
                    </div>
                    

                </div>
            </div>
        </div>
    </div>
  
   

@stop

@section('sidebar')
 @include('home.sidebar')
@stop