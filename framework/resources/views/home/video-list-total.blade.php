<feed>
	<resultLength>20</resultLength>
	<endIndex>20</endIndex>
	@foreach($lists as $list)
	<?php
		$vid = $list->video_id;
		$large_player_thumb = $vid.'.jpg';
		$thumb_file = "uploads/videos/mp4/player_thumbs/$large_player_thumb";

	?>
	
	<item sdImg='{{ "http://www.videochautari.com/".$thumb_file }}' hdImg='{{ "http://www.videochautari.com/".$thumb_file }}'>
		<contentId>{{$vid}}</contentId>
			<title>{{ htmlentities($list->title,ENT_COMPAT | ENT_XML1)}}</title>
		<contentQuality>SD</contentQuality>
		<streamFormat>mp4</streamFormat>
		<media>
			<streamQuality>SD</streamQuality>
			<streamBitrate>1500</streamBitrate>
			<views>{{$list->number_of_views}}</views>
			<videoLength>{{$list->video_length}}</videoLength>
			@if(!empty($list->embed_id))
				<streamUrl>http://www.youtube.com/embed/{{$list->embed_id}}</streamUrl>
			@else 
				<streamUrl>
					
{{url('uploads/videos/mp4/'.$list->video_id.'.mp4')}}
				</streamUrl>
			@endif
		</media>
		<synopsis>{{ htmlentities($list->description, ENT_COMPAT | ENT_XML1)}}</synopsis>
	</item>
	@endforeach

</feed>