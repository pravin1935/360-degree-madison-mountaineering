<categories>
<banner_ad sd_img='{{ asset("vc_images/images/videochautari.png") }}' hd_img='{{ asset("vc_images/images/missing.png") }}'/>
<category title='Recent Videos' description= 'Recent videos' sd_img='{{ asset('vc_images/images/HD-SD/default_SD.png') }}' hd_img='http://www.videochautari.com/images/HD-SD/default_HD.png' >
	<categoryLeaf title="Recent Videos" description="" feed="{{url('api/videos_nonyoutube?channel_id=150') }}"/>
</category>
@foreach($channels as $channel)
<?php
	if(!empty($images[$channel->channel_id]))
	{
		$sd_image = asset('vc_images/images/HD-SD/'.$images[$channel->channel_id]['sd_img'].'.png');
		$hd_image = asset('vc_images/images/HD-SD/'.$images[$channel->channel_id]['hd_img'].'.png');
	}
	else
	{
		$sd_image = asset('vc_images/images/HD-SD/default_SD.png');
		$hd_image = asset('vc_images/images/HD-SD/default_HD.png');
	}
    ?>

	<category title='{{$channel->channel_name}}' description= '{{$channel->channel_description}}' sd_img='{{$sd_image}}' hd_img='{{$hd_image}}' >
		<categoryLeaf title="{{$channel->channel_name}}" description="" feed="{{url('api/videos_nonyoutube/'.'?channel_id='.$channel->channel_id)}}"/>
	</category>
@endforeach

</categories>