<?php echo  '<?xml version="1.0" encoding="UTF-8" ?>'; ?>

<categories>
<banner_ad sd_img="/var/www/videosite/images/videochautari.png" hd_img="/var/www/videosite/images/missing.png"/>
<category title='RECENT VIDEOS' description= 'Recent videos' sd_img='http://www.videochautari.com/images/HD-SD/default_SD.png' hd_img='http://www.videochautari.com/images/HD-SD/default_HD.png' >
		<categoryLeaf title="RECENT VIDEOS" description="" feed="http://videochautari.com/videos_list_total.php?channel_id=150"/>
</category>
@foreach($lists as $list)
<?php if(!empty($images[$list['channel_id']]))
	{
		$sd_image = asset('vc_images/images/HD-SD/'.$images[$list['channel_id']]['sd_img'].'.png');
		$hd_image = asset('vc_images/images/HD-SD/'.$images[$list['channel_id']]['hd_img'].'.png');
	}
	else
	{
		$sd_image = asset('images/HD-SD/default_SD.png');
		$hd_image = asset('vc_images/images/HD-SD/default_HD.png');
	}
?>
<category title="{{$list->channel_name}}" description="{{$list->channel_description}}" sd_img="{{$sd_image}}" hd_image="{{$hd_image}}">
	<categoryLeaf title="{{$list->channel_name}}" description="" feed="http://videochautari.com/videos_list_total.php?channel_id={{$list->channel_id}}"/>
</category>
@endforeach
</categories>