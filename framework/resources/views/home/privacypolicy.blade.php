@extends('master')
@section('content')
<div class="container">
	
	<div class="content" style="margin-top:0;padding-top:0;">
		<div class="row-fluid">
			
			<h2>Video Chautari Privacy Statement</h2>
<p>Video Chautari is serious about protecting our users by addressing potential privacy concerns. Our privacy policy applies to all users across the world. In other words, while the video in question may not violate your country's privacy laws, it may still violate Video Chautariís privacy policy.</p>
<p>Video Chautari contains links to other web sites. This Privacy Statement does not apply to the practices of any company or individual that Video Chautari does not control, or any web sites or services that you link to from the Video Chautari products. You should use caution and review the privacy policies of any web sites or services that you visit from ours to learn more about their information practices. Please take a few moments to read this Privacy Statement. By accessing the Video Chautari, you agree to accept the terms and conditions of this Privacy Statement and are aware that our policies may evolve in the future as indicated below. In the event of a conflict between this Privacy Statement and our Terms of Use, our Terms of Use will control.</p>
<h3>How does Video Chautari determine if content should be removed for a privacy violation?</h3>
<p>For content to be considered for removal, an individual must be uniquely identifiable by image, voice, full name, Social Security number, or contact information (e.g. home address, email address). Examples that would not violate our privacy policy include gamer tags, avatar names, and address information in which the individual is not named. We also take public interest, newsworthiness, and consent into account when determining if content should be removed for a privacy violation. Video Chautari reserves the right to make the final determination of whether a violation of its privacy guidelines has occurred.</p>
<h3>How Video Chautari's privacy process works</h3>
<p>If a privacy complaint is filed, Video Chautari Team will then review the complaint within the 48 hours of time.</p>
<h4>First-party claims required</h4>
<p>We do not accept claims on behalf of third parties except in the following situations:</p>
<ul>
<li>the individual whose privacy is being violated does not have access to a computer</li>
<li>the individual whose privacy is being violated is a vulnerable individual</li>
<li>you are the parent or legal guardian of the individual whose privacy is being violated </li>
<li>you act as a legal representative for the individual whose privacy is being violated</li>
</ul>
We will not accept privacy complaints filed on behalf of:
<ul>
<li>other family members (e.g., husband, wife, cousin, brother, sister)</li>
<li>co-workers or employees (individuals must report themselves)</li>
</ul>
<h3>Tips on filing a complete privacy complaint</h4>
<p>Please be clear and concise so that the Video Chautari Team can identify you within the video.</p>
<ul>
<li>Use the time stamp to indicate only one or two places where you clearly appear in the video.</li>
<li>In the description area, please specify what you are wearing or doing that differentiates you from others within the video.</li>
<li>Make sure you've included the video URL in your report. You are not required to submit a URL if you are reporting an entire channel.</li>
<li>If you are reporting a comment made in the comments section of a video, please note this in the description area. Include the commenter's username in the username field.</li>
</ul>
<h3>Postings</h3>
<p>If you use our bulletin boards, post user comments regarding content available through the Video Chautari, or engage in similar activity, please remember that anything you post is publicly available. Portions of your user profile will also be available to other users of our websites.</p>

<h3>Changes to this Privacy Statement</h3>
<p>We may make changes to this Privacy Statement from time to time for any reason. Use of information we collect now is subject to the Privacy Statement in effect at the time such information is used (though you always have the option to delete your account as described above). If we make changes in the way we use Personally Identifiable Information, we will notify you via e-mail or by posting an announcement on our website.</p>




	
		</div>
	</div><!-- content ends -->	
  </div>
@stop