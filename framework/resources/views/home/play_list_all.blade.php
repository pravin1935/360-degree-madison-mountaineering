<playlist>
@foreach($videos as $video)
	<video>
		<id>{{$video->video_id}}</id>
		<title>{{$video->title}}</title>
		<embed_id/>
		<source>{{ url('video/playbackurl/'.$video->indexer) }}</source>
		<thumb>{{asset('uploads/'.$video->video_id.'.jpg')}}</thumb>
	</video>
@endforeach
</playlist>