 

@extends('master')
@section('content')
	<div class="sections">
                        <div class="well well-lg search_well"> <h2 class="heading">Video search result</h2></div>
                       
                        <div class="clearfix"></div>
                        <div class="row">

                        @if($videos->count()>0)

                         @foreach($videos as $video)
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12"> 

                                <div class="card">
                                  <a href="{{url("detail/$video->indexer")}}">
                                            <img src="{{ asset('uploads/'.$video->video_id.'.'.$video->thumb) }}" class="img-responsive " style="height:216.65px;" alt="" />
                                            <img class="OverlayIcon" src="{{asset('uploads/video_icon_overlay.png')}}" alt="" />
                                        </a>
                                  <br />
                                  <div class="card-content">
                                      <h4 class="card-title">
                                          <span class="mli-info1"><a href="{{url("/detail/$video->indexer")}}">{{str_limit($video->title,25)}}</a></span>
                                      
                                   
                                          <div class="view right">
                                          <i class="fa fa-eye view-icon space" aria-hidden="true"></i>{{"$video->number_of_views"}}
                                          </div>
                                      </h4>

                                      <div class="">
                                          {{App\User::find($video->user_id)->first_name}},{{$video->date_uploaded}},{{App\Channel::find($video->channel_id)->channel_name}}
                                      </div>
                                  </div>
                                  <div class="card-read-more">
                                      <a href="{{url("detail/$video->indexer")}}" class=" center btn-block btn-link">
                                          WATCH NOW
                                      </a>
                                  </div>
                              </div>




                                <!-- Video Box Start -->


                                <!-- Video Box End --> 
                            </div>
                            @endforeach
                          {!! $videos->render() !!}

                        
                        @else
                        <div class="">
                            <div class="col-sm-8 col-md-12 col-xs-12">
                            <div class="error-notice">
                              <div class="oaerror danger">
                                <strong>Sorry</strong> - We dont have any videos related to that word in our site. sorry try other words ..
                              </div>
                            </div>
                            <hr>
                            <div class="gap"></div>
                            </div>

                            </div>
                        @endif
                           
                        </div>
                        <div class="well well-lg search_well" > <h2 class="heading">Image search result</h2></div>
                        <div class="clearfix"></div>
                        <div class="row">
                        @if($images->count()>0)
                            @foreach($images as $image)
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12"> 
                                <!-- Video Box Start -->
                                
                                <div class="videobox2 card" style="padding: 0px 2px !important">
                                          <figure>
                                        
                                          <iframe
                                          frameborder="0"
                                          width="100%"
                                          height=240px
                                          scrolling="no"
                                          allowfullscreen
                                          src="{{url('index.html')}}?image={{asset('uploads/'.$image->image_id.'.'.$image->type)}} ">
                                          </iframe>
                                        
                                              <!-- Video Thumbnail Start -->


                                          </figure>
                                          <!-- Video Title Start -->
                                          <span class="">
                                                <div class="description col-md-12">
                                                   <a href="{{url("image/$image->indexer")}}" class="left gap" >
                                                    <span class=" uploader  mli-info1  " style="font-weight: 600 !important; padding-bottom: 5px !important; color:#1d7fe6; ">{{(str_limit($image->title , 35))}}</span>
                                                    </a>

                                                </div>
                                                <br>
                                                <div class="upload-gap"></div>

                                                <div class="gap_high_border"></div>
                                                <div class="upload_data">
                                                  <div class="inform left">
                                                        <span class="uploader " style="margin-right: 15px; color:#0f68c5;"><i class="fa fa-upload space" aria-hidden="true"></i>{{(App\User::find($image->user_id)->first_name)}}
                                                        </span>

                                                        <span class=" uploader " style="font-weight: 600 !important;">{{(App\Channel::find($image->channel_id)->channel_name)}}
                                                        </span>
                                                  </div>


                                                  <div class="upload-date">
                                                        <span class="uploader right"><i class="fa fa-clock-o space" aria-hidden="true"></i>
                                                            {{$image->date_uploaded}}
                                                          <div class="upload-gap"></div>
                                                        </span>

                                                  </div>
                                                  </div>
                                           </span>


                                <!-- Video Box End --> 
                            </div>
                        </div>
                            @endforeach
                        @else
                            <div class="">
                            <div class="col-sm-8 col-md-12 col-xs-12">
                            <div class="error-notice">
                              <div class="oaerror danger">
                                <strong>Sorry</strong> -  We dont have any images related to that word in our site. sorry try other words ..
                              </div>
                            </div>
                            </div>
                            <hr>
                            </div>
                      
                        @endif
                           
                         
                        </div>
                    </div>   
@stop
@section('sidebar')

    @include('home.sidebar')

@stop