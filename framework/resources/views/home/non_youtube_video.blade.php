<feed>
<resultLength>20</resultLength>
<endIndex>20</endIndex>

@foreach($videos as $video)
	<item sdImg='{{asset("uploads/videos/mp4/player_thumbs/".$video->video_id.".jpg")}}' hdImg='{{asset("uploads/videos/mp4/player_thumbs/".$video->video_id.".jpg")}}'>
	
	<contentId>{{$video->video_id}}</contentId>
<title>{{$video->title}}</title>
<contentQuality>SD</contentQuality>
<streamFormat>mp4</streamFormat>
<media>
<streamQuality>SD</streamQuality>
<streamBitrate>1500</streamBitrate>
<views>{{$video->number_of_views}}</views>
<videoLength>{{$video->video_length}}</videoLength>
<streamUrl>{{url('uploads/videos/mp4/'.$video->video_id.'.mp4')}}</streamUrl>
</media>
<synopsis>{{$video->description}}</synopsis>
</item>
@endforeach
</feed>