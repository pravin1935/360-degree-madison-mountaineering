@extends('master')
@section('content')

                 <div class="sections">
                        <h2 class="heading">Feature Video List</h2>
                        <div class="clearfix"></div>
                        
                            <?php $a=1; ?>
                            @foreach($videos->chunk(4) as $chunk)
                           <div class="row">
                            @foreach($chunk as $video)
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12"> 
                                <!-- Video Box Start -->
                                <div class="videobox2">
                                    <figure> 
                                        <!-- Video Thumbnail Start --> 
                                        <a href="{{url("detail/$video->indexer")}}">
                                            <img src="{{ asset('uploads/'.$video->video_id.'.'.$video->thumb) }}" class="img-responsive hovereffect" alt="" />
                                        </a> 
                                        <!-- Video Thumbnail End --> 
                                        <!-- Video Info Start -->
                                        <div class="vidopts">
                                            <ul>
                                                <li><i class="fa fa-heart"></i>{{$video->number_of_views}}</li>
                                                <li><i class="fa fa-clock-o"></i>{{$video->video_length}}</li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <!-- Video Info End --> 
                                    </figure>
                                    <!-- Video Title Start -->
                                    <h4><a href="{{url("detail/$video->indexer")}}">{{str_limit($video->title,30)}}</a></h4>
                                    
                                    <!-- Video Title End --> 
                                </div>
                                <!-- Video Box End --> 
                            </div>
                            @endforeach
                            </div>
                            <?php $a++; ?>
                            @endforeach
                          {!! $videos->render() !!}
                       
                    </div> 
@stop
@section('sidebar')

    @include('home.sidebar')

@stop