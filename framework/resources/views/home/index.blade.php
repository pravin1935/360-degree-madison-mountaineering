
@extends('master')

@section('title_page')
 ParacosmaTV | 360 Videos
@stop

@section("image-slider")

  @if(Session::has('msg1') || Session::has('msg2'))
    <div class="alert alert-danger">
    <span class="closebtn right" onclick="this.parentElement.style.display='none';">&times;</span> 
     <span class="glyphicon glyphicon-hand-right space"></span> <strong>SORRY  , but you have error Message</strong>
     <hr class="message-inner-separator">
    @if(Session::has('msg1'))
    <h5>{{Session::get('msg1')}}</h5> 
    @endif
      @if(Session::has('msg1') && Session::has('msg2'))
        <hr>
      @endif
    @if(Session::has('msg2'))
    <h5>{{Session::get('msg2')}} </h5>
    @endif
</div>

  @endif


<div class="sections">
  
 

<div id="myCarousel" class="carousel slide" data-interval="false">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>



  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  
  <div class="carousel-inner" role="listbox">
    <div class="item active">
       <iframe
                                                   frameborder:0
                                                   width ="100%"
                                                   height = "500px"
                                                   scrolling="no"
                                                   is_vr-off ="no"
                                                   allowfullscreen
                                                   src="{{url('index.html')}}?image={{asset('/uploads/22.jpg') }}&scrolling=true">
                                                </iframe>

      <div class="carousel-caption">
        <h3>American Alpine club library, Colorado</h3>
        <p>The library contains more than 50,000 books and all the information you could ever want on mountain history, mountain culture, and climbing routes, and more.</p>
      </div>
    </div>

    <div class="item">
       <iframe
                                                   frameborder:0
                                                   width ="100%"
                                                   height = "500px"
                                                   scrolling="no"
                                                   is_vr-off ="no"
                                                   allowfullscreen
                                                   src="{{url('index.html')}}?image={{asset('/uploads/33.jpg') }}&scrolling=true">
                                                </iframe>
      <div class="carousel-caption">
        <h3>Busy street somewhere in city</h3>
        <p>Drag your mouse to see the busy street and street area and life near to it .</p>
      </div>
    </div>



    
    

    
  </div>
    
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

                    </div>

@stop


@section('content')
                <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                  <h2 class="module-title font-alt">360 Videos and Image</h2>
                   <div class="module-subtitle font-serif">Gallery of 360 Images and Videos of mountaineering expeditions to the “7 Summits” and other renowned international and domestic peaks.</div>
                 </div>
              </div>


                <div class="sections">
                        <div class="gallery-title" style="display: inline-block;">
                              <span class="heading heading1" style="display: inline-block;padding: 14px;">Featured</span>
                              <ul class="nav nav-tabs" style="display: inline-block; margin-left:3px; position:relative;  background: none; top: 20px;">
                                  <li class=" lvideo lvideo1 " ><a class="ajax btn" href="#"><span  style="color:#222222; cursor: pointer;" >Featured-360-Image</span></a></li>
                                  <li class="limages limage1 "><a class="ajax btn" href="#"><span  >Featured-360-Video</span></a></li>
                                  <li class="lvideoplain videoplain videoplain1  " id="fvplain"><a class="ajax btn" href="#"><span style="color: red;" >Featured-plain Video</span></a>
                                  <!-- <li class="limages"><a class="ajax btn" href="#"><span  >Featured-Video</span></a> -->
                                  </li>
                              </ul>
                       </div>
                       <div class="clearfix"></div>
                       <hr class="latest-hr">

            <div id="test">
            </div>

            <div id="fvplain">
            </div>

            <div class="link">
              <section class="module" id="news">
              <div class="container-fluid">
             
               <div class="row multi-columns-row post-columns">

                @foreach($fimage->chunk(3) as $chunk)            
                   @foreach($chunk as $feature)
                  <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="post mb-20">
                    <a href="{{url("image/$feature->indexer")}}">
                    <div class="post-thumbnail"><img src="{{asset($shareadd['images_url'] . $feature->image_id.'thumb.'.$feature->type)}} " alt="Blog-post Thumbnail" style="max-height: 282px; width: 100%;" /></div>
                    <div class="post-header font-alt">
                      <h2 class="post-title">{{(str_limit($feature->title , 26))}}</h2>
                      <div class="post-meta">By&nbsp;{{(App\User::find($feature->user_id)->first_name)}}&nbsp;| {{$feature->date_uploaded->diffForHumans()}} | {{$feature->number_of_views}} Views
                      </div>
                    </div>
                    <div class="post-entry">
                      <p>{{(str_limit($feature->description , 28))}}</p>
                    </div>
                   	 
                    </a>
                </div>
              </div>

               @endforeach
             @endforeach
               </div>

            </div>
            
          </section>



               <!--  @foreach($fimage->chunk(3) as $chunk)
                            
                            @foreach($chunk as $feature)

                            
                                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  featured-width ">
                                      
                                      <div class="videobox2 card" style="padding: 0px 2px !important">
                                          <figure>
                                        <a href="{{url("image/$feature->indexer")}}" class="left gap" >
                                          
                                          <img src="{{asset('uploads/'.$feature->image_id.'thumb.'.$feature->type)}} ">
                                        
                                        </a>   


                                          </figure>
                                         
                                          <span class="">
                                                <div class="description col-md-12">
                                                   <a href="{{url("image/$feature->indexer")}}" class="left gap" >
                                                    <span class=" uploader  mli-info1  " style="font-weight: 600 !important; padding-bottom: 5px !important; color:#1d7fe6; ">{{(str_limit($feature->title , 35))}}</span>
                                                    </a>

                                                </div>
                                                <br>
                                                <div class="upload-gap"></div>

                                                <div class="gap_high_border"></div>
                                                <div class="upload_data">
                                                  <div class="inform left">
                                                        <span class="uploader " style="margin-right: 15px; color:#0f68c5;"><i class="fa fa-upload space" aria-hidden="true"></i>{{(App\User::find($feature->user_id)->first_name)}}
                                                        </span>

                                                        <span class=" uploader " style="font-weight: 600 !important;">{{(App\Channel::find($feature->channel_id)->channel_name)}}
                                                        </span>
                                                  </div>


                                                  <div class="upload-date">
                                                        <span class="uploader right"><i class="fa fa-clock-o space" aria-hidden="true"></i>{{$feature->date_uploaded->diffForHumans()}}
                                                          <div class="upload-gap"></div>
                                                        </span>

                                                  </div>
                                                  </div>
                                           </span>


                                         

                                        
                                      </div>
                                      
                                  </div>



                        @endforeach

                        
             @endforeach -->
             </div>

             </div>




              <div class="sections">

                        <div class="latest-show">
                              <span class="module-title font-alt2">Latest  Videos </span>
                              <ul class="nav nav-tabs" style="display: inline-block; margin-left:3px; position:relative;  background: none; top: 20px;">

                                  <li class="lvideo360" ><a class="ajax btn" href="#"><span  style="color:#222222; cursor: pointer;" >latest-360-Image</span></a></li>

                                  <li class="lvideoplain " id="lvplain" ><a class="ajax btn" href="#"><span style="color: red;" >Latest-plain Video</span></a>
                                  </li>
                              </ul>


                                  <div class="show-all ">
                                        <a href="{{url('promoted-video')}}" style="color: white !important;" class=" btn btn-primary">Show All</a>
                                  </div>
                        </div>
                        <div id="lvplain">
                        </div>
                        <hr class="latest-hr">
                        <div class="lvplainlink">
                        <div class="latestsection"> 
                          @foreach($latestvideo1->chunk(10) as $chunk)
                              <div class="responsive">
                                 @foreach($chunk as $promote)

                                      <!-- Video Box Start -->
                                      <div class="videobox2 ">
                                          <figure >
                                              <!-- Video Thumbnail Start -->
                                              <div class="item">
                                              <a href="{{url("detail/$promote->indexer")}}">
                                                  <img src="{{ asset('uploads/'.$promote->video_id.'.'.$promote->thumb) }}" class="img-responsive hovereffect latest-image" alt="" />
                                                  <img class="OverlayIcon" src="{{asset('uploads/video_icon_overlay.png')}}" alt="" />
                                              </a>
                                              </div>
                                              <!-- Video Thumbnail End -->
                                              <!-- Video Info Start
                                              <div class="vidopts">
                                                  <ul>
                                                      <li><i class="fa fa-heart"></i>{{$promote->number_of_views}}</li>
                                                      <li><i class="fa fa-clock-o"></i>{{$promote->video_length}}</li>
                                                  </ul>
                                                  <div class="clearfix"></div>
                                              </div>
                                               Video Info End -->
                                          </figure>
                                          <!-- Video Title Start -->
                                          <div class="upload-gap"></div>
                                          <span class="mli-info1" >
                                          <a href="{{url("detail2/$promote->indexer")}}" class="font-alt">{{str_limit($promote->title,28)}}</a>
                                          </span>
                                          <!-- Video Title End -->
                                      </div>

                                      <!-- Video Box End -->

                                  @endforeach

                            </div>
                         @endforeach
                       </div>
                     </div>

                </div>

                <?php /* <div class="sections">
                        <h2 class="heading">Promoted  Videos</h2>
                        <div class="clearfix"></div>
                        <div class="row">
                            @foreach($promoted as $promote)
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <!-- Video Box Start -->
                                <div class="videobox2">
                                    <figure>
                                        <!-- Video Thumbnail Start -->
                                        <a href="{{url("detail/$promote->indexer")}}">
                                            <img src="{{ asset('uploads/videos/mp4/thumbs/'.$promote->video_id.'.jpg') }}" class="img-responsive hovereffect" alt="" />
                                        </a>
                                        <!-- Video Thumbnail End -->
                                        <!-- Video Info Start -->
                                        <div class="vidopts">
                                            <ul>
                                                <li><i class="fa fa-heart"></i>{{$promote->number_of_views}}</li>
                                                <li><i class="fa fa-clock-o"></i>{{$promote->video_length}}</li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <!-- Video Info End -->
                                    </figure>
                                    <!-- Video Title Start -->
                                    <h4><a href="{{url("detail/$promote->indexer")}}">{{str_limit($promote->title,35)}}</a></h4>
                                    <!-- Video Title End -->
                                </div>
                                <!-- Video Box End -->
                            </div>
                            @endforeach
                          {!! link_to('promoted-video', $title = 'Show All', $attributes = array(), $secure = null) !!}
                        </div>
                    </div>
          */ ?>

                
                 <div class="sections">
                        <span class="module-title font-alt2">Most Viewed </span>
                        <ul class=" nav-tabs" style="display: inline-block; margin-left:3px; position:relative;  background: none; top: 17px;">
                                  
                                  <li class="mvideo360" ><a class="ajax btn" href="#"><span  style="color:#222222; cursor: pointer;" >most-viewed-360-video</span></a></li>
                                  <li class="lvideoplain " id="mvplain" ><a class="ajax btn" href="#"><span style="color: red;" >Most viewed-plain Video</span></a>
                                  <!-- <li class="limages"><a class="ajax btn" href="#"><span  >Featured-Video</span></a> -->
                                  </li>
                              </ul>
                        <div class="clearfix"></div>
                        <div id="mvplain">
                        </div>
                        <hr class="latest-hr">
                        <div class="mvplainlink">
                        <section class="module" id="news">
            <div class="container-fluid">
             
               <div class="row multi-columns-row post-columns">

               @foreach($mostviewd->chunk(4) as $chunk)
                        
                        @foreach($chunk as $most)
                  <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="post mb-20">
                  <a href="{{url("detail/$most->indexer")}}">
                  <div class="post-thumbnail">
                    <img src="{{ asset('uploads/'.$most->video_id.'.'.$most->thumb) }}" class="img-responsive " style="height: 290px;" alt="" />
                    
                    <img class="OverlayIcon" src="{{asset('uploads/video_icon_overlay.png')}}" alt="" /></div>
                  
                    <div class="post-header font-alt">
                      <h2 class="post-title">{{(str_limit($most->title , 28))}}</h2>
                      <div class="post-meta">By&nbsp;{{(App\User::find($most->user_id)->first_name)}}&nbsp;| {{$most->date_uploaded->diffForHumans()}} | {{"$most->number_of_views"}} Views
                      </div>
                    </div>
                  <div class="post-entry">
                    <p>{{(str_limit($most->description , 28))}}</p>
                  </div>
                  
                  </a>
                </div>
              </div>

               @endforeach
             @endforeach
               </div>

            </div>
            
          </section>
        </div>


           <!--  @foreach($mostviewd->chunk(4) as $chunk)
                        
                        @foreach($chunk as $most)

                          <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="card">
                                  <a href="{{url("detail/$most->indexer")}}">
                                            <img src="{{ asset('uploads/'.$most->video_id.'.'.$most->thumb) }}" class="img-responsive " style="height: 290px;" alt="" />
                                            <img class="OverlayIcon" src="{{asset('uploads/video_icon_overlay.png')}}" alt="" />
                                        </a>
                                  <br />
                                  <div class="card-content">
                                      <h4 class="card-title">
                                          <span class=" mli-info1 " ><a href="{{url("detail/$most->indexer")}}">{{str_limit($most->title,25)}}</a>
                                      
                                    </span>
                                          <div class="view right">
                                          <i class="fa fa-eye view-icon space" aria-hidden="true"></i>{{"$most->number_of_views"}}
                                          </div>
                                      </h4>

                                      <div class="">
                                          <span>
                                          <span class="uploader"> 

                                          <div class="upload_data">
                                                  <div class="inform left">
                                                        <span class="uploader " style="margin-right: 15px; color:#0f68c5;"><i class="fa fa-upload space" aria-hidden="true"></i>{{( App\User::find($most->user_id)->first_name)}}
                                                        </span>

                                                        <span class=" uploader " style="font-weight: 600 !important;">{{App\channel::find($most->channel_id)->channel_name}}
                                                        </span>
                                                  </div>


                                                  <div class="upload-date">
                                                        <span class="uploader right"><i class="fa fa-clock-o space" aria-hidden="true"></i>{{$most->date_uploaded->diffForHumans()}} 
                                                          <div class="upload-gap"></div>
                                                        </span>

                                                  </div>
                                                  </div>
                                      </div>
                                      <br>
                                      <div class="card-read-more">
                                     <a href="{{url("detail/$most->indexer")}}" class=" center btn-block btn-link">
                                          WATCH NOW
                                      </a>
                                  </div>
                                  </div>
                                  
                              </div>
                          </div>
                        @endforeach
             @endforeach -->
                    </div>










                  <div class="sections">
                        <span class="module-title font-alt2">Browsed Videos </span>
                        <div class="clearfix"></div>
                        <hr class="latest-hr">


                         <section class="module" id="news">
            <div class="container-fluid">
             
               <div class="row multi-columns-row post-columns">

               @foreach($videos->chunk(4) as $chunk)
                  @foreach($chunk as $video)
                  <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="post mb-20">
                  <a href="{{url("detail/$video->indexer")}}">
                  <div class="post-thumbnail">
                    <img src="{{ asset('uploads/'.$video->video_id.'.'.$video->thumb) }}" class="img-responsive " style="height: 290px;" alt="" />
                    
                    <img class="OverlayIcon" src="{{asset('uploads/video_icon_overlay.png')}}" alt="" /></div>
                  
                  <div class="post-header font-alt">
                    <h2 class="post-title">{{(str_limit($video->title , 35))}}</h2>
                    <div class="post-meta">By&nbsp;{{(App\User::find($video->user_id)->first_name)}}&nbsp;| {{$video->date_uploaded->diffForHumans()}} | {{"$video->number_of_views"}} Views
                    </div>
                  </div>
                  <div class="post-entry">
                    <p>{{(str_limit($video->description , 28))}}</p>
                  </div>
                 
                  </a>
                </div>
              </div>

               @endforeach
             @endforeach
               </div>

            </div>
            
          </section>


          <!--   @foreach($videos->chunk(4) as $chunk)
                        

                            @foreach($chunk as $video)

                            <div class=" col-xs-12 col-sm-4 col-md-4">
                              <div class="card">
                                  <a href="{{url("detail/$video->indexer")}}">
                                            <img src="{{ asset('uploads/'.$video->video_id.'.'.$video->thumb) }}" class="img-responsive " style="height:216.65px;" alt="" />
                                            <img class="OverlayIcon" src="{{asset('uploads/video_icon_overlay.png')}}" alt="" />
                                        </a>
                                  <br />
                                  <div class="card-content">
                                      <h4 class="card-title">
                                          <span class="mli-info1"><a href="{{url("/detail/$video->indexer")}}">{{str_limit($video->title,25)}}</a></span>
                                      
                                   
                                          <div class="view right">
                                          <i class="fa fa-eye view-icon space" aria-hidden="true"></i>{{"$video->number_of_views"}}
                                          </div>
                                      </h4>

                                      <div class="">
                                          {{App\User::find($video->user_id)->first_name}},{{$video->date_uploaded->diffForHumans()}},{{App\Channel::find($video->channel_id)->channel_name}}
                                      </div>
                                  </div>
                                  <div class="card-read-more">
                                      <a href="{{url("detail/$video->indexer")}}" class=" center btn-block btn-link">
                                          WATCH NOW
                                      </a>
                                  </div>
                              </div>
                          </div>


                            @endforeach

             @endforeach -->
                    </div>


 <!-- Contents Section Started -->





               <div class="sections">
                       <h2></h2>
                       <div class="clearfix"></div>
                       <hr class="latest-hr">
                       <div class="row">
                       <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                       <a href="https://www.thenorthface.com/">
                       <div class="image-advertisement1">
                          <img src="{{ asset('uploads/reco.jpg')}}" class="advertise" alt="reco360">
                       </div>
                        </a>
                       </div>

                       <div class="hidden-lg hidden-md hidden-sm col-xs-12">
                       <a href="https://www.thenorthface.com/">
                       <div class="image-advertisement1">
                          <img src="{{ asset('uploads/1212.jpg')}}" class="advertise" alt="reco360">
                       </div>
                        </a>
                       </div>




                        </div>
                   </div>
                    <!-- Contents Section End -->
                    <div class="clearfix"></div>


@stop
@section('sidebar')

<div class="widget">
    <h4 class="module-title font-alt">360 Media Categories</h4>

    <div id="accordion" role="tablist" aria-multiselectable="true">
  
  @foreach($channels as $channel)
  <div class="card">
    <div class="card-header" role="tab" id="headingThree">
      <div class="mb-0">
        <a data-toggle="collapse" data-parent="#accordion" href= "#{{$channel->channel_name}}"  aria-expanded="false" aria-controls="collapseOne" class="collapsed">
          <i class="fa fa-file-text-o" aria-hidden="true"></i>
            <span class="font-alt" style="letter-spacing: 0px !important;">{{$channel->channel_name_seo}}</span>
        </a>
        <i class="fa fa-angle-right" aria-hidden="true"></i>
      </div>
    </div>
   
    <div id= "{{$channel->channel_name}}" class="collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
      <ul class="undesign">
       @foreach($channel->subchannels as $subchlist)
      <div class="card-block">
        <li ><a href="{{url("subchannel/$subchlist->sub_channel_id")}}"> {{$subchlist->sub_channel_name}}</a></li> 
      </div>
       @endforeach
       </ul>
    </div>
   
  </div>
  @endforeach
</div>


<div class="widget" style="margin-top: 0px !important">
    <h4 class="module-title font-alt">360 Media Categories (VIDEOS)</h4>
    <ul style="list-style: none; ">
        @foreach($channels as $channel)

    <li style="padding: 15px 10px; border-bottom: 1px solid #c0c7c2; margin-bottom: 4px; font-size: 16px;">
      
      <a onclick="return true;" style="font-weight: bold; font-size: 14px; padding: 12px 20px; text-transform: uppercase; letter-spacing: 1px; color: #5f6563;" class="font-alt" href="{{url("channel/".$channel->channel_id)}}">{{$channel->channel_name}} 

        <span class="number"></span>
      </a>
    </li>
        @endforeach
    </ul>
    
</div>


    <!-- <div class="acc-container">

        @foreach($channels as $channel)
        <div class="acc-btn"><h5 class="font-alt">{{$channel->channel_name_seo}}</h5></div>
          <div class="acc-content">
            <div class="acc-content-inner">
              <ul>
                @foreach($channel->subchannels as $subchlist)
                <li style="padding: 9px 0px; border-bottom: 1px solid #dddddd; ">
                  <a onclick="return true;" class="category_text"  href="{{url("subchannel/$subchlist->sub_channel_id")}}">{{$subchlist->sub_channel_name}}
                  </a>
                </li>
                @endforeach
              </ul>
            </div>
          </div>
      @endforeach

   </div> -->
    <!-- <ul style="list-style: none; ">
        @foreach($channels as $channel)
              <li style="padding: 9px 0px; border-bottom: 1px solid #dddddd; ">
      <a onclick="return true;" class="category_text"  href="{{url("channel/".$channel->channel_name)}}">{{$channel->channel_name}}
      <span class="number">({{$channel->total}} )</span>
      </a></li>
        @endforeach
    </ul> -->

</div>
<div class="container2">
<div class="widget widget-advertise2">
<a href="https://www.thenorthface.com/">
<img src="{{ asset('/uploads/PowerDirector.jpg') }}" alt="just for test"width="100%" height="320px"  />
<div class="blog-post-div">
  <!--<span class="blog-text">Treavel experience sitting in a room </span><br>
<span class="writter" style="font-style: italic; "><i class="fa fa-pencil-square-o" aria-hidden="true"></i> someone famous</span> -->
</div>
</a>
</div>

<div class="widget widget-promoted">
    <h4 class="module-title font-alt">Promoted Videos</h4>
    <ul class="promotion">
          @foreach($promoted as $promote)
        <li class="promotion-video">

                        <!-- Video Title Start -->
                        <a href="{{url("detail/$promote->indexer")}}"><p  class="category_text">{{str_limit($promote->title,35)}}</p></a>
                        <!-- Video Title End -->
        </li>
        @endforeach
    </ul>


</div>
</div>

<div class="widget">
  
<div class="fb-page" data-href="https://www.facebook.com/MadisonMtng/" data-tabs="timeline" data-width="748" data-height="450" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/MadisonMtng/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/MadisonMtng/">Madison Mountaineering</a></blockquote></div>

</div>
<div>
 <script src="https://snapwidget.com/js/snapwidget.js"></script>



</div>


@stop

<script src="{{asset('js/lib/wow/dist/wow.js')}}"></script>

