@extends('master')
@section('content')

                 <div class="sections">
                        

                          @if( !empty($data))
                            <div class="upload-gap"></div>
                            <h2 class="heading">Videos List By Categories
                            <hr>
                            <div class="clearfix"></div>
                                @foreach($data->chunk(6) as $chunk)
                                 
                                    @foreach($chunk as $video)   
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12  "> 
                                        <!-- Video Box Start -->
                                        <div class="videobox2 card" style="background: white;">
                                            <figure> 
                                                <!-- Video Thumbnail Start --> 
                                                <a href="{{url("detail/$video->indexer")}}">
                                                    <img src="{{ asset('uploads/'.$video->video_id.'.'.$video->thumb) }}" class="img-responsive hovereffect" style="height:200px;"  alt="" />
                                                </a> 
                                                <!-- Video Thumbnail End --> 
                                               
                                            </figure>
                                            <!-- Video Title Start -->
                                            <h4><a href="{{url("detail/$video->indexer")}}">{{str_limit($video->title,30)}}</a></h4>
                                            <!-- Video Title End --> 
                                        </div>
                                        <!-- Video Box End --> 
                                    </div>
                                    <ul class="categories visible-xs">
                                        <li style="padding: 5px 10px; border-bottom: 1px solid #fff; margin-bootm: 4px; font-size: 16px;">
                                        <a style="font-weight: normal; font-size: 14px; color: #1F97D4;" 
                                        href="{{url("image/$video->indexer")}}">{{str_limit($video->title,35)}}</a></li>
                                    </ul>
                                    @endforeach    
                                @endforeach
                            
                            @endif

                        
                    </div> 
@stop
@section('sidebar')

@include('home.sidebar')

@stop