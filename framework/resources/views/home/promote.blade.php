@extends('master')
@section('content')

                 <div class="sections">
                        <h2 class="heading module-title font-alt">Promoted Video List</h2>
                        <div class="clearfix"></div>
                       
                            
                            @foreach($videos->chunk(4) as $chunk)
                             <div class="row">
                            @foreach($chunk as $video)   
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12"> 
                                <!-- Video Box Start -->
                                <div class="videobox2">
                                    <figure> 
                                        <!-- Video Thumbnail Start --> 
                                        <a href="{{url("detail/$video->indexer")}}">
                                            <img src="{{ asset('uploads/'.$video->video_id.'.'.$video->thumb) }}" class="img-responsive hovereffect" alt="" />
                                        </a> 
                                        <!-- Video Thumbnail End --> 
                                        <!-- Video Info Start -->
                                        <div class="vidopts">
                                            <ul>
                                                <li><i class="fa fa-heart"></i>{{$video->number_of_views}}</li>
                                                <li><i class="fa fa-clock-o"></i>{{$video->video_length}}</li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                       <!-- Video Info End --> 
                                    </figure>
                                    <!-- Video Title Start -->
                                    <h4><a href="{{url("detail/$video->indexer")}}">{{str_limit($video->title,30)}}</a></h4>
                                    <!-- Video Title End --> 
                                </div>
                                <!-- Video Box End --> 
                            </div>
                            <!-- <ul class="categories hidden-md hidden-lg">
                                <li style="padding: 5px 10px; border-bottom: 1px solid #fff; margin-bootm: 4px; font-size: 16px;">
                                <a style="font-weight: normal; font-size: 14px; color: #1F97D4" href="{{url("detail/$video->indexer")}}">{{str_limit($video->title,35)}}</a></li>
                            </ul> -->
                            @endforeach
                             </div>
                            @endforeach
                          {!! $videos->render() !!}

                          @if ( ! $videos->count())
                          <div class="jumbotron">
                              <h1 style="text-align: center;">Sorry this time !!!!!!! </h1> 
                              <h3 style="text-align: center;">Sorry there is no promoted video now here sorry  , please visit soon . </h3>
                            </div>
                            
                          @endif
                       
                    </div> 
@stop
@section('sidebar')

@include('home.sidebar')

@stop