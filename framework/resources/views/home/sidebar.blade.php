<div class="widget" style="margin-top: 0px !important">
    <h4 class="module-title font-alt">360 Media Categories (VIDEOS)</h4>
    <ul style="list-style: none; ">
        @foreach($channels as $channel)

		<li style="padding: 5px 10px; border-bottom: 1px solid #c0c7c2; margin-bottom: 4px; font-size: 16px;">
			
      <a onclick="return true;" style="font-weight: bold; font-size: 14px; padding: 12px 20px;text-transform: uppercase;" class="font-alt" href="{{url("channel/".$channel->channel_id)}}">{{$channel->channel_name}} 

				<span class="number">({{$channel->total}})</span>
			</a>
		</li>
        @endforeach
    </ul>
		
</div>

<div class="widget">
  <a href="https://www.thenorthface.com/">
<img src="{{ asset('images/paracosmatvad/ricoh-theta-m15.jpg') }}" alt="just for test"width="100%" height="200px"  />
</a>
</div>
<div class="widget">
	    
    <ul class="bloglist">
    	@if( !empty ($featured))

    	<h4 class="module-title font-alt">LATEST VIDEOS</h4>
   		
   			@foreach($featured as $promote)
   		
   			<li style="padding: 5px 10px; margin-bottom: 4px; font-size: 16px; " class="category_text">
   				<!-- Video Title Start -->
   				<a  href="{{url("detail/$promote->indexer")}}"><p style="padding: 5px; color: #0b7ab3; font-size: 14px;">{{str_limit($promote->title,35)}}</p></a>
   				<!-- Video Title End --> 
   			</li>
	 		@endforeach 
	 	@endif
	                    
	</ul>

	
	          

</div>

<div class="widget">
	    
    
	
  <ul class="bloglist">

      @if(!empty ($featured_image) )

      <h4 class="module-title font-alt">LATEST IMAGES</h4>

        @foreach($featured_image as $promote)
        
        <li style="padding: 5px 10px;  margin-bottom: 4px; font-size: 16px; " class="category_text">
          <!-- Video Title Start -->
          <a  href="{{url("image/$promote->indexer")}}"><p style="padding: 5px;color: #0b7ab3;  font-size: 14px;">{{str_limit($promote->title,35)}}</p></a>
          <!-- Video Title End --> 
        </li>
      @endforeach 
      @endif             
  </ul>

  
	          

</div>

<div class="fb-page" data-href="https://www.facebook.com/MadisonMtng/" data-tabs="timeline" data-width="100%" data-height="700" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/MadisonMtng/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/MadisonMtng/">Madison Mountaineering</a></blockquote></div>

<div class="widget">
	
</div>