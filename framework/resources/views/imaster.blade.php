<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title>Paracosma TV</title>
<!--// Responsive //-->
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="True">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<!--// Stylesheets //-->




<link rel="stylesheet" href="{{asset('css/style.css')}}">

<link rel="stylesheet" href="{{asset('css/bootstrap.css') }}">

<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
<!--[if lt IE 9]>
    <script src="{{ asset('js/html5shiv.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/respond.min.js') }}"></script>
<![endif]-->
</head>
<body>

    
 @yield('content')


<script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1486518024692226&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>
(function($){
// 	$('.slider1').bxSlider({
// 		slideWidth: 120,
// 		minSlides: 2,
// 		maxSlides: 3,
// 		slideMargin: 10
// });
$('.images').on('click',function(){
	var video = $(this).data('video');
	var url = $(this).data('url');
	console.log(url);
	$.ajax({
		url: url,
		type: 'GET',
		data: {video: video},
		success: function(res){
			console.log(res);
			//$('.test').load(url);
			$("#frame").attr("src", "{{url("demo")}}");
			//$("#frame").contents().find("html").html(res);
		}
	});
});

})(jQuery);
</script>
</body>
</html>