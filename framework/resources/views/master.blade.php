<!DOCTYPE html>
<html lang="en">

<head>
<script type="text/javascript">var baseURL = "{{url('/')}}";</script>
@include('partials._head')
<!--<script src="http://vjs.zencdn.net/ie8/1.1.0/videojs-ie8.min.js"></script>-->
@yield('css')
</head>
 <body >

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
$(function() {

    function readURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#blah2').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#imgInp2").change(function() {

      console.log('image changed');
      readURL(this);
    
    });

});
 

$(document).ready(function(){
    
});
    
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '2013056728933412',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();   
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Wrapper Start -->
<div class="wrapper"> 
    <!-- Header Start -->
     <header>
      <div class="navbar navbar-default navbar-fixed-top navigatsioon">
  <div class="container-fluid" style="background-color: white;">

    <div class="navbar-header navigatsioon">
      <button button type="button" class="navbar-toggle collapsed navigatsioon" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
        <a class="navbar-brand " rel="home" href="http://madisonmountaineering.com/" title="Madison Mountaineering" style="z-index: 12;">
        
        <img style=" margin-top: -17px;" src="http://madisonmountaineering.com/wp-content/uploads/2014/03/MM_header_logo.png">

      </a>

    </div>

    <div id="navbar" class="collapse navigatsioon navbar-collapse navbar-responsive-collapse">
      <ul class="nav navigatsioon navbar-center navbar-nav navbar-left ">
        <li class="active1"><a href="{{url('/')}}" style="color:#4aa706;" style="text-transform: none;">HOME</a></li>
        <li class="dropdown"><a class="dropdown-toggle " href="#" data-toggle="dropdown">360 IMAGES</a>
                      <ul class="dropdown-menu">

                        @foreach ($shareadd['channel_list'] as $channelname)

                          <li class="dropdown" style="width: 100% ;padding: 7px 0px; border-bottom: 1px solid #ccc;"><a class=" font-alt" href="#" data-toggle="dropdown"> {{$channelname->channel_name_seo }}</a>
                          
                        </li>

                        @endforeach
                        
                        </ul>
        </li>

        <li class="dropdown"><a class="dropdown-toggle " href="#" data-toggle="dropdown">360 VIDEOS</a>
                      <ul class="dropdown-menu">
                        @foreach ($shareadd['channel_list'] as $channelname)

                          <li class="dropdown" style="width: 100% ; padding: 7px 0px;  border-bottom: 1px solid #ccc;"><a class=" font-alt" href="{{url('')}}"  > {{$channelname->channel_name_seo }}</a>
                          
                        </li>

                        @endforeach

                    </ul>
                    </li>

        <li><a href="http://madisonmountaineering.com/gallery/">GALLERY</a></li>
        <li style="padding-right:35px;" >
          @include('partials._search')
        </li>
        
        <li><a href="http://madisonmountaineering.com/#contact">CONTACT US</a></li>
        <li><a href="http://madisonmountaineering.com/dispatches/" style="color: red !important;"><b>DISPATCHES</b></a></li>
        <li><a href="{{url('add/user')}}">ADD USER</a></li>
        <li><a href="https://www.facebook.com/MadisonMtng" class="icon_ref"><i class="fa fa-facebook" style="color: #3b5998;"></i></a></li>
        

        <li><a href="https://youtube.com/madisonmtng" class="icon_ref"><i class="fa fa-youtube" style="color: #bb0000;"></i></a></li>
        <li><a href="https://instagram.com/madisonmtng" class="icon_ref"><i class="fa fa-instagram" style="color: #5e3e23;"></i></a></li>
        <li><a href="https://plus.google.com/madisonmtng" class="icon_ref"><i class="fa fa-google-plus" style="color: #dd4b39;"></i></a></li>
      </ul>

      <div class="user_panel_div">
        @include('partials._userPanel')
      </div>
    </div>

  </div>
</div>
    </header class='main-header'>

 <div class="modal fade" id="myModal" role="dialog" tabindex="-1"  hidden="true" style="background-image: url(/paracosmatv/uploads/form-back.jpg);">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Welcome To Pracosma tv</h4>
        </div>
        <div class="modal-body">
              <div class="row">    
        <div id="loginbox"  class="mainbox col-md-12 col-md-offset-0 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div style="padding:5px 0px 0px 0px;" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                        <form id="loginForm" class="form-horizontal" role="form" method="POST" action = "{{route('post_login')}}">
            {{ csrf_field()}}
                            <figure style="text-align: center;">
                              <img src="{{asset('uploads/img_avatar.png')}}" class="img-circle image-avatar" />
                            </figure>
                                    
                            <div style="margin-bottom: 25px" class="input-group form-group" id="email-div">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="email" type="email" class="form-control" name="email" value="" placeholder="username or email" title="Please enter your email" required="">
                                        {{-- <div class="help-block">
                                          <strong id="form-errors-email"></strong>
                                        </div>
                                        <span class="help-block small">Your email</span> --}}                                        
                            </div>
                                
                            <div style="margin-bottom: 25px" class="input-group form-group" id="password-div">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input title="Please enter your password" id="password" type="password" class="form-control" name="password" placeholder="********" value="" required="">
{{-- 
                                        <div class="help-block">
                                          <strong id="form-errors-password"></strong>
                                        </div>
                                        <span class="help-block small">Your strong password</span> --}}
                            </div>

                            <div class="form-group" id="login-errors">
                              <div class="help-block">
                                <strong id="form-login-errors"></strong>
                              </div>
                            </div>

                            <div>
                            <div class="help-block" style="color:#A52A2A">
                              <div id="form-errors-email"></div><br>
                              <div id="form-errors-password"></div><br>
                                <div id="form-login-errors"></div>
                              </div>
                            </div>
                               
                            <div class="input-group form-group"">
                                      <div class="checkbox">
                                        <label>
                                          <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                        </label>
                                      </div>
                            </div>


                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">

                                      <input type="submit" id="sign_in" value="Sign In" class="btn btn-success">
                                      <div style="font-size: 100%;position: relative;top: 3px;border-bottom: 1px solid; margin-left: 12px;display: inline-block;"></div>
                                    </div>
                                </div>

<!-- 
                                <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#c4c4c4; padding-top:15px; font-size:85%" >
                                            Don't have an account! 
                                        <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
                                           <span class="sign-up-span" > Sign Up  </span>
                                        </a>
                                        </div>
                                    </div>
                                </div>    --> 
                        </form>     



                        </div>                     
                    </div>  
        </div>




       
    </div>
    
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>





<div class="modal fade" id="myModalas" role="dialog" tabindex="-1"  hidden="true" style="background-image: url(/paracosmatv/uploads/form-back.jpg);">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title font-alt">THIS IS FORM TO ADD ADMIN !!!!</h4>
        </div>
        <div class="modal-body">
              <div class="row">    
        <div id="loginbox"  class="mainbox col-md-12 col-md-offset-0 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title font-alt">NEW MEMBER TO JOIN </div>
                            
                        </div>  
                        <div class="panel-body" >
                            <form id="registerForm" class="form-horizontal" role="form" method="POST" action="{{route('post_register')}}">
              {{ csrf_field()}}
                                
                                <div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                    <span></span>
                                </div>
                                    
                                
                                  
                                <div class="form-group" id="register-first-name">
                                    <label for="firstname" class="col-md-3 control-label">First Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="eg:John" required="" title="Please enter you name">
                                        <span class="help-block">
                                          <strong id="register-errors-first-name"></strong>
                                        </span>
                                        <span class="help-block small">Your first name</span>
                                    </div>
                                </div>
                                <div class="form-group" id="register-last-name">
                                    <label for="lastname" class="col-md-3 control-label">Last Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="eg:Poudel" required="" title="Please enter you name" value="">
                                        <span class="help-block">
                                          <strong id="register-errors-last-name"></strong>
                                        </span>
                                        <span class="help-block small">Your Last name</span>
                                    </div>
                                </div>

                                <div class="form-group" id="register-email">
                                    <label for="email" class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="eg:example@mail.com" required="" title="Please enter you email" value="">
                                        <span class="help-block">
                                        <strong id="register-errors-email"></strong>
                                        </span> 
                                        <span class="help-block small">Your email</span>
                                    </div>
                                </div>
                                    
                                
                                <div class="form-group" id="register-password">
                                    <label for="password" class="col-md-3 control-label">Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="password" placeholder="eg:X8dfl90E0" required="" title="Please enter your password" value="">
                                        <span class="help-block">
                                        <strong id="register-errors-password"></strong>
                                        </span> 
                                        <span class="help-block small">Your strong password</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">Confirm Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="eg:X8dfl90E0">
                                        <span class="help-block">
                                        <strong id="form-errors-password-confirm"></strong>
                                        </span> 
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="icode" class="col-md-3 control-label">Invitation Code</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="icode" placeholder="eg:f0l9X8dE0">
                                    </div>
                                </div>
                                <div class="form-group" id="login-errors">
                                  <span class="help-block"><strong id="form-login-errors"></strong></span>
                                </div>

                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Sign Up</button>
                                        <span style="margin-left:8px;">or</span>  
                                    </div>
                                </div>
                                
                                <!-- <div style="border-top: 1px solid #999; padding-top:10px"  class="form-group">
                                    
                                    <div>&nbsp</div>
                                    <div class="col-md-offset-3 col-md-9" style="padding: 0px;">
                                        <a href="redirect"><button id="btn-signup"type="button" class="btn btn-primary"> <i class="fa fa-facebook-official space" aria-hidden="true"></i>Log in with facebook</button></a>   
                                        <button id="btn-fbsignup" type="button" class="btn btn-danger" ><i class="fa fa-google" aria-hidden="true"></i> Sign Up with Google</button>
                                    </div>                                         
                                        
                                </div> -->
                                
                                
                                
                            </form>
                         </div>
                    </div>
        </div>




       
    </div>
    
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


<div id="editform" class="modal hide fade" role="dialog" style="z-index: 9900;">
  <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Form </h4>
        </div>
        <div class="modal-body">
              <div class="container-fluid">    
                <div id="loginbox"  class="mainbox ">                    
                    <div class="panel panel-info" >
                            <div style="padding:5px 0px 0px 0px;" class="panel-body" >

                                <div style="display:none;"  id="login-alert" class="alert alert-danger col-sm-12"></div>
                                    
                                <form id="upload_form" name = "upload_form" action="#" enctype="multipart/form-data" method="PUT" class="form-horizontal" role="form">
                                    
                                    <i class="fa fa-cloud-upload image-avatar" aria-hidden="true"  style="font-size: 48px; margin: 0% 42% 4% 42%;"></i>
                                    
                                    <!-- <div style="margin-bottom: 25px" class="input-group"> -->
                                               <!--  <span class="input-group-addon"><i class="fa fa-tag" aria-hidden="true"></i></span>
                                                <button type="button" class="btn btn-success" data-target="#channel_menu" data-toggle="collapse" style="margin-right: 8px;">
                                                  <span class="option">Channel</span><span class="caret"></span>
                                                </button> -->
                                               <!--  <i class="fa fa-pencil-square-o space" aria-hidden="true" style="font-size: 15px;position: relative;top:2px;"></i>
                                                <input type="text" id="subchannel" name="subchannel" style="width: 70%; height: 35px; border:none; border-bottom: 1px solid;" placeholder="Name of your album " value=""/> -->
                                               <!--  <div class="collapse" id="channel_menu">
                                                    <select class="collapse1" role="menu" id ="channel" name="channel" style="width: 100%; padding: 1.5% 0%;" value="">
                      
                                                              <option value="1">Travel</option>
                                                              <option value="2">Action</option>
                                                              <option value="3">Sports</option>
                                                              <option value="4">Music</option>
                                                              <option value="5">Events</option>
                                                              <option value="6">Comedy</option>
                                                              <option value="7">Nepali Events</option>

                                                    </select>
                                                </div> -->

                                    <!-- </div> -->
                                    
                                    <div style="margin-bottom: 25px" class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" id = "title" name="title" placeholder="Title of the File" value="" style="border: 2px solid  #6b6b6b;" />
                                   </div>

                                   <div style="margin-bottom: 25px" class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" id="description"  name="description" placeholder="Information about File " value="" style="border: 2px solid  #6b6b6b;"/>
                                   </div>

                                   <input type='file' name="thumbput" id="imgInp2" />
                                        <img id="blah2" src="#" alt="Browse image for thumbnail to change thumbnail" style="text-align: center;max-width: 90%;font-size: 16px;font-family: Roboto Condensed, sans-serif; border-bottom:2px solid #ff8201;" 
                                         />
                                    
                                    <div class="margin-bottom"></div>
                                                   <!--  <div class="privacy1">
                                                        <select class="collapse1 privacy-select" style="padding: 1.5% 0% !important; width: 27%;" role="menu" name="privacy" id ="video_privacy" >
                          
                                                                  <option value="yes">public</option>
                                                                  <option value="no">private</option>            
                                                       </select>
                                                   </div> -->
                                                    <!-- <input type = "file" name="video"  id ="video" style="margin: 0% 43%;color: red;"> <br> -->
                                    
                                                    <hr>
                                                    <br>
                                                    <!-- <input type = "file" name="video"  id ="video" style="margin: 0% 43%;color: red;"> <br> -->
                                                    <p class = "help-block">All this field are assets for your file so you could easily search and locate in future.</p> <hr style="border-top: 1px solid #969696;">

                                                    <div class="status" style="color: red;"></div>
                                                    <button type="submit" id="submit" class="btn btn-danger" >submit</button>


                                                   
                                                    <h3 id="status"></h3>
                                                    <p id="loaded_n_total"></p>
                                                    </form> 
                                                    </div>
                                    </div>

                                        



                                </div>                     
                            </div>  
                </div>
              </div>
        </div>
</div>











  





  <div class="modal fade" id="modal-upload1" role="dialog"  style="background-image: url(/paracosmatv1/uploads/form-back.jpg);">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload 360 image/video to Paracosma tv</h4>
        </div>
        <div class="modal-body">
              <div class="container">    
                <div id="loginbox"  class="mainbox col-md-6 col-md-offset-0 col-sm-8 ">                    
                    <div class="panel panel-info" >
                            <div style="padding:5px 0px 0px 0px;" class="panel-body" >

                                <div style="display:none;"  id="login-alert" class="alert alert-danger col-sm-12"></div>
                                    
                                <form id="upload_form" action="{{url('uploads/store')}}" enctype="multipart/form-data" method="post" class="form-horizontal" role="form">
                                    
                                    <i class="fa fa-cloud-upload image-avatar" aria-hidden="true"  style="font-size: 48px; margin: 0% 42% 4% 42%;"></i>
                                    
                                    <div style="margin-bottom: 25px" class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-tag" aria-hidden="true"></i></span>
                                                <button type="button" class="btn btn-success" data-target="#channel_menu1" data-toggle="collapse" style="margin-right: 8px;">
                                                  <span class="option">Channel</span><span class="caret"></span>
                                                </button>
                                                <i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size: 15px;position: relative;top:2px;"></i>
                                                <input type="text" name="subchannel" style="width: 72%; height: 35px; border:none; border-bottom: 1px solid;" placeholder="   Name of your album " />
                                                <div class="collapse" id="channel_menu1">
                                                    <select class="collapse1" role="menu" name="channel" style="width: 100%; padding: 1.5% 0%;">
                      
                                                              <option value="1">Travel</option>
                                                              <option value="2">Action</option>
                                                              <option value="3">Sports</option>
                                                              <option value="4">Music</option>
                                                              <option value="5">Events</option>
                                                              <option value="6">Comedy</option>
                                                              <option value="7">Nepali Events</option>

                                                    </select>
                                                </div>

                                    </div>
                                    
                                    <div style="margin-bottom: 25px" class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                                <input id="login-password" type="text" class="form-control" name="title" placeholder="Title of the File(optional)">
                                   </div>

                                   <div style="margin-bottom: 25px" class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                                <input id="login-password" type="text" class="form-control" name="description" placeholder="Information about File (optional)">
                                   </div>
                                            

                                        
                                    


                                        <div class = "form-group" style="text-align: center;">
                                                    
                                                    <div>
                                                    <div class="file-input">
                                                        <label for = "inputfile"   style="font-size: 17px;">Image file input</label>
                                                        <input type = "file" name="image[]" id ="image" style="color: red;font-size: 14px;" multiple />
                                                    </div>
                                                    </div>
                                                    <br>

                                                    <div class="privacy1">
                                                        <select class="collapse1 privacy-select" style="padding: 1.5% 0% !important; width: 27%;" role="menu" name="image_privacy" >
                          
                                                                  <option value="yes">public</option>
                                                                  <option value="no">private</option>            
                                                       </select>
                                                   </div>
                                                    <!-- <input type = "file" name="video"  id ="video" style="margin: 0% 43%;color: red;"> <br> -->
                                                    <p class = "help-block">All this field are assets for your file so you could easily search and locate in future.</p> <hr style="border-top: 1px solid #969696;">

                                                    
                                                    <hr>

                                                    <label for = "inputfile"  style="font-size: 17px;">Video file input</label><br>
                                                    <div>
                                                    <div class="file-input">
                                                        <label for="video" class="btn upload-label">select Video to upload</label>
                                                        <input type = "file"   name="video" id ="video" value="Video-file" style=";color: red; font-size: 14px;" multiple />
                                                    </div>

                                                    <div class="file-input">
                                                        <label for="thumbnail" class="btn upload-label">Select image thumbnail</label>
                                                        <input type = "file" name="thumbnail" id ="thumnnail" value="Video-file" style="color: red; font-size: 14px;" multiple />
                                                    </div>
                                                    </div>
                                                    <br>
                                                    <div class="privacy1">
                                                    <select class="collapse1 privacy-select" style="padding: 1.5% 0% !important; width: 27%;" role="menu" name="video_privacy"  ">
                      
                                                              <option value="yes">public</option>
                                                              <option value="no">private</option>
                                                              
                                                              
                                                    </select>
                                                    </div>

                                                    <!-- <input type = "file" name="video"  id ="video" style="margin: 0% 43%;color: red;"> <br> -->
                                                    <p class = "help-block">All this field are assets for your file so you could easily search and locate in future.</p> <hr style="border-top: 1px solid #969696;">

                                                    
                                                    <button type="submit" id="submit" class="btn btn-danger" >Upload your file</button>
                                                   
                                                    <progress id="progressBar" value="0" max="100" style="width:100%;margin-top: 12px;"></progress>
                                                    <h3 id="status"></h3>
                                                    <p id="loaded_n_total"></p>
                                                    </div>

                                    </form>     



                                </div>                     
                            </div>  
                </div>
              </div>
        </div>
       </div>
      </div>
    </div>






    <!-- Header End -->
    
    <!-- Contents Start -->
    
    <div class="">
        <div class="custom-container contents">
            <div class="row flex-container"> 
           
               @yield('breadcumb')
                <!-- Bread Crumb End -->
                <!-- Content Column Start -->

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 slider-360 first" style="padding-right: 12px !important;padding-left:0px !important;">
                   @yield("image-slider")
                </div>

                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 equalcol graysidebar side-col section2 third"  >    
                   @yield('sidebar')  
                </div>

               <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 equalcol conentsection section1 second" >
                    
                   @yield('content')
                   
                </div>
                <!-- Content Column End --> 
                <!-- Gray Sidebar Start -->
                
                <!-- Gray Sidebar End --> 
            </div>
        </div>
    </div>
    <!-- Contents End -->
  
  <!--- -->
  
    <div class="custom-container ">
  <!--- -->
    <!-- Footer Start -->
   @include('partials._footer')
    </div>
<!-- Wrapper End --> 
<!--// Javascript //-->
@include('partials._js')
@yield('js')
</body>
</html>