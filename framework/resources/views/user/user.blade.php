@extends('master')
@section('content')
<style type="text/css">
  .demos {
    text-align: center;
    margin-top: 20px;
    width: 490px;
    margin: auto;
  }
  .demo-image {
    cursor: url("http://tholman.com/intense-images/img/plus_cursor.png") 25 25, auto;
    display: inline-block;
    width: 220px;
    height: 220px;
    background-size: cover;
    background-position: 50% 50%;
    margin-left: 8px;
    margin-right: 8px;
    margin-bottom: 16px;
  }
  .demo-image.first {
    background-image: url("{{asset('uploads/avatars/'.Auth::user()->avatar)}}");
  }
  .demo-image.second {
    background-position: 50% 10%;
    background-image: url("{{asset('uploads/avatars/'.Auth::user()->avatar)}}");
  }
</style>
<div class="sections">
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <div class="demo-image first" data-image="{{asset('uploads/avatars/'.Auth::user()->avatar)}}" data-title=" {{Auth::user()->first_name}} {{Auth::user()->last_name}} " data-caption="User profile picture">
              </div>
              <script>
                window.onload = function() {
                  var elements = document.querySelectorAll( '.demo-image' );
                  Intense( elements );
                }
              </script>
              <h3 class="profile-username text-center">{{Auth::user()->first_name}} {{Auth::user()->last_name}}
              </h3>
              <div class="margin"></div>
              



              <div class="margin"></div>
              {{-- 
              <p class="text-muted text-center">Electronics and communication Engineer
              </p> --}}
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <div class="info-box bg-yellow">
                    <span class="info-box-icon">
                      <i class="fa fa-file-video-o" aria-hidden="true">
                      </i>
                    </span>
                    <div class="info-box-content">
                      <span class="info-box-text">Videos
                      </span>
                      <span class="info-box-number">{{$user->videos->count()}}
                      </span>
                      <div class="progress">
                        <div class="progress-bar" style="width: 50% ; display: none;">
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li class="list-group-item">
                  <div class="info-box bg-green">
                    <span class="info-box-icon">
                      <i class="fa fa-file-image-o" aria-hidden="true">
                      </i>
                    </span>
                    <div class="info-box-content">
                      <span class="info-box-text">Images
                      </span>
                      <span class="info-box-number">{{$user->images->count()}}
                      </span>
                      <div class="progress">
                        <div class="progress-bar" style="width: 20% ; display: none;">
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
             
            </div>
            <!-- /.box-header -->
            
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs" >
              <li class="active">
                <a href="#activity" data-toggle="tab">Uploads
                </a>
              </li>
              
              <li>
                <a href="#settings" data-toggle="tab">Profile edit
                </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <div class="container1">
                  <ul class="nav nav-tabs" style="border-top: 1px solid #d8d8d8; ">
                    <li class="active">
                      <a data-toggle="tab" href="#home">HOME
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#menu1">ASIA
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#menu2">AUSTRALIA and OCENIA 
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#menu3">ANTARTICA
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#menu4">EUROPE
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#menu5">AFRICA
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#menu6">SOUTH AMERICA
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#menu7">NORTH AMERICA
                      </a>
                    </li>
                  </ul>
                  <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                      <h3 style="color: #00a65a; padding-top: 20px; margin: 5px 0px !important;">select your Channel to see your content
                      </h3>
                      <h3>Here is your content safe and well arranged
                      </h3>
                      <p>select the channel name under where you have uploaded your imagages and videos 
                      </p>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                      <div class="row ">
                        <div class="collapsed_content" id="pravin_demo1SSS"> 
                          @foreach($user->subchannels as $subchannel)
                          <?php
                            $var = $subchannel->images->count(); 
                            ?>
                          @if($subchannel->images->count()>0)
                          @if($subchannel->channel_id == 1)
                          <div class=""> 
                            <div class="gap">
                            </div>
                            <div class="col-md-12"> 
                              <button class="btn btn-primary ">{{$subchannel->sub_channel_name}}
                              </button>
                            </div>
                            <div class="gap">
                            </div>                                             
                            @foreach(($subchannel->images) as $image)   
                            <?php 
                                    $count = 0;
                                    ?>
                            @if ( ($image->isthumb == 'no') && ($image->user_id == $user->id) )
                            <?php
                              $count=1;
                              ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  featured-width " id ="image{{$image->indexer}}"> 
                              <!-- Video Box Start -->
                              <div class="videobox2 card" >
                                <figure> 
                                  <iframe
                                          frameborder="0"
                                          width="100%"
                                          height=290px
                                          scrolling="no"
                                          allowfullscreen    
                                          src="{{url('index.html')}}?image={{asset('uploads/'.$image->image_id.'.'.$image->type)}}"> 
                                  </iframe>
                                  <!-- Video Thumbnail Start --> 
                                </figure>
                                <!-- Video Title Start -->
                                <span class=" mli-info ">
                                  <a href="{{url('image/$image->indexer')}}" style="color:#288bf5; padding-right: 21px; ">{{str_limit($image->title,15)}}
                                  </a>
                                  <span class="category-name" style="color:#288bf5 !important;float: right; ">Travel
                                  </span>
                                </span>
                                <button class="btn btn-warning btn-detail editmodal" type= "image" value="{{$image->indexer}}">Edit
                                </button>
                                <button class="btn btn-danger btn-delete delete-task" type= "image" value="{{$image->indexer}}">Delete
                                </button>
                                <!-- Video Title End --> 
                              </div>
                              <!-- Video Box End --> 
                            </div> 
                            @endif
                            @endforeach
                            @if($count == 0)
                           <!--  <h4>Sorry this folder is empty ..... 
                            </h4> -->
                            @endif
                          </div>
                          @endif
                          @endif
                          @endforeach
                        </div>
                      </div> 
                      <div class=" ">
                        <h4>Videos
                        </h4>
                        <hr>                  
                        @foreach($user->subchannels as $subchannel)
                        <?php
                        $var = $subchannel->images->count(); 
                        ?>
                        @if($var>0)
                        @if($subchannel->channel_id == 1)
                        <div class="row">
                          <div class="gap">
                          </div>
                          <div class="col-sm-12 col-md-12">
                            <button class="btn btn-primary ">{{$subchannel->sub_channel_name}}
                            </button>
                            <hr>
                            <div class="gap">
                            </div>
                          </div>
                          @foreach($subchannel->videos as $content)   
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id ="image{{$content->indexer}}">
                            <div class="videobox2 card" >
                              <figure> 
                                <!-- Video Thumbnail Start --> 
                                <a href="{{url("detail/$content->indexer")}}">
                                  <img src="{{ asset('uploads/'.$content->video_id.'.'.$content->thumb) }}" class="img-responsive hovereffect" style="height: 290px;" alt="" />
                                  <img class="OverlayIcon" src="{{asset('uploads/video_icon_overlay.png')}}" alt="" /> 
                                </a>    
                              </figure>
                              <!-- Video Title Start -->
                              <span class=" mli-info" >
                                <a style="color:#288bf5; href="{{url("detail/$content->indexer")}}">{{str_limit($content->title,35)}}</a>
                                  </span>
                                     <button class="btn btn-warning   btn-detail editmodal" type= "video" value="{{$content->indexer}}">Edit
                                  </button>
                                  <button class="btn btn-danger   btn-delete delete-task" type= "video" value="{{$content->indexer}}">Delete
                                  </button>
                                <!-- Video Title End --> 
                                </div>
                            </div>
                            @endforeach
                          </div>
                          @endif
                          @endif
                          @endforeach
                        </div>
                      </div>
                      <div id="menu2" class="tab-pane fade">
                        <div class=" ">
                          {{-- @foreach($data as $value) --}}
                          <div class="collapsed_content" id="pravin_demo1SSS"> 
                            @foreach($user->subchannels as $subchannel)
                            <?php
$var = $subchannel->images->count(); 
?>
                            @if($var>0)
                            @if($subchannel->channel_id == 2)
                            <div class=""> 
                              <div class="gap">
                              </div> 
                              <div class="col-sm-12 col-md-12">
                                <button class="btn btn-primary">{{$subchannel->sub_channel_name}}
                                </button>
                              </div>
                              <div class="gap">
                              </div>                                             
                              @foreach($subchannel->images as $content)   
                              <?php 
                                $count = 0;
                                ?>
                              @if ( ($content->isthumb == 'no') && ($content->user_id == $user->id) )
                              <?php 
                                  $count = 1;
                                  ?>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 featured-width " id ="image{{$content->indexer}}"> 
                                <!-- Video Box Start -->
                                <div class="videobox2 card">
                                  <figure> 
                                    <iframe
                                            frameborder="0"
                                            width="100%"
                                            height=290px
                                            scrolling="no"
                                            allowfullscreen    
                                            src="{{url('index.html')}}?image={{asset('uploads/'.$content->image_id.'.'.$content->type)}}"> 
                                    </iframe>
                                    <!-- Video Thumbnail Start --> 
                                  </figure>
                                  <!-- Video Title Start -->
                                  <span class=" mli-info ">
                                    <a href="{{url('image/$content->indexer')}}"  style="color:#288bf5;  ">{{str_limit($image->title,15)}}
                                    </a>
                                    <span class="category-name" style="color:#288bf5 !important; padding-right: 21px; float: right;">Action
                                    </span>

                                  </span>
                                  <button class="btn btn-warning btn-detail editmodal" type= "image" value="{{$content->indexer}}">Edit
                                  </button>
                                  <button class="btn btn-danger   btn-delete delete-task" type= "image" value="{{$content->indexer}}">Delete
                                  </button>
                                  <!-- Video Title End --> 
                                </div>
                                <!-- Video Box End --> 
                              </div> 
                              @endif
                              @endforeach
                              @if($count == 0)
                              <h4>There is no image in this folder..... 
                              </h4>
                              @endif
                            </div>
                            @endif
                            @endif
                            @endforeach
                          </div>
                        </div> 
                        <div class=" ">
                          <h4>Videos
                          </h4>
                          <hr>
                          @foreach($user->subchannels as $subchannel)
                          <?php
$var = $subchannel->videos->count(); 
?>
                          @if($var>0)
                          @if($subchannel->channel_id == 2)
                          <div class="">
                            <div class="col-sm-12 col-md-12">
                              <button class="btn btn-primary ">{{$subchannel->sub_channel_name}}
                              </button>
                              <div class="gap">
                              </div>
                            </div>
                            @foreach($subchannel->videos as $content)   
                            <?php 
$count = 0;
?>
                            @if($content->user_id == $user->id)
                            <?php $count = 1;?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id ="image{{$content->indexer}}">
                              <div class="videobox2 card">
                                <figure> 
                                  <!-- Video Thumbnail Start --> 
                                  <a href="{{url("detail/$content->indexer")}}">
                                    <img src="{{ asset('uploads/'.$content->video_id.'.'.$content->thumb) }}" class="img-responsive hovereffect" style="height: 290px;" alt="" />
                                    <img class="OverlayIcon" src="/paracosmatv1/uploads/video_icon_overlay.png" alt="" /> 
                                  </a>    
                                </figure>
                                <!-- Video Title Start -->
                                <span class=" mli-info" >
                                  <a href="{{url("detail/$content->indexer")}}">{{str_limit($content->title,35)}}
                                  </a>
                                </span>
                                <button class="btn btn-warning   btn-detail editmodal" type= "video" value="{{$content->indexer}}">Edit
                                </button>
                                <button class="btn btn-danger   btn-delete delete-task" type= "video" value="{{$content->indexer}}">Delete
                                </button>
                                <!-- Video Title End --> 
                              </div>
                            </div>
                            @endif       
                            @endforeach
                            @if($count == 0)
                            <!-- <h4>Sorry ,There is no Video in this folder..... 
                            </h4>-->
                            @endif
                          </div>
                          @endif
                          @endif
                          @endforeach
                        </div>
                      </div>
                      <div id="menu3" class="tab-pane fade">
                        <div class=" ">
                          {{-- @foreach($data as $value) --}}
                          <div class="collapsed_content" id="pravin_demo1SSS"> 
                            @foreach($user->subchannels as $subchannel)
                            <?php
$var = $subchannel->images->count(); 
?>
                            @if($var>0)
                            @if($subchannel->channel_id == 3)
                            <div class=""> 
                              <div class="gap">
                              </div> 
                              <div class="col-sm-12 col-md-12">
                                <button class="btn btn-primary">{{$subchannel->sub_channel_name}}
                                </button>
                              </div>
                              <div class="gap">
                              </div>                                             
                              @foreach($subchannel->images as $image)   
                              <?php $count = 0;?>
                              @if ( ($image->isthumb == 'no') && ($image->user_id == $user->id) )
                              <?php $count = 1;?>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12featured-width " id ="image{{$image->indexer}}"> 
                                <!-- Video Box Start -->
                                <div class="videobox2 card">
                                  <figure> 
                                    <iframe
                                            frameborder="0"
                                            width="100%"
                                            height=290px
                                            scrolling="no"
                                            allowfullscreen    
                                            src="{{url('index.html')}}?image={{asset('uploads/'.$image->image_id.'.'.$image->type)}}"> 
                                    </iframe>
                                    <!-- Video Thumbnail Start --> 
                                  </figure>
                                  <!-- Video Title Start -->
                                  <span class=" mli-info ">
                                    <a href="{{url('image/$image->indexer')}}"  style="color:#288bf5; padding-right: 21px; ">{{str_limit($image->title,15)}}
                                    </a>

                                    <span class="category-name" style="color:#288bf5 !important ; float: right;">Sports
                                    </span>
                                    
                                  </span>
                                  <button class="btn btn-warning   btn-detail editmodal" type= "image" value="{{$image->indexer}}">Edit
                                  </button>
                                  <button class="btn btn-danger   btn-delete delete-task" type= "image" value="{{$image->indexer}}">Delete
                                  </button>
                                  <!-- Video Title End --> 
                                </div>
                                <!-- Video Box End --> 
                              </div> 
                              @endif  
                              @endforeach
                              @if($count == 0)
                              <!-- <h4>Sorry ,There is no image in this folder.....  
                              </h4>-->
                              @endif
                            </div>
                            @endif
                            @endif
                            @endforeach
                          </div>
                        </div> 
                        <div class=" ">
                          <h4>Videos
                          </h4>
                          <hr>
                          @foreach($user->subchannels as $subchannel)
                          <?php
$var = $subchannel->videos->count(); 
?>
                          @if($var>0)
                          @if($subchannel->channel_id == 3)
                          <div class="">
                            @foreach($subchannel->videos as $content)   
                            <?php $count = 0;?>
                            @if($content->user_id == $user->id)
                            <?php $count = 1;?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id ="image{{$content->indexer}}">
                              <div class="videobox2 card">
                                <figure> 
                                  <!-- Video Thumbnail Start --> 
                                  <a href="{{url("detail/$content->indexer")}}">
                                    <img src="{{ asset('uploads/'.$content->video_id.'.'.$content->thumb) }}" class="img-responsive hovereffect" style="height: 290px;" alt="" />
                                    <img class="OverlayIcon" src="/paracosmatv1/uploads/video_icon_overlay.png" alt="" /> 
                                  </a>    
                                </figure>
                                <!-- Video Title Start -->
                                <span class=" mli-info" >
                                  <a href="{{url("detail/$content->indexer")}}">{{str_limit($content->title,35)}}
                                  </a>
                                </span>
                                <button class="btn btn-warning   btn-detail editmodal" type= "video" value="{{$content->indexer}}">Edit
                                </button>
                                <button class="btn btn-danger   btn-delete delete-task" type= "video" value="{{$content->indexer}}">Delete
                                </button>
                                <!-- Video Title End --> 
                              </div>
                            </div>
                            @endif     
                            @endforeach
                            @if($count == 0)
                            <!-- <h4>Sorry ,There is no Video in this folder.....  
                            </h4> -->
                            @endif
                          </div>
                          @endif
                          @endif
                          @endforeach
                        </div>
                      </div>
                      <div id="menu4" class="tab-pane fade">
                        <div class=" ">
                          {{-- @foreach($data as $value) --}}
                          <div class="collapsed_content" id="pravin_demo1SSS"> 
                            @foreach($user->subchannels as $subchannel)
                            <?php
$var = $subchannel->images->count(); 
?>
                            @if($var>0)
                            @if($subchannel->channel_id == 4)
                            <div class=""> 
                              <div class="gap">
                              </div> 
                              <div class="col-sm-12 col-md-12">
                                <button class="btn btn-primary">{{$subchannel->sub_channel_name}}
                                </button>
                              </div>
                              <div class="gap">
                              </div>                                             
                              @foreach($subchannel->images as $image)   
                              <?php $count = 0;?>
                              @if ( ($image->isthumb == 'no') && ($image->user_id == $user->id) )
                              <?php $count = 1;?>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 featured-width " id ="image{{$image->indexer}}"> 
                                <!-- Video Box Start -->
                                <div class="videobox2 card">
                                  <figure> 
                                    <iframe
                                            frameborder="0"
                                            width="100%"
                                            height=290px
                                            scrolling="no"
                                            allowfullscreen    
                                            src="{{url('index.html')}}?image={{asset('uploads/'.$image->image_id.'.'.$image->type)}}"> 
                                    </iframe>
                                    <!-- Video Thumbnail Start --> 
                                  </figure>
                                  <!-- Video Title Start -->
                                  <span class=" mli-info ">
                                    <span class="category-name" style="color:#288bf5 !important ; float: right;">Music
                                    </span>
                                    <a href="{{url('image/$image->indexer')}}"  style="color:#288bf5; padding-right: 21px; float: right;">
                                      {{str_limit($image->title,15)}}
                                    </a>
                                  </span>
                                  <button class="btn btn-warning   btn-detail editmodal" type= "image" value="{{$image->indexer}}">Edit
                                  </button>
                                  <button class="btn btn-danger   btn-delete delete-task" type= "image" value="{{$image->indexer}}">Delete
                                  </button>
                                  <!-- Video Title End --> 
                                </div>
                                <!-- Video Box End --> 
                              </div> 
                              @endif     
                              @endforeach
                              @if($count == 0)
                              <!-- <h4>Sorry ,There is no image in this folder.....  
                              </h4>-->
                              @endif
                            </div>
                            @endif
                            @endif
                            @endforeach
                          </div>
                        </div> 
                        <div class=" ">
                          <h4>Videos
                          </h4>
                          <hr>
                          @foreach($user->subchannels as $subchannel)
                          <?php
$var = $subchannel->videos->count(); 
?>
                          @if($var>0)
                          @if($subchannel->channel_id == 4)
                          <div class="">
                            @foreach($subchannel->videos as $content)  
                            <?php $count = 0;?> 
                            @if($content->user_id == $user->id)
                            <?php $count = 1;?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id ="image{{$content->indexer}}">
                              <div class="videobox2 card">
                                <figure> 
                                  <!-- Video Thumbnail Start --> 
                                  <a href="{{url("detail/$content->indexer")}}">
                                    <img src="{{ asset('uploads/'.$content->video_id.'.'.$content->thumb) }}" class="img-responsive hovereffect" style="height: 290px;" alt="" />
                                    <img class="OverlayIcon" src="/paracosmatv1/uploads/video_icon_overlay.png" alt="" /> 
                                  </a>    
                                </figure>
                                <!-- Video Title Start -->
                                <span class=" mli-info" >
                                  <a href="{{url("detail/$content->indexer")}}">{{str_limit($content->title,35)}}
                                  </a>
                                </span>
                                <button class="btn btn-warning   btn-detail editmodal" type= "video" value="{{$content->indexer}}">Edit
                                </button>
                                <button class="btn btn-danger   btn-delete delete-task" type= "video" value="{{$content->indexer}}">Delete
                                </button>
                                <!-- Video Title End --> 
                              </div>
                            </div>
                            @endif      
                            @endforeach
                            @if($count == 0)
                            <!-- <h4>Sorry ,There is no Video in this folder..... 
                            </h4>-->
                            @endif
                          </div>
                          @endif
                          @endif
                          @endforeach
                        </div>
                      </div>
                      <div id="menu5" class="tab-pane fade">
                        <div class=" ">
                          {{-- @foreach($data as $value) --}}
                          <div class="collapsed_content" id="pravin_demo1SSS"> 
                            @foreach($user->subchannels as $subchannel)
                            <?php
$var = $subchannel->images->count(); 
?>
                            @if($var>0)
                            @if($subchannel->channel_id == 5)
                            <div class="row"> 
                              <div class="gap">
                              </div> 
                              <div class="col-sm-12 col-md-12">
                                <button class="btn btn-primary">{{$subchannel->sub_channel_name}}
                                </button>
                              </div>
                              <div class="gap">
                              </div>                                             
                              @foreach($subchannel->images as $image)   
                              <?php $count = 0;?>
                              @if ( ($image->isthumb == 'no') && ($image->user_id == $user->id) )
                              <?php $count = 1;?>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 featured-width " id ="image{{$image->indexer}}"> 
                                <!-- Video Box Start -->
                                <div class="videobox2 card">
                                  <figure> 
                                    <iframe
                                            frameborder="0"
                                            width="100%"
                                            height=290px
                                            scrolling="no"
                                            allowfullscreen    
                                            src="{{url('index.html')}}?image={{asset('uploads/'.$image->image_id.'.'.$image->type)}}"> 
                                    </iframe>
                                    <!-- Video Thumbnail Start --> 
                                  </figure>
                                  <!-- Video Title Start -->
                                  <span class=" mli-info ">
                                    <span class="category-name" style="color:#288bf5 !important;float: right;">Events
                                    </span>
                                    <a href="{{url('image/$content->indexer')}}"  style="color:#288bf5; padding-right: 21px; ">{{str_limit($image->title,15)}}
                                    </a>
                                  </span>
                                  <button class="btn btn-warning   btn-detail editmodal" type= "image" value="{{$image->indexer}}">Edit
                                  </button>
                                  <button class="btn btn-danger   btn-delete delete-task" type= "image" value="{{$image->indexer}}">Delete
                                  </button>
                                  <!-- Video Title End --> 
                                </div>
                                <!-- Video Box End --> 
                              </div> 
                              @endif      
                              @endforeach
                              @if($count == 0)
                              <!-- <h4>Sorry ,There is no image in this folder.....  
                              </h4>-->
                              @endif
                            </div>
                            @endif
                            @endif
                            @endforeach
                          </div>
                        </div> 
                        <div class=" ">
                          <h4>Videos
                          </h4>
                          <hr>
                          @foreach($user->subchannels as $subchannel)
                          <?php
$var = $subchannel->videos->count(); 
?>
                          @if($var>0)
                          @if($subchannel->channel_id == 5)
                          <div class="">
                            @foreach($subchannel->videos as $content)
                            <?php $count = 0;?>   
                            @if($content->user_id == $user->id)
                            <?php $count = 0;?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id ="image{{$content->indexer}}">
                              <div class="videobox2 card">
                                <figure> 
                                  <!-- Video Thumbnail Start --> 
                                  <a href="{{url("detail/$content->indexer")}}">
                                    <img src="{{ asset('uploads/'.$content->video_id.'.'.$content->thumb) }}" class="img-responsive hovereffect" style="height: 290px;" alt="" />
                                    <img class="OverlayIcon" src="/paracosmatv1/uploads/video_icon_overlay.png" alt="" /> 
                                  </a>    
                                </figure>
                                <!-- Video Title Start -->
                                <span class=" mli-info" >
                                  <a href="{{url("detail/$content->indexer")}}">{{str_limit($content->title,35)}}
                                  </a>
                                </span>
                                <button class="btn btn-warning btn-detail editmodal" type= "video" value="{{$content->indexer}}">Edit
                                </button>
                                <button class="btn btn-danger   btn-delete delete-task" type= "video" value="{{$content->indexer}}">Delete
                                </button>
                                <!-- Video Title End --> 
                              </div>
                            </div>
                            @endif
                            @endforeach
                            @if($count == 0)
                            <!-- <h4>Sorry ,There is no Video in this folder..... 
                            </h4>-->
                            @endif
                          </div>
                          @endif
                          @endif
                          @endforeach
                        </div>
                      </div>
                      <div id="menu6" class="tab-pane fade">
                        <div class=" ">
                          {{-- @foreach($data as $value) --}}
                          <div class="collapsed_content" id="pravin_demo1SSS"> 
                            @foreach($user->subchannels as $subchannel)
                            <?php
                                $var = $subchannel->images->count(); 
                                ?>
                            @if($var>0)
                            @if($subchannel->channel_id == 6)
                            <div class=""> 
                              <div class="gap">
                              </div> 
                              <div class="col-sm-12 col-md-12">
                                <button class="btn btn-primary">{{$subchannel->sub_channel_name}}
                                </button>
                              </div>
                              <div class="gap">
                              </div>                                             
                              @foreach($subchannel->images as $image)   
                              <?php $count = 0;?>
                              @if ( ($image->isthumb == 'no') && ($image->user_id == $user->id) )
                              <?php $count = 1;?>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 featured-width " id ="image{{$content->indexer}}"> 
                                <!-- Video Box Start -->
                                <div class="videobox2 card">
                                  <figure> 
                                    <iframe
                                            frameborder="0"
                                            width="100%"
                                            height=290px
                                            scrolling="no"
                                            allowfullscreen    
                                            src="{{url('index.html')}}?image={{asset('uploads/'.$image->image_id.'.'.$image->type)}}"> 
                                    </iframe>
                                    <!-- Video Thumbnail Start --> 
                                  </figure>
                                  <!-- Video Title Start -->
                                  <span class=" mli-info ">
                                    <span class="category-name" style="color:#288bf5 !important ; float: right;">Comedy
                                    </span>
                                    <a href="{{url('image/$image->indexer')}}"  style="color:#288bf5; padding-right: 21px;">{{str_limit($image->title,15)}}
                                    </a>
                                  </span>
                                  <button class="btn btn-warning   btn-detail editmodal" type= "image" value="{{$image->indexer}}">Edit
                                  </button>
                                  <button class="btn btn-danger   btn-delete delete-task" type= "image" value="{{$image->indexer}}">Delete
                                  </button>
                                  <!-- Video Title End --> 
                                </div>
                                <!-- Video Box End --> 
                              </div> 
                              @endif  
                              @endforeach
                              @if($count == 0)
                              <!-- <h4>Sorry ,There is no image in this folder.....  
                              </h4>-->
                              @endif
                            </div>
                            @endif
                            @endif
                            @endforeach
                          </div>
                        </div> 
                        <div class=" ">
                          <h4>Videos
                          </h4>
                          <hr>
                          @foreach($user->subchannels as $subchannel)
                          <?php
$var = $subchannel->videos->count(); 
?>
                          @if($var>0)
                          @if($subchannel->channel_id == 6)
                          <div class="">
                            @foreach($subchannel->videos as $content)  
                            <?php $count = 0;?> 
                            @if($content->user_id == $user->id)
                            <?php $count = 1;?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id ="image{{$content->indexer}}">
                              <div class="videobox2 card">
                                <figure> 
                                  <!-- Video Thumbnail Start --> 
                                  <a href="{{url("detail/$content->indexer")}}">
                                    <img src="{{ asset('uploads/'.$content->video_id.'.'.$content->thumb) }}" class="img-responsive hovereffect" style="height: 290px;" alt="" />
                                    <img class="OverlayIcon" src="/paracosmatv1/uploads/video_icon_overlay.png" alt="" /> 
                                  </a>    
                                </figure>
                                <!-- Video Title Start -->
                                <span class=" mli-info" >
                                  <a href="{{url("detail/$content->indexer")}}">{{str_limit($content->title,35)}}
                                  </a>
                                </span>
                                <button class="btn btn-warning   btn-detail editmodal" type= "video" value="{{$content->indexer}}">Edit
                                </button>
                                <button class="btn btn-danger   btn-delete delete-task" type= "video" value="{{$content->indexer}}">Delete
                                </button>
                                <!-- Video Title End --> 
                              </div>
                            </div>
                            @endif      
                            @endforeach
                            @if($count == 0)
                            <!-- <h4>Sorry ,There is no Video in this folder..... 
                            </h4>-->
                            @endif
                          </div>
                          @endif
                          @endif
                          @endforeach
                        </div>
                      </div>
                      <div id="menu7" class="tab-pane fade">
                        <div class=" ">
                          {{-- @foreach($data as $value) --}}
                          <div class="collapsed_content" id="pravin_demo1SSS"> 
                            @foreach($user->subchannels as $subchannel)
                            <?php
$var = $subchannel->images->count(); 
?>
                            @if($var>0)
                            @if($subchannel->channel_id == 7)
                            <div class=""> 
                              <div class="gap">
                              </div> 
                              <div class="col-sm-12 col-md-12" id ="image{{$content->indexer}}">
                                <button class="btn btn-primary">{{$subchannel->sub_channel_name}}
                                </button>
                              </div>
                              <div class="gap">
                              </div>                                             
                              @foreach($subchannel->images as $image)   
                              <?php $count = 0;?>
                              @if ( ($image->isthumb == 'no') && ($image->user_id == $user->id) )
                              <?php $count = 1;?>
                              <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12 hidden-xs hidden-sm featured-width "> 
                                <!-- Video Box Start -->
                                <div class="videobox2 card">
                                  <figure> 
                                    <iframe
                                            frameborder="0"
                                            width="100%"
                                            height=290px
                                            scrolling="no"
                                            allowfullscreen    
                                            src="{{url('index.html')}}?image={{asset('uploads/'.$image->image_id.'.'.$image->type)}}"> 
                                    </iframe>
                                    <!-- Video Thumbnail Start --> 
                                  </figure>
                                  <!-- Video Title Start -->
                                  <span class=" mli-info ">
                                    <span class="category-name" style="color:#288bf5 !important;float: right;">Animated
                                    </span>
                                    <a href="{{url('image/$image->indexer')}}"  style="color:#288bf5; padding-right: 21px; ">{{str_limit($image->title,15)}}
                                    </a>
                                  </span>
                                  <button class="btn btn-warning   btn-detail editmodal" type= "image" value="{{$image->indexer}}">Edit
                                  </button>
                                  <button class="btn btn-danger   btn-delete delete-task" type= "image" value="{{$image->indexer}}">Delete
                                  </button>
                                  <!-- Video Title End --> 
                                </div>
                                <!-- Video Box End --> 
                              </div> 
                              @endif      
                              @endforeach
                              @if($count == 0)
                              <!-- <h4>Sorry ,There is no image in this folder.....  
                              </h4>-->
                              @endif
                            </div>
                            @endif
                            @endif
                            @endforeach
                          </div>
                        </div> 
                        <div class=" ">
                          <h4>Videos
                          </h4>
                          <hr>
                          @foreach($user->subchannels as $subchannel)
                          <?php
$var = $subchannel->videos->count(); 
?>
                          @if($var>0)
                          @if($subchannel->channel_id == 7)
                          <div class="">
                            @foreach($subchannel->videos as $content) 
                            <?php $count = 0;?>  
                            @if($content->user_id == $user->id) 
                            <?php $count = 1;?>
                            <div class="col-lg-4 col-md-3 col-xs-6" id ="image{{$content->indexer}}">
                              <div class="videobox2 card">
                                <figure> 
                                  <!-- Video Thumbnail Start --> 
                                  <a href="{{url("detail/$content->indexer")}}">
                                    <img src="{{ asset('uploads/'.$content->video_id.'.'.$content->thumb) }}" class="img-responsive hovereffect" style="height: 290px;" alt="" />
                                    <img class="OverlayIcon" src="/paracosmatv1/uploads/video_icon_overlay.png" alt="" /> 
                                  </a>    
                                </figure>
                                <!-- Video Title Start -->
                                <span class=" mli-info" >
                                  <a href="{{url("detail/$content->indexer")}}">{{str_limit($content->title,35)}}
                                  </a>
                                </span>
                                <button class="btn btn-warning   btn-detail editmodal" type= "video" value="{{$content->indexer}}">Edit
                                </button>
                                <button class="btn btn-danger   btn-delete delete-task" type= "video" value="{{$content->indexer}}">Delete
                                </button>
                                <!-- Video Title End --> 
                              </div>
                            </div>
                            @endif       
                            @endforeach
                            @if($count == 0)
                           <!--  <h4>Sorry ,There is no video in this folder.....
                            </h4> -->
                            @endif
                          </div>
                          @endif
                          @endif
                          @endforeach
                        </div>
                      </div>
                      <div id="menu8" class="tab-pane fade">
                        <div class=" ">
                          {{-- @foreach($data as $value) --}}
                          <div class="collapsed_content" id="pravin_demo1SSS"> 
                            @foreach($user->subchannels as $subchannel)
                            <?php
$var = $subchannel->images->count(); 
?>
                            @if($var>0)
                            @if($subchannel->channel_id == 8)
                            <div class=""> 
                              <div class="gap">
                              </div> 
                              <div class="col-sm-12 col-md-12">
                                <button class="btn btn-primary">{{$subchannel->sub_channel_name}}
                                </button>
                              </div>
                              <div class="gap">
                              </div>                                             
                              @foreach($subchannel->images as $image)   
                              <?php $count = 0;?>
                              @if ( ($image->isthumb == 'no') && ($image->user_id == $user->id) )
                              <?php $count = 1;?>
                              <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12 hidden-xs hidden-sm featured-width "> 
                                <!-- Video Box Start -->
                                <div class="videobox2 card">
                                  <figure> 
                                    <iframe
                                            frameborder="0"
                                            width="100%"
                                            height=290px
                                            scrolling="no"
                                            allowfullscreen    
                                            src="{{url('index.html')}}?image={{asset('uploads/'.$image->image_id.'.'.$image->type)}}"> 
                                    </iframe>
                                    <!-- Video Thumbnail Start --> 
                                  </figure>
                                  <!-- Video Title Start -->
                                  <span class=" mli-info ">
                                    <span class="category-name" style="color:#288bf5 !important; float: right;">Nepali Events
                                    </span>
                                    <a href="{{url('image/$image->indexer')}}"  style="color:#288bf5; padding-right: 21px;">{{str_limit($image->title,15)}}
                                    </a>
                                  </span>
                                  <button class="btn btn-danger edit  " data-toggle="modal" data-target="#editform">Edit
                                  </button>
                                  <!-- Video Title End --> 
                                </div>
                                <!-- Video Box End --> 
                              </div> 
                              @endif   
                              @endforeach
                              @if($count == 0)
                              <!-- <h4>Sorry ,There is no image in this folder.....
                              </h4> -->
                              @endif
                            </div>
                            @endif
                            @endif
                            @endforeach
                          </div>
                        </div> 
                        <div class=" ">
                          <h4>Videos
                          </h4>
                          <hr>
                          @foreach($user->subchannels as $subchannel)
                          <?php
$var = $subchannel->videos->count(); 
?>
                          @if($var>0)
                          @if($subchannel->channel_id == 8)
                          <div class="">
                            @foreach($subchannel->videos as $content) 
                            <?php $count = 0;?>  
                            @if($content->user_id == $user->id)
                            <?php $count = 1;?>
                            <div class="col-lg-4">
                              <div class="videobox2 card">
                                <figure> 
                                  <!-- Video Thumbnail Start --> 
                                  <a href="{{url("detail/$content->indexer")}}">
                                    <img src="{{ asset('uploads/'.$content->video_id.'.'.$content->thumb) }}" class="img-responsive hovereffect" style="height: 290px;" alt="" />
                                    <img class="OverlayIcon" src="/paracosmatv1/uploads/video_icon_overlay.png" alt="" /> 
                                  </a>    
                                </figure>
                                <!-- Video Title Start -->
                                <span class=" mli-info" >
                                  <a href="{{url("detail/$content->indexer")}}">{{str_limit($content->title,35)}}
                                  </a>
                                </span>
                                <button class="btn btn-warning   btn-detail editmodal" type= "video" value="{{$content->indexer}}">Edit
                                </button>
                                <button class="btn btn-danger   btn-delete delete-task" type= "video" value="{{$content->indexer}}">Delete
                                </button>
                                <!-- Video Title End --> 
                              </div>
                            </div>
                            @endif    
                            @endforeach
                            @if($count == 0)
                           <!--  <h4>Sorry ,There is no video in this folder.....
                            </h4> -->
                            @endif
                          </div>
                          @endif
                          @endif
                          @endforeach
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              
                <!-- /.tab-pane -->
                <div class="tab-pane profile" id="settings">
                  <div class="" id="settings">
                    <div class="">
                      {{-- {{ $user->images}} --}}
                      <div class="col-md-12 center" >
                        <hr>
                        <img src="{{asset('/uploads/avatars/'.$user->avatar)}}" style="width:150px; height:150px;  border-radius:50%; margin-right:25px;" id="profile-img-tag">
                         </div>
                        <form enctype="multipart/form-data" class="center" action="{{route('profile.store')}}" method="POST" >
                          {{csrf_field()}}
                          <label>Update Profile Image
                          </label>
                          <input type="file" name="avatar" id="imgInp" class="center_input">
                          <hr>
                          
                        
                        <h2>{{ $user->first_name}}'s Profile
                        </h2>
                     
                        <div class="gap">
                        </div>
                        <div class="gap">
                        </div>
                        <div class="form-group">
                          <hr>
                          <label for="inputfirstname" class=" col-sm-2 control-label">First name 
                          </label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control profile_edit_input" id="inputName" name="first_name" placeholder="firsdt name">
                          </div>
                        </div>

                        <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fas fa-id-card-alt"></i></div>
                    <input class="form-control input-lg" type="email" id="email" name="email" placeholder="Your Email*" required="required" data-validation-required-message="Please enter your email address."/>
                  </div>
                </div>
                


                        <div class="form-group">
                          <label for="inputlastname" class=" col-sm-2  control-label">Last name 
                          </label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control profile_edit_input" id="inputlastname" name="last_name" placeholder="last name ">
                          </div>
                        </div>



                        <div class="form-group">
                          <label for="inputEmail" class=" col-sm-2  control-label">Email
                          </label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control profile_edit_input" id="inputEmail" name="email" placeholder="additional Email">
                          </div>
                        </div>
                        
                       <!--  <div class="form-group">
                          <label for="location" class="col-sm-2 control-label">Location
                          </label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control profile_edit_input" id="location"  name ="location" >
                          </div>
                        </div>
                        

                        <div class="form-group">
                          <label for="inputExperience" class="col-sm-2 control-label">About me
                          </label>
                          <div class="col-sm-10">
                            <textarea class="form-control" id="inputExperience profile_edit_input" placeholder="Experience" name="aboutme">
                            </textarea>
                          </div>
                        </div> -->
                    

                       
                            <button type="submit" class="btn btn-primary margin_ul">Update Profile
                            </button>
                       
                      </form>
                    </div>
                  </div>
                  
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        </section>
      <!-- /.content -->
      </div>
    <div class="gap">
    </div>
    <div class="gap">
    </div>
    <!-- section for tab inside tab end here -->
  </div>
  @stop @section('sidebar') @include('home.sidebar') @stop
