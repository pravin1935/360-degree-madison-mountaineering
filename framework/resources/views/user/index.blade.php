
@extends('master')

@section('title_page')
 ParacosmaTV | 360 Videos
@stop 

@section('content')
	<div class="container">
		<div class="row">
		{{-- {{ $user->images}} --}}
			<div class="col-md-10 col-md-offset-1">
				<img src="{{asset('/uploads/avatars/'.$user->avatar)}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">

				<form enctype="multipart/form-data" action="{{route('profile.store')}}" method="POST">
				{{csrf_field()}}
					<label>Update Profile Image</label>
					<input type="file" name="avatar">
					<input type="submit" class="pull-right btn btn-sm btn-primary">

				</form>
				<h2>{{ $user->first_name}}'s Profile</h2>
			</div>
		</div>
	</div>


                 <div class="sections">
                        <h2 class="heading">User content List</h2>
                        <div class="clearfix"></div>
                       
                            
                            @foreach($user->videos->chunk(3) as $chunk)
                             <div class="row">
                            @foreach($chunk as $video)   
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12"> 
                                <!-- Video Box Start -->
                                <div class="videobox2">
                                    <figure> 
                                        <!-- Video Thumbnail Start --> 
                                        <a href="{{url("detail/$video->indexer")}}">
                                            <img src="{{ asset('uploads/'.$video->video_id.'.'.$video->thumb) }}" class="img-responsive hovereffect" alt="" />
                                        </a> 
                                        <!-- Video Thumbnail End --> 
                                        <!-- Video Info Start -->
                                        <div class="vidopts">
                                            <ul>
                                                <li><i class="fa fa-heart"></i>{{$video->number_of_views}}</li>
                                                <li><i class="fa fa-clock-o"></i>{{$video->video_length}}</li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                       <!-- Video Info End --> 
                                    </figure>
                                    <!-- Video Title Start -->
                                    <h4><a href="{{url("detail/$video->indexer")}}">{{str_limit($video->title,30)}}</a></h4>
                                    <!-- Video Title End --> 
                                </div>
                                <!-- Video Box End --> 
                            </div>
                            <ul class="categories hidden-md hidden-lg">
                                <li style="padding: 5px 10px; border-bottom: 1px solid #fff; margin-bootm: 4px; font-size: 16px;">
                                <a style="font-weight: normal; font-size: 14px; color: #1F97D4" href="{{url("detail/$video->indexer")}}">{{str_limit($video->title,35)}}</a></li>
                            </ul>
                            @endforeach
                             </div>
                            @endforeach
                          {!! $videos->render() !!}
                       
                    </div> 
                    <div class="link">
                @foreach($user->images->chunk(4) as $chunk)
                            <div class="row">
                            @foreach($chunk as $feature)
                          
                                 <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hidden-xs hidden-sm featured-width "> 
                                      <!-- Video Box Start -->
                                      <div class="videobox2">
                                          <figure> 
                                        
                                          <iframe
                                          frameborder="0"
                                          width="100%"
                                          height=290px
                                          scrolling="no"
                                          allowfullscreen    
                                          src="vrview/index.html?image={{asset('uploads/'.$feature->image_id. '.' .$feature->type)}}  &is_stereo=true"> 
                                          
                                          </iframe>
                                        
                                              <!-- Video Thumbnail Start --> 
                                              
                                               
                                          </figure>
                                          <!-- Video Title Start -->
                                          
                                          <span class=" mli-info " style="background: url('/paracosmatv/images/mask-title.png') top repeat-x;">
                                            <h4 class="category-name" style="color: white !important;">{{(App\Channel::find($feature->channel_id)->channel_name)}}</h4>
                                            <a href="{{url('detail/$feature->indexer')}}" >{{str_limit($feature->title,10)}}</a></span>
                                          
                                          <!-- <span>
                                            Channel: {{(App\Channel::find($feature->channel_id)->channel_name)}}
                                          </span> -->
                                          
                                          <!-- Video Title End --> 
                                      </div>
                                      <!-- Video Box End --> 
                                  </div>
                              
                            
              <ul class="categories hidden-md hidden-lg">
                <li style="padding: 5px 10px; border-bottom: 1px solid #fff; margin-bootm: 4px; font-size: 16px;">
                <a style="font-weight: bold; font-size: 14px; color: #1F97D4" href="{{url('detail/$feature->indexer')}}">{{str_limit($feature->title,35)}}</a></li>
              </ul>
                        @endforeach

                        </div>
             @endforeach
             </div>
@stop
@section('sidebar')

@include('home.sidebar')

@stop