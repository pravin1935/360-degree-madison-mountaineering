<?php return array (
  'app' => 
  array (
    'debug' => true,
    'url' => 'http://localhost',
    'timezone' => 'UTC',
    'locale' => 'en',
    'fallback_locale' => 'en',
    'key' => 'b9NMU5wcexawiNB6VeW28fyfpTvA8R9U',
    'cipher' => 'AES-256-CBC',
    'log' => 'single',
    'providers' => 
    array (
      0 => 'Illuminate\\Foundation\\Providers\\ArtisanServiceProvider',
      1 => 'Illuminate\\Auth\\AuthServiceProvider',
      2 => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
      3 => 'Illuminate\\Bus\\BusServiceProvider',
      4 => 'Illuminate\\Cache\\CacheServiceProvider',
      5 => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
      6 => 'Illuminate\\Routing\\ControllerServiceProvider',
      7 => 'Illuminate\\Cookie\\CookieServiceProvider',
      8 => 'Illuminate\\Database\\DatabaseServiceProvider',
      9 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
      10 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
      11 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
      12 => 'Illuminate\\Hashing\\HashServiceProvider',
      13 => 'Illuminate\\Mail\\MailServiceProvider',
      14 => 'Illuminate\\Pagination\\PaginationServiceProvider',
      15 => 'Illuminate\\Pipeline\\PipelineServiceProvider',
      16 => 'Illuminate\\Queue\\QueueServiceProvider',
      17 => 'Illuminate\\Redis\\RedisServiceProvider',
      18 => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
      19 => 'Illuminate\\Session\\SessionServiceProvider',
      20 => 'Illuminate\\Translation\\TranslationServiceProvider',
      21 => 'Illuminate\\Validation\\ValidationServiceProvider',
      22 => 'Illuminate\\View\\ViewServiceProvider',
      23 => 'Collective\\Html\\HtmlServiceProvider',
      24 => 'Laravel\\Socialite\\SocialiteServiceProvider',
      25 => 'App\\Providers\\AppServiceProvider',
      26 => 'App\\Providers\\EventServiceProvider',
      27 => 'App\\Providers\\RouteServiceProvider',
      28 => 'App\\Providers\\SideBarNavigationProvider',
      29 => 'App\\Providers\\HeaderNavigationProvider',
      30 => 'Intervention\\Image\\ImageServiceProvider',
      31 => 'Pbmedia\\LaravelFFMpeg\\FFMpegServiceProvider',
    ),
    'aliases' => 
    array (
      'App' => 'Illuminate\\Support\\Facades\\App',
      'Artisan' => 'Illuminate\\Support\\Facades\\Artisan',
      'Auth' => 'Illuminate\\Support\\Facades\\Auth',
      'Blade' => 'Illuminate\\Support\\Facades\\Blade',
      'Bus' => 'Illuminate\\Support\\Facades\\Bus',
      'Cache' => 'Illuminate\\Support\\Facades\\Cache',
      'Config' => 'Illuminate\\Support\\Facades\\Config',
      'Cookie' => 'Illuminate\\Support\\Facades\\Cookie',
      'Crypt' => 'Illuminate\\Support\\Facades\\Crypt',
      'DB' => 'Illuminate\\Support\\Facades\\DB',
      'Eloquent' => 'Illuminate\\Database\\Eloquent\\Model',
      'Event' => 'Illuminate\\Support\\Facades\\Event',
      'File' => 'Illuminate\\Support\\Facades\\File',
      'Hash' => 'Illuminate\\Support\\Facades\\Hash',
      'Input' => 'Illuminate\\Support\\Facades\\Input',
      'Inspiring' => 'Illuminate\\Foundation\\Inspiring',
      'Lang' => 'Illuminate\\Support\\Facades\\Lang',
      'Log' => 'Illuminate\\Support\\Facades\\Log',
      'Mail' => 'Illuminate\\Support\\Facades\\Mail',
      'Password' => 'Illuminate\\Support\\Facades\\Password',
      'Queue' => 'Illuminate\\Support\\Facades\\Queue',
      'Redirect' => 'Illuminate\\Support\\Facades\\Redirect',
      'Redis' => 'Illuminate\\Support\\Facades\\Redis',
      'Request' => 'Illuminate\\Support\\Facades\\Request',
      'Response' => 'Illuminate\\Support\\Facades\\Response',
      'Route' => 'Illuminate\\Support\\Facades\\Route',
      'Schema' => 'Illuminate\\Support\\Facades\\Schema',
      'Session' => 'Illuminate\\Support\\Facades\\Session',
      'Storage' => 'Illuminate\\Support\\Facades\\Storage',
      'URL' => 'Illuminate\\Support\\Facades\\URL',
      'Validator' => 'Illuminate\\Support\\Facades\\Validator',
      'View' => 'Illuminate\\Support\\Facades\\View',
      'Form' => 'Collective\\Html\\FormFacade',
      'Html' => 'Collective\\Html\\HtmlFacade',
      'Socialite' => 'Laravel\\Socialite\\Facades\\Socialite',
      'Imge' => 'Intervention\\Image\\Facades\\Image',
      'FFMpeg' => 'Pbmedia\\Laravel\\FFMpegFacade',
    ),
  ),
  'auth' => 
  array (
    'driver' => 'eloquent',
    'model' => 'App\\User',
    'table' => 'users',
    'password' => 
    array (
      'email' => 'emails.password',
      'table' => 'password_resets',
      'expire' => 60,
    ),
  ),
  'broadcasting' => 
  array (
    'default' => 'pusher',
    'connections' => 
    array (
      'pusher' => 
      array (
        'driver' => 'pusher',
        'key' => NULL,
        'secret' => NULL,
        'app_id' => NULL,
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
      'log' => 
      array (
        'driver' => 'log',
      ),
    ),
  ),
  'cache' => 
  array (
    'default' => 'file',
    'stores' => 
    array (
      'apc' => 
      array (
        'driver' => 'apc',
      ),
      'array' => 
      array (
        'driver' => 'array',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'cache',
        'connection' => NULL,
      ),
      'file' => 
      array (
        'driver' => 'file',
        'path' => 'C:\\xampp\\htdocs\\madison_tv_v2\\framework\\storage\\framework/cache',
      ),
      'memcached' => 
      array (
        'driver' => 'memcached',
        'servers' => 
        array (
          0 => 
          array (
            'host' => '127.0.0.1',
            'port' => 11211,
            'weight' => 100,
          ),
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
    ),
    'prefix' => 'laravel',
  ),
  'compile' => 
  array (
    'files' => 
    array (
    ),
    'providers' => 
    array (
    ),
  ),
  'database' => 
  array (
    'fetch' => 8,
    'default' => 'mysql',
    'connections' => 
    array (
      'sqlite' => 
      array (
        'driver' => 'sqlite',
        'database' => 'C:\\xampp\\htdocs\\madison_tv_v2\\framework\\storage\\database.sqlite',
        'prefix' => '',
      ),
      'mysql' => 
      array (
        'driver' => 'mysql',
        'host' => 'localhost',
        'database' => 'a2',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => '',
        'strict' => false,
      ),
      'pgsql' => 
      array (
        'driver' => 'pgsql',
        'host' => 'localhost',
        'database' => 'forge',
        'username' => 'forge',
        'password' => '',
        'charset' => 'utf8',
        'prefix' => '',
        'schema' => 'public',
      ),
      'sqlsrv' => 
      array (
        'driver' => 'sqlsrv',
        'host' => 'localhost',
        'database' => 'forge',
        'username' => 'forge',
        'password' => '',
        'charset' => 'utf8',
        'prefix' => '',
      ),
    ),
    'migrations' => 'migrations',
    'redis' => 
    array (
      'cluster' => false,
      'default' => 
      array (
        'host' => '127.0.0.1',
        'port' => 6379,
        'database' => 0,
      ),
    ),
  ),
  'filesystems' => 
  array (
    'default' => 'local',
    'cloud' => 's3',
    'disks' => 
    array (
      'local' => 
      array (
        'driver' => 'local',
        'root' => 'C:\\xampp\\htdocs\\madison_tv_v2\\framework\\storage\\app',
      ),
      'ftp' => 
      array (
        'driver' => 'ftp',
        'host' => 'ftp.example.com',
        'username' => 'your-username',
        'password' => 'your-password',
      ),
      's3' => 
      array (
        'driver' => 's3',
        'key' => 'AKIAIYA3E2NUIQQXBR6A',
        'secret' => 'qljJh/1sr/RDKTn+vfLfhRPVYxI361fyqimLOOSx',
        'region' => 'us-west-1',
        'bucket' => 'madisontv',
      ),
      'rackspace' => 
      array (
        'driver' => 'rackspace',
        'username' => 'your-username',
        'key' => 'your-key',
        'container' => 'your-container',
        'endpoint' => 'https://identity.api.rackspacecloud.com/v2.0/',
        'region' => 'IAD',
        'url_type' => 'publicURL',
      ),
    ),
  ),
  'images' => 
  array (
    28 => 
    array (
      'sd_img' => 'terai_tv_SD',
      'hd_img' => 'terai_tv_HD',
    ),
    7 => 
    array (
      'sd_img' => 'SonyTV_SD',
      'hd_img' => 'SonyTV_HD',
    ),
    34 => 
    array (
      'sd_img' => 'News24_SD',
      'hd_img' => 'News24_HD',
    ),
    13 => 
    array (
      'sd_img' => 'NepalTV_SD',
      'hd_img' => 'NepalTV_HD',
    ),
    29 => 
    array (
      'sd_img' => 'nbex_mountain_tv_SD',
      'hd_img' => 'nbex_mountain_tv_HD',
    ),
    1 => 
    array (
      'sd_img' => 'Kantipur_TV_SD',
      'hd_img' => 'Kantipur_TV_HD',
    ),
    2 => 
    array (
      'sd_img' => 'ImageChannel_SD',
      'hd_img' => 'ImageChannel_HD',
    ),
    27 => 
    array (
      'sd_img' => 'Himalaya_TV_SD',
      'hd_img' => 'Himalaya_TV_HD',
    ),
    19 => 
    array (
      'sd_img' => 'colors_SD',
      'hd_img' => 'colors_HD',
    ),
    3 => 
    array (
      'sd_img' => 'Avenues_SD',
      'hd_img' => 'Avenues_HD',
    ),
    14 => 
    array (
      'sd_img' => 'ABC_SD',
      'hd_img' => 'ABC_HD',
    ),
    4 => 
    array (
      'sd_img' => 'comedy_SD',
      'hd_img' => 'comedy_HD',
    ),
    17 => 
    array (
      'sd_img' => 'events-shows_SD',
      'hd_img' => 'events-shows_HD',
    ),
    5 => 
    array (
      'sd_img' => 'zeetv_SD',
      'hd_img' => 'zeetv_HD',
    ),
    21 => 
    array (
      'sd_img' => 'GlobalNRN_SD',
      'hd_img' => 'GlobalNRN_HD',
    ),
    23 => 
    array (
      'sd_img' => 'TodayHigh_SD',
      'hd_img' => 'TodayHigh_HD',
    ),
    20 => 
    array (
      'sd_img' => 'StarPlus_SD',
      'hd_img' => 'StarPlus_HD',
    ),
    6 => 
    array (
      'sd_img' => 'LifeOK_SD',
      'hd_img' => 'LifeOK_HD',
    ),
    15 => 
    array (
      'sd_img' => 'SpecialNews_SD',
      'hd_img' => 'SpecialNews_HD',
    ),
    10 => 
    array (
      'sd_img' => 'PopSongs_SD',
      'hd_img' => 'PopSongs_HD',
    ),
    24 => 
    array (
      'sd_img' => 'Community_SD',
      'hd_img' => 'Community_HD',
    ),
    32 => 
    array (
      'sd_img' => 'TalkShows_SD',
      'hd_img' => 'TalkShows_HD',
    ),
    11 => 
    array (
      'sd_img' => 'NepaliLokGeet_SD',
      'hd_img' => 'NepaliLokGeet_HD',
    ),
    33 => 
    array (
      'sd_img' => 'MNTV_SD',
      'hd_img' => 'MNTV_HD',
    ),
    9 => 
    array (
      'sd_img' => 'NepaliMovies_SD',
      'hd_img' => 'NepaliMovies_HD',
    ),
    8 => 
    array (
      'sd_img' => 'NepaliEvents_SD',
      'hd_img' => 'NepaliEvents_HD',
    ),
    22 => 
    array (
      'sd_img' => 'MahaProgram_SD',
      'hd_img' => 'MahaProgram_HD',
    ),
    31 => 
    array (
      'sd_img' => 'HongKong_SD',
      'hd_img' => 'HongKong_HD',
    ),
    12 => 
    array (
      'sd_img' => 'HindiSongs_SD',
      'hd_img' => 'HindiSongs_HD',
    ),
    16 => 
    array (
      'sd_img' => 'HindiMovies_SD',
      'hd_img' => 'HindiMovies_HD',
    ),
    18 => 
    array (
      'sd_img' => 'EnglishMovies_SD',
      'hd_img' => 'EnglishMovies_HD',
    ),
    26 => 
    array (
      'sd_img' => 'NRNVoice_SD',
      'hd_img' => 'NRNVoice_HD',
    ),
  ),
  'laravel-ffmpeg' => 
  array (
    'default_disk' => 'local',
    'ffmpeg.binaries' => '/usr/local/bin/ffmpeg',
    'ffmpeg.threads' => 12,
    'ffprobe.binaries' => '/usr/local/bin/ffprobe',
    'timeout' => 3600,
  ),
  'queue' => 
  array (
    'default' => 'sync',
    'connections' => 
    array (
      'sync' => 
      array (
        'driver' => 'sync',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'jobs',
        'queue' => 'default',
        'expire' => 60,
      ),
      'beanstalkd' => 
      array (
        'driver' => 'beanstalkd',
        'host' => 'localhost',
        'queue' => 'default',
        'ttr' => 60,
      ),
      'sqs' => 
      array (
        'driver' => 'sqs',
        'key' => 'your-public-key',
        'secret' => 'your-secret-key',
        'queue' => 'your-queue-url',
        'region' => 'us-east-1',
      ),
      'iron' => 
      array (
        'driver' => 'iron',
        'host' => 'mq-aws-us-east-1.iron.io',
        'token' => 'your-token',
        'project' => 'your-project-id',
        'queue' => 'your-queue-name',
        'encrypt' => true,
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
        'queue' => 'default',
        'expire' => 60,
      ),
    ),
    'failed' => 
    array (
      'database' => 'mysql',
      'table' => 'failed_jobs',
    ),
  ),
  'services' => 
  array (
    'mailgun' => 
    array (
      'domain' => '',
      'secret' => '',
    ),
    'mandrill' => 
    array (
      'secret' => '',
    ),
    'ses' => 
    array (
      'key' => '',
      'secret' => '',
      'region' => 'us-east-1',
    ),
    'stripe' => 
    array (
      'model' => 'App\\User',
      'key' => '',
      'secret' => '',
    ),
    'facebook' => 
    array (
      'client_id' => '1486518024692226',
      'client_secret' => 'd329e426a925a3dffe38784f707260fb',
      'redirect' => 'http://www.paracosma.tv/callback',
    ),
  ),
  'session' => 
  array (
    'driver' => 'file',
    'lifetime' => 120,
    'expire_on_close' => false,
    'encrypt' => false,
    'files' => 'C:\\xampp\\htdocs\\madison_tv_v2\\framework\\storage\\framework/sessions',
    'connection' => NULL,
    'table' => 'sessions',
    'lottery' => 
    array (
      0 => 2,
      1 => 100,
    ),
    'cookie' => 'laravel_session',
    'path' => '/',
    'domain' => NULL,
    'secure' => false,
  ),
  'view' => 
  array (
    'paths' => 
    array (
      0 => 'C:\\xampp\\htdocs\\madison_tv_v2\\framework\\resources\\views',
    ),
    'compiled' => 'C:\\xampp\\htdocs\\madison_tv_v2\\framework\\storage\\framework\\views',
  ),
  'image' => 
  array (
    'driver' => 'gd',
  ),
);
