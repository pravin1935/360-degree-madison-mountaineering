$(document).ready(function() {
 
  $("#owl-demo").owlCarousel({
 
      autoPlay: 1000, //Set AutoPlay to 3 seconds
 	  slideSpeed:200,
 	  goToFirstSpeed:100,
      items : 4,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3],
      navigation :true,
      autoPlay :true,
      goToFirst:true
  });
 
});